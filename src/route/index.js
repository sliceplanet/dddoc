import {connect} from 'react-redux';
import component from './component';

function mapStateToProps(state) {
  return {
    user: state.user,
    profile: state.profile,
    chats: state.chats,
    favorites: state.favorites,
  };
}

export default connect(mapStateToProps, null)(component);
