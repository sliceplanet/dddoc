import React from 'react';
import {LocaleConfig} from 'react-native-calendars';
import Sound from 'react-native-sound';

// Screens
import Auth from './directions/Auth';
import Client from './directions/Client';
import Doctor from './directions/Doctor';

import {locales} from '../helpers/constant';

export default class Route extends React.Component {
  componentDidMount() {
    LocaleConfig.locales = locales;
    LocaleConfig.defaultLocale = 'ru';

    Sound.setCategory('Playback');
    global.whoosh = new Sound('skype.mp3', Sound.MAIN_BUNDLE, (error) => {
      if (error) {
        console.log('failed to load the sound', error);
        return;
      }
      global.whoosh.setNumberOfLoops(-1);
      console.log(global.whoosh);
    });
  }

  render() {
    const {user} = this.props;
    console.log('Access information -> ', user);

    if (user.accessToken.length > 0) {
      const {role} = user.user;
      switch (role) {
        case 'client': {
          return <Client />;
        }
        case 'doctor': {
          return <Doctor />;
        }
      }
    }
    return <Auth />;
  }
}
