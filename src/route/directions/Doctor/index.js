import {connect} from 'react-redux';
import component from './component';

import {setToast} from '../../../store/actions';
import {
  reducerAlertVideoCallRequest,
  reducerBookingConfirmation,
  reducerDoctorBusyNow,
  reducerAlertMessage,
  reducerDidTheVideoCallTakePlace,
  reducerBookingConfirmed,
  reducerCallConfirmed,
  reducerClientPay,
  reducerDoctorPay,
  reducerAlertSound,
  reduceOnPressClose,
} from '../../../store/actions/alert';
import {
  fetchGetNotifications,
  fetchNotify,
  reducerNotify,
  reducerStatus,
} from '../../../store/actions/notifications';

import {
  fetchGetChats,
  reducerReceive,
  reducerCreated,
} from '../../../store/actions/chats';

import {
  fetchGetSpecializations,
  reducerVerify,
} from '../../../store/actions/personal';
import {fetchGetServices} from '../../../store/actions/services';
import {
  fetchGetVideos,
  reducerVideos,
  reducerVideosCancel,
  reducerVideosAccept,
  reducerVideoPay,
  reducerVideosConfirm,
  reducerVideoCount,
  reducerVideoTime,
} from '../../../store/actions/videos';
import {fetchGetFavorites} from '../../../store/actions/favorites';
import {
  fetchGetReviewsMy,
  fetchGetReviewsFrom,
  reducerReview,
} from '../../../store/actions/reviews';
import {
  fetchGetBalance,
  fetchGetTransactions,
} from '../../../store/actions/balance';
import {logoutUser} from '../../../store/actions/auth';

function mapStateToProps(state) {
  return {
    user: state.user,
    chats: state.chats,
    favorites: state.favorites,
    notifications: state.notifications,
    videos: state.videos,
    reviews: state.reviews,
    payments: state.balance,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setToast: (data) => dispatch(setToast(data)),
    logoutUser: () => dispatch(logoutUser()),
    fetchGetChats: (data) => dispatch(fetchGetChats(data)),
    fetchGetSpecializations: (data) => dispatch(fetchGetSpecializations(data)),
    fetchGetFavorites: (data) => dispatch(fetchGetFavorites(data)),
    fetchGetServices: (data) => dispatch(fetchGetServices(data)),
    fetchGetNotifications: (data) => dispatch(fetchGetNotifications(data)),
    fetchGetVideos: (data) => dispatch(fetchGetVideos(data)),
    fetchGetReviewsMy: (data) => dispatch(fetchGetReviewsMy(data)),
    fetchGetReviewsFrom: (data) => dispatch(fetchGetReviewsFrom(data)),
    fetchNotify: (notify) => dispatch(fetchNotify(notify)),
    fetchGetBalance: (data) => dispatch(fetchGetBalance(data)),
    fetchGetTransactions: (data) => dispatch(fetchGetTransactions(data)),
    reducerBookingConfirmed: (id) => dispatch(reducerBookingConfirmed(id)),
    reducerCallConfirmed: (id) => dispatch(reducerCallConfirmed(id)),

    reduceOnPressClose: () => dispatch(reduceOnPressClose()),
    reducerReceive: (receive, user) => dispatch(reducerReceive(receive, user)),
    reducerCreated: (created, user) => dispatch(reducerCreated(created, user)),
    reducerAlertVideoCallRequest: (id) =>
      dispatch(reducerAlertVideoCallRequest(id)),
    reducerBookingConfirmation: (notify) =>
      dispatch(reducerBookingConfirmation(notify)),
    reducerDoctorBusyNow: (notify) => dispatch(reducerDoctorBusyNow(notify)),
    reducerAlertMessage: () => dispatch(reducerAlertMessage()),
    reducerClientPay: () => dispatch(reducerClientPay()),
    reducerDoctorPay: () => dispatch(reducerDoctorPay()),
    reducerVideos: (notify, user) => dispatch(reducerVideos(notify, user)),
    reducerVideosCancel: (notify) => dispatch(reducerVideosCancel(notify)),
    reducerVideosAccept: (notify) => dispatch(reducerVideosAccept(notify)),
    reducerVerify: (notify) => dispatch(reducerVerify(notify)),
    reducerNotify: (notify) => dispatch(reducerNotify(notify)),
    reducerVideosConfirm: (notify) => dispatch(reducerVideosConfirm(notify)),
    reducerAlertSound: (notify) => dispatch(reducerAlertSound(notify)),
    reducerDidTheVideoCallTakePlace: (id) =>
      dispatch(reducerDidTheVideoCallTakePlace(id)),
    reducerVideoPay: (notify) => dispatch(reducerVideoPay(notify)),
    reducerVideoCount: (id) => dispatch(reducerVideoCount(id)),
    reducerVideoTime: (notify) => dispatch(reducerVideoTime(notify)),

    reducerStatus: (status) => dispatch(reducerStatus(status)),
    reducerReview: (notify) => dispatch(reducerReview(notify)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(component);
