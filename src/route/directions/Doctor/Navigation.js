import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

// Helpers
import {navigationRef} from '../../../helpers/navigation';

// Screens
import Doctor from '../../../components/Screens/Doctor/Doctor';
import Favorite from '../../../components/Screens/Doctor/Favorite';
import DoctorFinder from '../../../components/Screens/Doctor/DoctorFinder';
import VideoCalls from '../../../components/Screens/Doctor/VideoCalls/VideoCalls';
import Call from '../../../components/Screens/Doctor/VideoCalls/Call';
import Profile from '../../../components/Screens/Doctor/Profile';
import Services from '../../../components/Screens/Doctor/Services/Services';
import ServicesDoctor from '../../../components/Screens/Doctor/Services/ServicesDoctor';
import ServiceAdd from '../../../components/Screens/Doctor/Services/ServiceAdd';
import ServiceEdit from '../../../components/Screens/Doctor/Services/ServiceEdit';
import Messages from '../../../components/Screens/Doctor/Messages/Messages';
import Message from '../../../components/Screens/Doctor/Messages/Message';
import DateReservation from '../../../components/Screens/Doctor/Messages/DateReservation';
import TimeReservation from '../../../components/Screens/Doctor/Messages/TimeReservation';
import Specializations from '../../../components/Screens/Doctor/Specializations';
import Notification from '../../../components/Screens/Doctor/Notifications/Notification';
import Notifications from '../../../components/Screens/Doctor/Notifications/Notifications';
import Settings from '../../../components/Screens/Doctor/Settings';
import Verification from '../../../components/Screens/Doctor/Verification';
import Support from '../../../components/Screens/Doctor/Support';
import Reviews from '../../../components/Screens/Doctor/Reviews/Reviews';
import Review from '../../../components/Screens/Doctor/Reviews/Review';
import Balance from '../../../components/Screens/Doctor/Balance';
import Menu from '../../../components/Screens/Doctor/Menu';

const Stack = createStackNavigator();

const screenOptions = {
  headerStyle: {
    borderBottomWidth: 0,
    elevation: 0,
    height: 0,
  },
  headerLeft: null,
};

const options = {title: null};

function Root() {
  return (
    <NavigationContainer ref={navigationRef}>
      <Stack.Navigator screenOptions={screenOptions}>
        <Stack.Screen
          name="DoctorFinder"
          component={DoctorFinder}
          options={options}
        />
        <Stack.Screen
          name="DateReservation"
          component={DateReservation}
          options={options}
        />
        <Stack.Screen
          name="TimeReservation"
          component={TimeReservation}
          options={options}
        />
        <Stack.Screen
          name="Specializations"
          component={Specializations}
          options={options}
        />
        <Stack.Screen
          name="VideoCalls"
          component={VideoCalls}
          options={options}
        />
        <Stack.Screen
          name="ServiceAdd"
          component={ServiceAdd}
          options={options}
        />
        <Stack.Screen
          name="ServiceEdit"
          component={ServiceEdit}
          options={options}
        />
        <Stack.Screen
          name="ServicesDoctor"
          component={ServicesDoctor}
          options={options}
        />
        <Stack.Screen
          name="Notifications"
          component={Notifications}
          options={options}
        />
        <Stack.Screen
          name="Notification"
          component={Notification}
          options={options}
        />
        <Stack.Screen
          name="Verification"
          component={Verification}
          options={options}
        />
        <Stack.Screen name="Balance" component={Balance} options={options} />
        <Stack.Screen name="Reviews" component={Reviews} options={options} />
        <Stack.Screen name="Review" component={Review} options={options} />
        <Stack.Screen name="Support" component={Support} options={options} />
        <Stack.Screen name="Settings" component={Settings} options={options} />
        <Stack.Screen name="Call" component={Call} options={options} />
        <Stack.Screen name="Services" component={Services} options={options} />
        <Stack.Screen name="Doctor" component={Doctor} options={options} />
        <Stack.Screen name="Favorite" component={Favorite} options={options} />
        <Stack.Screen name="Profile" component={Profile} options={options} />
        <Stack.Screen name="Messages" component={Messages} options={options} />
        <Stack.Screen name="Message" component={Message} options={options} />
        <Stack.Screen name="Menu" component={Menu} options={options} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default Root;
