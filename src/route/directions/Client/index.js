import {connect} from 'react-redux';
import component from './component';

import {setToast} from '../../../store/actions';
import {
  reducerAlertVideoCallRequest,
  reducerDoctorBusyNow,
  reducerAlertMessage,
  reducerDidTheVideoCallTakePlace,
  reducerBookingConfirmed,
  reducerCallConfirmed,
  reducerClientPay,
  reducerDoctorPay,
  reducerAlertSound,
  reduceOnPressClose,
} from '../../../store/actions/alert';

import {
  fetchNotify,
  reducerNotify,
  fetchGetNotifications,
  reducerStatus,
} from '../../../store/actions/notifications';
import {
  fetchGetChats,
  reducerReceive,
  reducerCreated,
} from '../../../store/actions/chats';
import {fetchGetFavorites} from '../../../store/actions/favorites';
import {fetchGetSpecializations} from '../../../store/actions/personal';
import {
  fetchGetVideos,
  reducerVideos,
  reducerVideosConfirm,
  reducerVideosCancel,
  reducerVideosAccept,
  reducerVideoPay,
  reducerVideoCount,
  reducerVideoTime,
} from '../../../store/actions/videos';
import {
  fetchGetReviewsMy,
  fetchGetReviewsFrom,
  reducerReview,
} from '../../../store/actions/reviews';
import {
  fetchGetBalance,
  fetchGetTransactions,
} from '../../../store/actions/balance';
import {logoutUser} from '../../../store/actions/auth';

function mapStateToProps(state) {
  return {
    user: state.user,
    chats: state.chats,
    favorites: state.favorites,
    videos: state.videos,
    notifications: state.notifications,
    reviews: state.reviews,
    payments: state.balance,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setToast: (data) => dispatch(setToast(data)),
    logoutUser: () => dispatch(logoutUser()),
    fetchGetChats: (data) => dispatch(fetchGetChats(data)),
    fetchGetFavorites: (data) => dispatch(fetchGetFavorites(data)),
    fetchGetSpecializations: (data) => dispatch(fetchGetSpecializations(data)),
    fetchGetNotifications: (data) => dispatch(fetchGetNotifications(data)),
    fetchGetVideos: (data) => dispatch(fetchGetVideos(data)),
    fetchGetReviewsMy: (data) => dispatch(fetchGetReviewsMy(data)),
    fetchGetReviewsFrom: (data) => dispatch(fetchGetReviewsFrom(data)),
    fetchNotify: (notify) => dispatch(fetchNotify(notify)),
    fetchGetBalance: (data) => dispatch(fetchGetBalance(data)),
    fetchGetTransactions: (data) => dispatch(fetchGetTransactions(data)),

    reduceOnPressClose: () => dispatch(reduceOnPressClose()),
    reducerReceive: (receive, user) => dispatch(reducerReceive(receive, user)),
    reducerCreated: (created, user) => dispatch(reducerCreated(created, user)),
    reducerAlertVideoCallRequest: (notify) =>
      dispatch(reducerAlertVideoCallRequest(notify)),
    reducerDoctorBusyNow: (notify) => dispatch(reducerDoctorBusyNow(notify)),
    reducerAlertMessage: () => dispatch(reducerAlertMessage()),
    reducerClientPay: () => dispatch(reducerClientPay()),
    reducerDoctorPay: () => dispatch(reducerDoctorPay()),
    reducerBookingConfirmed: (id) => dispatch(reducerBookingConfirmed(id)),
    reducerCallConfirmed: (id) => dispatch(reducerCallConfirmed(id)),
    reducerVideos: (notify, user) => dispatch(reducerVideos(notify, user)),
    reducerVideosCancel: (notify) => dispatch(reducerVideosCancel(notify)),
    reducerVideosAccept: (notify) => dispatch(reducerVideosAccept(notify)),
    reducerVideoTime: (notify) => dispatch(reducerVideoTime(notify)),
    reducerNotify: (notify) => dispatch(reducerNotify(notify)),
    reducerAlertSound: (notify) => dispatch(reducerAlertSound(notify)),
    reducerVideosConfirm: (notify) => dispatch(reducerVideosConfirm(notify)),
    reducerDidTheVideoCallTakePlace: (id) =>
      dispatch(reducerDidTheVideoCallTakePlace(id)),
    reducerVideoPay: (notify) => dispatch(reducerVideoPay(notify)),
    reducerVideoCount: (id) => dispatch(reducerVideoCount(id)),

    reducerStatus: (status) => dispatch(reducerStatus(status)),
    reducerReview: (notify) => dispatch(reducerReview(notify)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(component);
