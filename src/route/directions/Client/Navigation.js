import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

// Helpers
import {navigationRef} from '../../../helpers/navigation';

// Screens
import DoctorFinder from '../../../components/Screens/Client/DoctorFinder';
import Profile from '../../../components/Screens/Client/Profile';
import VideoCalls from '../../../components/Screens/Client/VideoCalls/VideoCalls';
import Call from '../../../components/Screens/Client/VideoCalls/Call';
import Favorite from '../../../components/Screens/Client/Favorite';
import Doctor from '../../../components/Screens/Client/Doctor';
import Services from '../../../components/Screens/Client/Services';
import Messages from '../../../components/Screens/Client/Messages/Messages';
import Message from '../../../components/Screens/Client/Messages/Message';
import DateReservation from '../../../components/Screens/Client/Messages/DateReservation';
import TimeReservation from '../../../components/Screens/Client/Messages/TimeReservation';
import Notifications from '../../../components/Screens/Client/Notifications/Notifications';
import Notification from '../../../components/Screens/Client/Notifications/Notification';
import Settings from '../../../components/Screens/Client/Settings';
import Support from '../../../components/Screens/Client/Support';
import Reviews from '../../../components/Screens/Client/Reviews/Reviews';
import Review from '../../../components/Screens/Client/Reviews/Review';
import Balance from '../../../components/Screens/Client/Balance';
import Menu from '../../../components/Screens/Client/Menu';

const Stack = createStackNavigator();

const screenOptions = {
  headerStyle: {
    borderBottomWidth: 0,
    elevation: 0,
    height: 0,
  },
  headerLeft: null,
};

const options = {title: null};

function Root() {
  return (
    <NavigationContainer ref={navigationRef}>
      <Stack.Navigator screenOptions={screenOptions}>
        <Stack.Screen
          name="DoctorFinder"
          component={DoctorFinder}
          options={options}
        />
        <Stack.Screen
          name="VideoCalls"
          component={VideoCalls}
          options={options}
        />
        <Stack.Screen
          name="DateReservation"
          component={DateReservation}
          options={options}
        />
        <Stack.Screen
          name="TimeReservation"
          component={TimeReservation}
          options={options}
        />
        <Stack.Screen
          name="Notifications"
          component={Notifications}
          options={options}
        />
        <Stack.Screen
          name="Notification"
          component={Notification}
          options={options}
        />
        <Stack.Screen name="Balance" component={Balance} options={options} />
        <Stack.Screen name="Reviews" component={Reviews} options={options} />
        <Stack.Screen name="Review" component={Review} options={options} />
        <Stack.Screen name="Support" component={Support} options={options} />
        <Stack.Screen name="Settings" component={Settings} options={options} />
        <Stack.Screen name="Call" component={Call} options={options} />
        <Stack.Screen name="Services" component={Services} options={options} />
        <Stack.Screen name="Doctor" component={Doctor} options={options} />
        <Stack.Screen name="Favorite" component={Favorite} options={options} />
        <Stack.Screen name="Profile" component={Profile} options={options} />
        <Stack.Screen name="Messages" component={Messages} options={options} />
        <Stack.Screen name="Message" component={Message} options={options} />
        <Stack.Screen name="Menu" component={Menu} options={options} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default Root;
