import React from 'react';
import {Alert, Linking} from 'react-native';

import {URL} from '../../../store/api';
import {
  navigate,
  replace,
  goBack,
  navigationRef,
} from '../../../helpers/navigation';

import Navigation from './Navigation';

export default class Client extends React.Component {
  componentDidMount() {
    const signalR = require('@microsoft/signalr');
    const {
      user,
      favorites,
      videos,
      notifications,
      payments,
      logoutUser,
      fetchGetChats,
      fetchGetFavorites,
      fetchGetSpecializations,
      fetchGetVideos,
      fetchGetNotifications,
      fetchGetReviewsMy,
      fetchGetReviewsFrom,
      fetchNotify,
      fetchGetBalance,
      fetchGetTransactions,
      reducerReceive,
      reducerCreated,
      reducerNotify,
      reducerVideoCount,
      reducerVideos,
      reducerVideosCancel,
      reducerVideosConfirm,
      reducerVideosAccept,
      reducerDidTheVideoCallTakePlace,
      reducerStatus,
      reducerVideoPay,
      reducerClientPay,
      reducerDoctorPay,
      reducerDoctorBusyNow,
      reducerReview,
      reducerVideoTime,
      reducerAlertSound,
      reduceOnPressClose,
    } = this.props;

    fetchGetSpecializations();
    fetchGetBalance({user});

    const data = {
      id: 0,
      count: 20,
    };
    fetchGetChats({user, data});

    if (payments.transactions.length === 0) {
      const path = {
        page: 1,
        pageSize: 100,
      };
      fetchGetTransactions({user, path});
    }

    if (favorites.length === 0) {
      const path = {
        page: 1,
        pageSize: 100,
      };
      fetchGetFavorites({user, path});
    }

    if (notifications.length === 0) {
      const path = {
        page: 1,
        pageSize: 100,
      };
      fetchGetNotifications({user, path});
    }

    if (videos.items.length === 0) {
      const path = {
        page: 1,
        pageSize: 100,
      };
      fetchGetVideos({user, path});
    }

    const path = {
      page: 1,
      pageSize: 100,
    };
    fetchGetReviewsMy({user, path});
    fetchGetReviewsFrom({user, path});

    const connection = new signalR.HubConnectionBuilder()
      .withUrl(URL + '/chat', {
        accessTokenFactory: () => user.accessToken,
      })
      .build();

    this.chatHub = connection;

    connection.on('receive', (receive) => {
      console.log(receive);
      reducerReceive(receive, user);
    });

    connection.on('created', (created) => {
      console.log(created);
      reducerCreated(created, user);
    });

    connection.onclose(() => {
      if (this.unmount) {
        return;
      }
      console.log('Reconnect');
      connection
        .start()
        .then(() => {
          global.connection = connection;
        })
        .catch((err) => console.log(err));
    });

    connection
      .start()
      .then(() => {
        global.connection = connection;
      })
      .catch((err) => console.log(err));

    const connectionNotify = new signalR.HubConnectionBuilder()
      .withUrl(URL + '/notify', {
        accessTokenFactory: () => user.accessToken,
      })
      .build();

    this.notifyHub = connectionNotify;

    connectionNotify.on('status', (status) => {
      console.log(status);
      reducerStatus(status);
    });

    connectionNotify.on('notify', (notify) => {
      console.log(notify);
      switch (notify.type) {
        case 2: {
          reducerVideos(notify, user);
          if (notify.from._id !== user.user.id) {
            reducerNotify(notify);
          }
          break;
        }
        case 3: {
          reducerVideos(notify, user);
          if (notify.from._id !== user.user.id) {
            reducerNotify(notify);
          }
          break;
        }
        case 4: {
          if (
            notify.clientId === user.user.id &&
            notify.to._id === user.user.id
          ) {
            reducerDoctorBusyNow(notify);
          }
          reducerVideosCancel(notify);
          if (notify.from._id !== user.user.id) {
            reducerNotify(notify);
          }
          break;
        }
        case 5: {
          if (notify.from._id !== user.user.id) {
            reducerNotify(notify);
          }
          reducerVideosConfirm(notify);
          fetchNotify(notify);
          break;
        }
        case 6: {
          if (notify.doctorId === user.user.id) {
            reducerDoctorPay();
          } else {
            reducerClientPay();
          }
          reducerVideoPay(notify);
          break;
        }
        case 11: {
          if (notify.from._id === user.user.id) {
            navigate('Call', {channelName: notify.extra.id});
          } else {
            if (!global.whoosh.isPlaying()) {
              global.whoosh.play((success) => {
                if (success) {
                  console.log('successfully finished playing');
                } else {
                  console.log('playback failed due to audio decoding errors');
                }
              });
            }

            reducerAlertSound(notify);
            // setTimeout(() => {
            //   Alert.alert(
            //     'Входящий звонок',
            //     'Желаете принять звонок?',
            //     [
            //       {
            //         text: 'Да',
            //         onPress: () => {
            //           apiPutVideosIdAccept({
            //             path: {id: notify.extra.id},
            //             user,
            //           });
            //           navigate('Call', {channelName: notify.extra.id});

            //           global.whoosh.pause();
            //         },
            //       },
            //       {
            //         text: 'Нет',
            //         onPress: () => {
            //           apiPutVideosIdDecline({
            //             path: {id: notify.extra.id},
            //             user,
            //           });

            //           global.whoosh.pause();
            //         },
            //       },
            //     ],
            //     {cancelable: false},
            //   );
            // }, 1000);
          }
          break;
        }
        case 13: {
          goBack();
          const {id} = notify.extra;
          const video = this.props.videos.items.filter((e) => e.id === id)[0];
          if (
            (video.clientId === user.user.id && !video.clientAnswer) ||
            (video.doctorId === user.user.id && !video.doctorAnswer)
          ) {
            reducerDidTheVideoCallTakePlace(id);
          }
          reducerVideoTime(notify);
          reducerVideoCount(id);
          break;
        }
        case 14: {
          reducerNotify(notify);
          break;
        }
        case 16: {
          if (navigationRef.current.getCurrentRoute().name === 'Call') {
            goBack();
            const {id} = notify.extra;
            const video = this.props.videos.items.filter((e) => e.id === id)[0];
            if (
              (video.clientId === user.user.id && !video.clientAnswer) ||
              (video.doctorId === user.user.id && !video.doctorAnswer)
            ) {
              reducerDidTheVideoCallTakePlace(id);
            }
            reducerVideoCount(id);
          } else {
            reducerVideosAccept(notify);
          }
          break;
        }
        case 17: {
          if (navigationRef.current.getCurrentRoute().name === 'Call') {
            goBack();
            const {id} = notify.extra;
            const video = this.props.videos.items.filter((e) => e.id === id)[0];
            if (
              (video.clientId === user.user.id && !video.clientAnswer) ||
              (video.doctorId === user.user.id && !video.doctorAnswer)
            ) {
              reducerDidTheVideoCallTakePlace(id);
            }
            reducerVideoCount(id);
          } else {
            reducerVideosCancel(notify);
          }
          break;
        }
        case 18: {
          reducerReview(notify);
          if (notify.from._id !== user.user.id) {
            reducerNotify(notify);
            Alert.alert(
              'Новый отзыв',
              `Пользователь ${this.getName(notify.from)} оставил новый отзыв`,
              [{text: 'Ок'}],
              {cancelable: false},
            );
          }
          break;
        }
        case 19: {
          fetchGetTransactions({
            user,
            path: {
              page: 1,
              pageSize: 100,
            },
          });
          fetchGetBalance({user});
          break;
        }
        case 20: {
          reduceOnPressClose();
          global.whoosh.pause();
          replace('DoctorFinder');
          break;
        }
        case 21: {
          global.whoosh.pause();
        }
      }
    });

    connectionNotify.onclose(() => {
      if (this.unmount) {
        return;
      }
      console.log('Reconnect notify');
      connectionNotify
        .start()
        .then(() => {
          global.connectionNotify = connectionNotify;
        })
        .catch((err) => console.log(err));
    });

    connectionNotify
      .start()
      .then(() =>
        connectionNotify
          .invoke('SendOffset', new Date().getTimezoneOffset())
          .catch((err) => {
            console.log(err);
          }),
      )
      .catch(() => logoutUser());

    Linking.addEventListener('url', this.handleOpenURL);
  }

  componentWillUnmount() {
    Linking.removeEventListener('url', this.handleOpenURL);
    this.unmount = true;
    if (this.chatHub) {
      this.chatHub.stop();
    }
    if (this.notifyHub) {
      this.notifyHub.stop();
    }
  }

  handleOpenURL = (event) => {
    console.log(event.url);
    const {fetchGetBalance, fetchGetTransactions, user} = this.props;
    fetchGetBalance({user});
    fetchGetTransactions({
      user,
      path: {
        page: 1,
        pageSize: 100,
      },
    });
  };

  getName(user) {
    const {name, lastName, patronymic} = user;
    let result = [];

    if (name) {
      result = [...result, name];
    }
    if (lastName) {
      result = [...result, lastName];
    }
    if (patronymic) {
      result = [...result, patronymic];
    }

    return result.join(' ');
  }

  render() {
    return <Navigation />;
  }
}
