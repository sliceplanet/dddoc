import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

import {navigationRef} from '../../../helpers/navigation';

// Screens
import SignIn from '../../../components/Screens/Auth/SignIn';
import SignUp from '../../../components/Screens/Auth/SignUp';
import Terms from '../../../components/Screens/Auth/Terms';
import RecoveryPassword from '../../../components/Screens/Auth/RecoveryPassword';

const Stack = createStackNavigator();

const options = {title: null};

function Auth() {
  return (
    <NavigationContainer ref={navigationRef}>
      <Stack.Navigator headerMode="none">
        <Stack.Screen name="SignIn" component={SignIn} options={options} />
        <Stack.Screen name="SignUp" component={SignUp} options={options} />
        <Stack.Screen name="Terms" component={Terms} options={options} />
        <Stack.Screen
          name="RecoveryPassword"
          component={RecoveryPassword}
          options={options}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default Auth;
