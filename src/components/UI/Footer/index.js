import {connect} from 'react-redux';
import component from './component';

import {fetchPostSupport} from '../../../store/actions/support';

function mapStateToProps(state) {
  return {
    user: state.user,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    fetchPostSupport: (data) => dispatch(fetchPostSupport(data)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(component);
