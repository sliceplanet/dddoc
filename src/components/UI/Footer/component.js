import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';

// Style
import {base} from './styles';

export default class Footer extends React.Component {
  onPressSupport = () => {
    const {user, onPress, fetchPostSupport} = this.props;
    if (onPress) {
      onPress();
    }
    fetchPostSupport({user});
  };

  render() {
    const {style} = this.props;
    return (
      <View style={[base.white, style]}>
        <View style={base.support}>
          <TouchableOpacity onPress={this.onPressSupport}>
            <Text style={base.text1}>Написать</Text>
          </TouchableOpacity>
          <Text style={base.text2}> в службу поддержки</Text>
        </View>
        <View style={base.copyright}>
          <Text style={base.text2}>@ Copyright 2020</Text>
        </View>
      </View>
    );
  }
}
