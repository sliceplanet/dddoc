import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
    alignItems: 'center',
  },
  white: {
    width: wp(100),
    backgroundColor: 'white',
    marginTop: 16,
  },
  support: {
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'center',
    marginTop: 15,
    marginBottom: 6,
  },
  text1: {
    fontFamily: 'SF Pro Text',
    fontSize: 13,
    fontWeight: '500',
    color: '#D962A5',
  },
  text2: {
    fontFamily: 'SF Pro Text',
    fontSize: 13,
    color: '#ACACAC',
  },
  copyright: {
    alignSelf: 'center',
    marginBottom: 15,
  },
});

export default {base};
