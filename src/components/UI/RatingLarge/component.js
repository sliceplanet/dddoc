import React from 'react';
import {View} from 'react-native';
import Image from 'react-native-scalable-image';

// Helpers
import * as Images from '../../../helpers/images';

// Style
import {base} from './styles';

export default class RatingLarge extends React.Component {
  render() {
    const {rating, onChange} = this.props;
    let star = [];
    for (let i = 1; i < 6; i++) {
      star = [
        ...star,
        <Image
          key={i}
          source={Images.star}
          width={20}
          style={i > rating && base.tintColor}
          onPress={() => onChange(i)}
        />,
      ];
    }

    return <View style={base.wrap}>{star}</View>;
  }
}
