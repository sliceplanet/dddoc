import React from 'react';
import Image from 'react-native-scalable-image';

// Components
import * as Images from '../../../helpers/images';

// Style
import {base} from './styles';

export default class Arrow extends React.Component {
  render() {
    const {direction} = this.props;
    if (direction === 'left') {
      return <Image source={Images.arrowLeft} height={24} />;
    }
    return <Image source={Images.arrowRight} height={24} />;
  }
}
