import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
    alignItems: 'center',
  },
  wrap: {
    width: wp(100) - 32,
    height: 56,
    alignItems: 'center',
    backgroundColor: '#75C9C3',
    borderRadius: 14,
    marginVertical: 8,
  },
  disabled: {
    backgroundColor: '#ACACAC',
  },
  text1: {
    fontFamily: 'SF Pro Text',
    fontSize: 17,
    fontWeight: '600',
    lineHeight: 22,
    color: 'white',
  },
});

export default {base};
