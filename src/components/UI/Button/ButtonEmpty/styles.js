import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
    alignItems: 'center',
  },
  wrap: {
    width: wp(100) - 32,
    height: 56,
    alignItems: 'center',
    backgroundColor: 'white',
    borderColor: '#75C9C3',
    borderWidth: 1,
    borderRadius: 14,
    marginVertical: 8,
  },
  text1: {
    fontFamily: 'SF Pro Text',
    fontSize: 17,
    fontWeight: '600',
    lineHeight: 22,
    color: '#75C9C3',
  },
});

export default {base};
