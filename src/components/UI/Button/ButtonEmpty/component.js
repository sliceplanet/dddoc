import React from 'react';
import {Text, TouchableOpacity, View} from 'react-native';

// Style
import {base} from './styles';

export default class ButtonEmpty extends React.Component {
  render() {
    const {style, title, onPress} = this.props;

    return (
      <TouchableOpacity style={[base.wrap, style]} onPress={onPress}>
        <View style={base.flex} />
        <Text style={base.text1}>{title}</Text>
        <View style={base.flex} />
      </TouchableOpacity>
    );
  }
}
