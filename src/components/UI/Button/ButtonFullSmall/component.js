import React from 'react';
import {Text, TouchableOpacity, View} from 'react-native';

// Style
import {base} from './styles';

export default class ButtonFullSmall extends React.Component {
  render() {
    const {style, title, disabled, onPress} = this.props;

    return (
      <TouchableOpacity
        disabled={disabled}
        style={[base.wrap, disabled && base.disabled, style]}
        onPress={onPress}>
        <View style={base.flex} />
        <Text style={base.text1}>{title}</Text>
        <View style={base.flex} />
      </TouchableOpacity>
    );
  }
}
