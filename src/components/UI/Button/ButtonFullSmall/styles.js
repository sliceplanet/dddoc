import {StyleSheet} from 'react-native';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
    alignItems: 'center',
  },
  wrap: {
    alignItems: 'center',
    backgroundColor: '#75C9C3',
    borderRadius: 10,
    marginVertical: 5,
  },
  disabled: {
    backgroundColor: '#ACACAC',
  },
  text1: {
    fontFamily: 'SF Pro Text',
    fontSize: 13,
    color: 'white',
    paddingVertical: 8,
    paddingHorizontal: 16,
  },
});

export default {base};
