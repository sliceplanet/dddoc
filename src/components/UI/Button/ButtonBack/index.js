import * as React from 'react';
import {useRoute, useNavigationState} from '@react-navigation/native';
import Image from 'react-native-scalable-image';

import * as Images from '../../../../helpers/images';
import {goBack} from '../../../../helpers/navigation';

import {base} from './styles';

export default function ButtonBack() {
  const route = useRoute();
  const isFirstRouteInParent = useNavigationState(
    (state) => state.routes[0].key === route.key,
  );

  if (!isFirstRouteInParent) {
    return (
      <Image
        source={Images.back}
        style={base.image}
        height={20}
        onPress={goBack}
      />
    );
  }
  return null;
}
