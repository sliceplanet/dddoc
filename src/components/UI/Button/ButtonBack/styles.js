import {StyleSheet} from 'react-native';

export const base = StyleSheet.create({
  image: {
    margin: 8,
  },
});

export default {base};
