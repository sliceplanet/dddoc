import React from 'react';
import {Text, TouchableOpacity, View} from 'react-native';

// Style
import {base} from './styles';

export default class ButtonEmptySmall extends React.Component {
  render() {
    const {style, disabled, title, onPress} = this.props;

    return (
      <TouchableOpacity
        disabled={disabled}
        style={[base.wrap, style]}
        onPress={onPress}>
        <View style={base.flex} />
        <Text style={base.text1}>{title}</Text>
        <View style={base.flex} />
      </TouchableOpacity>
    );
  }
}
