import {StyleSheet} from 'react-native';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
    alignItems: 'center',
  },
  wrap: {
    alignItems: 'center',
    backgroundColor: 'white',
    borderColor: '#75C9C3',
    borderWidth: 1,
    borderRadius: 10,
    marginVertical: 5,
  },
  text1: {
    fontFamily: 'SF Pro Text',
    fontSize: 13,
    color: '#75C9C3',
    paddingVertical: 8,
    paddingHorizontal: 16,
  },
});

export default {base};
