import {StyleSheet} from 'react-native';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
    alignItems: 'center',
  },
  wrap: {
    alignItems: 'center',
    backgroundColor: '#14D378',
    borderRadius: 8,
    paddingVertical: 7,
    paddingHorizontal: 12,
  },
  text1: {
    fontFamily: 'SF Pro Text',
    fontSize: 12,
    fontWeight: '600',
    lineHeight: 18,
    color: 'white',
  },
});

export default {base};
