import {StyleSheet} from 'react-native';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
    alignItems: 'center',
  },
  text1: {
    fontFamily: 'SF Pro Text',
    fontSize: 12,
    lineHeight: 18,
    color: '#D962A5',
    paddingVertical: 8,
    paddingHorizontal: 16,
    textDecorationColor: '#D962A5',
    textDecorationLine: 'underline',
  },
});

export default {base};
