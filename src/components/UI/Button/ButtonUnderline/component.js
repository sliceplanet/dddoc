import React from 'react';
import {Text, TouchableOpacity} from 'react-native';

// Style
import {base} from './styles';

export default class ButtonUnderline extends React.Component {
  render() {
    const {style, title, onPress} = this.props;

    return (
      <TouchableOpacity style={style} onPress={onPress}>
        <Text style={base.text1}>{title}</Text>
      </TouchableOpacity>
    );
  }
}
