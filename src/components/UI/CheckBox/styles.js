import {StyleSheet} from 'react-native';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
    alignItems: 'center',
  },
  wrap1: {
    backgroundColor: '#75C9C3',
    borderRadius: 4,
  },
  wrap2: {
    width: 16,
    height: 16,
    borderWidth: 1,
    borderColor: '#75C9C3',
    borderRadius: 4,
    backgroundColor: 'white',
  },
});

export default {base};
