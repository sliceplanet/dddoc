import React from 'react';
import {TouchableOpacity} from 'react-native';
import Image from 'react-native-scalable-image';

// Helpers
import * as Images from '../../../helpers/images';

// Style
import {base} from './styles';

export default class CheckBox extends React.Component {
  render() {
    const {value, onChange} = this.props;

    if (value) {
      return (
        <TouchableOpacity style={base.wrap1} onPress={onChange}>
          <Image source={Images.check} width={16} />
        </TouchableOpacity>
      );
    }
    return <TouchableOpacity style={base.wrap2} onPress={onChange} />;
  }
}
