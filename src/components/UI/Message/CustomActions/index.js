import {connect} from 'react-redux';
import component from './component';

import {setToast, setNetworkIndicator} from '../../../../store/actions';
import {reducerAddMessage} from '../../../../store/actions/chats';

function mapStateToProps(state) {
  return {
    user2: state.user,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setToast: (data) => dispatch(setToast(data)),
    setNetworkIndicator: (data) => dispatch(setNetworkIndicator(data)),
    reducerAddMessage: (messages, chatId, user) =>
      dispatch(reducerAddMessage(messages, chatId, user)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(component);
