import React from 'react';
import Image from 'react-native-scalable-image';
import DocumentPicker from 'react-native-document-picker';
import ImagePicker from 'react-native-image-picker';
import RNFS from 'react-native-fs';
import moment from 'moment';

// Helpers
import * as Images from '../../../../helpers/images';

// Api
import {apiPostUserPhoto} from '../../../../store/api/personal';

export default class CustomActions extends React.Component {
  onPress = () => {
    const options = {
      title: 'Выберите файл',
      customButtons: [{name: 'document', title: 'Выбрать документ'}],
      storageOptions: {
        skipBackup: true,
      },
    };

    ImagePicker.showImagePicker(options, (response) => {
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        DocumentPicker.pick({
          type: [DocumentPicker.types.allFiles],
        }).then((result) => {
          this.uploadFiles('document', result);
        });
      } else {
        this.uploadFiles('image', response);
      }
    });
  };

  uploadFiles = (type, files) => {
    const {connection} = global;
    const {
      user,
      user2,
      chatId,
      setNetworkIndicator,
      reducerAddMessage,
      messageIdGenerator,
    } = this.props;
    setNetworkIndicator(true);
    console.log(files);
    switch (type) {
      case 'document': {
        RNFS.readFile(files.uri, 'base64').then((data) => {
          apiPostUserPhoto({user: user2, data, contentType: files.type})
            .then((result) => {
              const {path} = result.data;
              const _id = messageIdGenerator();
              connection.invoke('send', {
                id: _id,
                chatId,
                to: user,
                text: files.name,
                attachments: [path],
              });

              if (chatId !== 0) {
                const messages = [
                  {
                    _id,
                    chatId,
                    user,
                    createdAt: moment(),
                    text: files.name,
                    attachments: [path],
                  },
                ];
                reducerAddMessage(messages, chatId, user);
              }
              setNetworkIndicator(false);
            })
            .catch((e) => {
              console.log(e);
              setNetworkIndicator(false);
            });
        });
        break;
      }
      case 'image': {
        const {data} = files;
        apiPostUserPhoto({user: user2, data, contentType: files.type})
          .then((result) => {
            const {path} = result.data;
            const _id = messageIdGenerator();

            connection.invoke('send', {
              id: _id,
              chatId,
              to: user,
              text: files.fileName || `image.${files.type.split('/')[1]}`,
              attachments: [path],
            });

            if (chatId !== 0) {
              const messages = [
                {
                  _id,
                  chatId,
                  user,
                  createdAt: moment(),
                  text: files.fileName || `image.${files.type.split('/')[1]}`,
                  attachments: [path],
                },
              ];
              reducerAddMessage(messages, chatId, user);
            }
            setNetworkIndicator(false);
          })
          .catch((e) => {
            console.log(e);
            setNetworkIndicator(false);
          });
        break;
      }
    }
  };

  render() {
    return <Image source={Images.actions} height={24} onPress={this.onPress} />;
  }
}
