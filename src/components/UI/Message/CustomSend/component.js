import React from 'react';
import Image from 'react-native-scalable-image';

// Helpers
import * as Images from '../../../../helpers/images';

// Style
import {base} from './styles';

export default class CustomSend extends React.Component {
  onPress = () => {
    const {text, onSend} = this.props;

    if (text && onSend) {
      onSend({text: text.trim()}, true);
    }
  };
  render() {
    return <Image source={Images.send} height={18} onPress={this.onPress} />;
  }
}
