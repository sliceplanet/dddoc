import PropTypes from 'prop-types';
import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  ActivityIndicator,
  ViewPropTypes,
} from 'react-native';
import Image from 'react-native-scalable-image';
import RNFS from 'react-native-fs';
import FileViewer from 'react-native-file-viewer';

// Helpers
import * as Images from '../../../../helpers/images';
import {URL} from '../../../../store/api';
import {base} from './styles';

const textStyle = {
  fontSize: 16,
  lineHeight: 20,
  marginTop: 5,
  marginBottom: 5,
  marginLeft: 10,
  marginRight: 10,
};

const styles = {
  left: StyleSheet.create({
    container: {},
    text: {
      color: 'black',
      ...textStyle,
    },
    link: {
      color: 'black',
      textDecorationLine: 'underline',
    },
  }),
  right: StyleSheet.create({
    container: {},
    text: {
      color: 'white',
      ...textStyle,
    },
    link: {
      color: 'white',
      textDecorationLine: 'underline',
    },
  }),
};

const DEFAULT_OPTION_TITLES = ['Call', 'Text', 'Cancel'];

export default class CustomMessageText extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
    };
  }

  // shouldComponentUpdate(nextProps) {
  //   return (
  //     !!this.props.currentMessage &&
  //     !!nextProps.currentMessage &&
  //     this.props.currentMessage.text !== nextProps.currentMessage.text
  //   );
  // }

  async onPressFile() {
    const {attachments} = this.props.currentMessage;
    const fromUrl = `${URL}${attachments[0]}`;
    const fileName = attachments[0].split('/');
    const toFile = `${RNFS.DocumentDirectoryPath}/${
      fileName[fileName.length - 1]
    }`;

    const fileExists = await RNFS.exists(toFile);
    if (!fileExists) {
      const options = {
        fromUrl,
        toFile,
        begin: () => this.setState({loading: true}),
      };

      const {promise} = RNFS.downloadFile(options);
      promise
        .then(() => {
          this.setState({loading: false});
          // FileViewer.open(toFile);
        })
        .catch((err) => {
          console.log(err);
        });
    } else {
      FileViewer.open(toFile);
    }
  }

  render() {
    const {loading} = this.state;

    return (
      <TouchableOpacity
        onPress={this.onPressFile.bind(this)}
        style={[
          styles[this.props.position].container,
          this.props.containerStyle &&
            this.props.containerStyle[this.props.position],
        ]}>
        <View style={base.row}>
          {loading ? (
            <ActivityIndicator size="small" color="#75C9C3" />
          ) : (
            <Image source={Images.document} width={18} />
          )}

          <Text
            style={[
              styles[this.props.position].text,
              this.props.textStyle && this.props.textStyle[this.props.position],
              this.props.customTextStyle,
            ]}
            childrenProps={{...this.props.textProps}}>
            {this.props.currentMessage.text}
          </Text>
        </View>
      </TouchableOpacity>
    );
  }
}

CustomMessageText.contextTypes = {
  actionSheet: PropTypes.func,
};

CustomMessageText.defaultProps = {
  position: 'left',
  optionTitles: DEFAULT_OPTION_TITLES,
  currentMessage: {
    text: '',
  },
  containerStyle: {},
  textStyle: {},
  linkStyle: {},
  customTextStyle: {},
  textProps: {},
  parsePatterns: () => [],
};
CustomMessageText.propTypes = {
  position: PropTypes.oneOf(['left', 'right']),
  optionTitles: PropTypes.arrayOf(PropTypes.string),
  currentMessage: PropTypes.object,
  containerStyle: PropTypes.shape({
    left: ViewPropTypes.style,
    right: ViewPropTypes.style,
  }),
  textStyle: PropTypes.shape({
    left: PropTypes.oneOfType([PropTypes.object, PropTypes.number]),
    right: PropTypes.oneOfType([PropTypes.object, PropTypes.number]),
  }),
  linkStyle: PropTypes.shape({
    left: PropTypes.oneOfType([PropTypes.object, PropTypes.number]),
    right: PropTypes.oneOfType([PropTypes.object, PropTypes.number]),
  }),
  parsePatterns: PropTypes.func,
  textProps: PropTypes.object,
  customTextStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.number]),
};
