import PropTypes from 'prop-types';
import React from 'react';
import {StyleSheet, View, Text, ViewPropTypes} from 'react-native';
import moment from 'moment';
import Image from 'react-native-scalable-image';

// Helpers
import * as Images from '../../../../helpers/images';
import {base} from './styles';

const textStyle = {
  fontSize: 16,
  lineHeight: 20,
  marginTop: 5,
  marginBottom: 5,
  marginLeft: 10,
  marginRight: 10,
};

const styles = {
  left: StyleSheet.create({
    container: {},
    text: {
      color: 'black',
      ...textStyle,
    },
    link: {
      color: 'black',
      textDecorationLine: 'underline',
    },
  }),
  right: StyleSheet.create({
    container: {},
    text: {
      color: 'white',
      ...textStyle,
    },
    link: {
      color: 'white',
      textDecorationLine: 'underline',
    },
  }),
};

const DEFAULT_OPTION_TITLES = ['Call', 'Text', 'Cancel'];

export default class CustomMessageVideo extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
    };
  }

  render() {
    const start = moment(this.props.currentMessage.start);
    const end = moment(this.props.currentMessage.end);

    return (
      <View
        style={[
          styles[this.props.position].container,
          this.props.containerStyle &&
            this.props.containerStyle[this.props.position],
        ]}>
        <View style={base.row}>
          <Image source={Images.videoCall} width={18} />

          <Text
            style={[
              styles[this.props.position].text,
              this.props.textStyle && this.props.textStyle[this.props.position],
              this.props.customTextStyle,
            ]}
            childrenProps={{...this.props.textProps}}>
            {`Видео-звонок\n${end.subtract(start).format('mm мин. ss сек.')}`}
          </Text>
        </View>
      </View>
    );
  }
}

CustomMessageVideo.contextTypes = {
  actionSheet: PropTypes.func,
};

CustomMessageVideo.defaultProps = {
  position: 'left',
  optionTitles: DEFAULT_OPTION_TITLES,
  currentMessage: {
    text: '',
  },
  containerStyle: {},
  textStyle: {},
  linkStyle: {},
  customTextStyle: {},
  textProps: {},
  parsePatterns: () => [],
};
CustomMessageVideo.propTypes = {
  position: PropTypes.oneOf(['left', 'right']),
  optionTitles: PropTypes.arrayOf(PropTypes.string),
  currentMessage: PropTypes.object,
  containerStyle: PropTypes.shape({
    left: ViewPropTypes.style,
    right: ViewPropTypes.style,
  }),
  textStyle: PropTypes.shape({
    left: PropTypes.oneOfType([PropTypes.object, PropTypes.number]),
    right: PropTypes.oneOfType([PropTypes.object, PropTypes.number]),
  }),
  linkStyle: PropTypes.shape({
    left: PropTypes.oneOfType([PropTypes.object, PropTypes.number]),
    right: PropTypes.oneOfType([PropTypes.object, PropTypes.number]),
  }),
  parsePatterns: PropTypes.func,
  textProps: PropTypes.object,
  customTextStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.number]),
};
