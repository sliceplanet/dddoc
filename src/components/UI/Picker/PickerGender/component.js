import React from 'react';
import {View, Text} from 'react-native';

// Components
import Touchable from '../../Touchable';

// Style
import {base} from './styles';

export default class PickerGender extends React.Component {
  renderButton = (i, title) => {
    const {index, editable, onChange} = this.props;

    if (index === i) {
      return (
        <View style={base.btnWrap1}>
          <Text style={base.text1}>{title}</Text>
        </View>
      );
    }
    return (
      <Touchable
        style={base.btnWrap2}
        editable={editable}
        onPress={() => onChange(i)}>
        <Text style={base.text2}>{title}</Text>
      </Touchable>
    );
  };

  render() {
    return (
      <View style={base.wrap}>
        {this.renderButton(1, 'Мужчина')}
        {this.renderButton(2, 'Женщина')}
      </View>
    );
  }
}
