import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
    alignItems: 'center',
  },
  wrap: {
    width: wp(100) - 32,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'rgba(118, 118, 128, 0.12)',
    borderRadius: 8,
    marginTop: 16,
    padding: 3,
  },
  btnWrap1: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#75C9C3',
    borderRadius: 6,
    paddingVertical: 7,
  },
  btnWrap2: {
    flex: 1,
    alignItems: 'center',
    paddingVertical: 7,
  },
  text1: {
    fontFamily: 'SF Pro Text',
    fontSize: 14,
    lineHeight: 18,
    fontWeight: '600',
    color: 'white',
  },
  text2: {
    fontFamily: 'SF Pro Text',
    fontSize: 13,
    lineHeight: 18,
    fontWeight: '500',
    color: 'black',
  },
});

export default {base};
