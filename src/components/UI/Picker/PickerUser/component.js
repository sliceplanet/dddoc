import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';

// Style
import {base} from './styles';

export default class PickerUser extends React.Component {
  renderButton = (i, title) => {
    const {index, onChange} = this.props;

    if (index === i) {
      return (
        <View style={base.btnWrap1}>
          <Text style={base.text1}>{title}</Text>
        </View>
      );
    }
    return (
      <TouchableOpacity style={base.btnWrap2} onPress={() => onChange(i)}>
        <Text style={base.text2}>{title}</Text>
      </TouchableOpacity>
    );
  };

  render() {
    return (
      <View style={base.wrap}>
        {this.renderButton(0, 'Я пользователь')}
        {this.renderButton(1, 'Я доктор')}
      </View>
    );
  }
}
