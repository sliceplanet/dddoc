import React from 'react';
import {Text} from 'react-native';
import Image from 'react-native-scalable-image';
import CountryPicker from 'react-native-country-picker-modal';

// Components
import Touchable from '../../Touchable';

// Helpers
import * as Images from '../../../../helpers/images';

// Style
import {base} from './styles';

export default class InputCountryEdit extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isVisible: false,
    };
  }

  onPress = () => this.setState({isVisible: true});
  onSelect = (country) => this.props.onChange(country.name);
  onClose = () => this.setState({isVisible: false});

  focus = () => this.setState({isVisible: true});

  render() {
    const {style, editable, title, value} = this.props;
    const {isVisible} = this.state;

    return (
      <Touchable
        editable={editable}
        style={[base.wrap, style]}
        onPress={this.onPress}>
        <Text style={base.text1}>{value}</Text>
        <Text style={base.text2}>{title}</Text>
        {editable && <Image source={Images.selector} width={22} />}
        <CountryPicker
          visible={isVisible}
          placeholder=""
          onSelect={this.onSelect}
          onClose={this.onClose}
        />
      </Touchable>
    );
  }
}
