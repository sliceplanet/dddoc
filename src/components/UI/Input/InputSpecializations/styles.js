import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  wrap: {
    flexDirection: 'row',
    width: wp(100) - 32,
    minHeight: 44,
    paddingVertical: 4,
    borderBottomWidth: 0.5,
    borderBottomColor: 'rgba(0,0,0,0.6)',
    alignItems: 'center',
  },
  tintColor: {
    tintColor: '#75C9C3',
    marginLeft: 6,
  },
  text1: {
    flex: 1,
    fontFamily: 'SF Pro Text',
    fontSize: 16,
    lineHeight: 22,
    color: 'black',
  },
  text2: {
    fontFamily: 'SF Pro Text',
    fontSize: 15,
    lineHeight: 22,
    color: '#979797',
    marginRight: 4,
  },
});

export default {base};
