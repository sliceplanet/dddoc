import React from 'react';
import {View, Text} from 'react-native';
import Image from 'react-native-scalable-image';

// Components
import Touchable from '../../Touchable';

// Helpers
import * as Images from '../../../../helpers/images';
import {navigate} from '../../../../helpers/navigation';

// Style
import {base} from './styles';

export default class InputSpecializations extends React.Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  onPress = () => {
    const {editable, one, data, value, onChange} = this.props;
    if (editable) {
      navigate('Specializations', {value, one, data, onChange});
    }
  };

  renderText = () => {
    const {value, editable, one, specializations} = this.props;
    if (!value) {
      return;
    }
    if (one) {
      return value.name;
    }
    if (!editable) {
      return value.join(', ');
    }
    let result = [];
    value.forEach((e) => {
      const s = specializations.filter((n) => n.id === e);
      if (s.length > 0) {
        result = [...result, s[0].name];
      }
    });
    return result.join(', ');
  };

  render() {
    const {style, editable, title} = this.props;

    return (
      <Touchable
        editable={editable}
        style={[base.wrap, style]}
        onPress={this.onPress}>
        <Text style={base.text1}>{this.renderText()}</Text>
        <View style={base.row}>
          <Text style={base.text2}>{title}</Text>
          {editable && <Image source={Images.selector} width={22} />}
        </View>
      </Touchable>
    );
  }
}
