import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
  },
  wrap: {
    flexDirection: 'row',
    width: wp(100) - 32,
    height: 44,
    borderBottomWidth: 0.5,
    borderBottomColor: 'rgba(0,0,0,0.6)',
    alignItems: 'center',
  },
  text1: {
    flex: 1,
    fontFamily: 'SF Pro Text',
    fontSize: 17,
    color: 'black',
    marginVertical: 8,
  },
  text2: {
    fontFamily: 'SF Pro Text',
    fontSize: 15,
    lineHeight: 22,
    color: '#979797',
    marginLeft: 4,
    marginVertical: 8,
  },
});

export default {base};
