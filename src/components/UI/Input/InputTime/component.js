import React from 'react';
import {View, Text} from 'react-native';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import moment from 'moment';

// Components
import Touchable from '../../Touchable';

// Style
import {base} from './styles';

export default class InputTime extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isVisible: false,
    };
  }

  showDatePicker = () => {
    this.setState({
      isVisible: true,
    });
  };

  hideDatePicker = () => {
    this.setState({
      isVisible: false,
    });
  };

  handleConfirm = (date) => {
    const {onChange} = this.props;
    onChange(moment(date));

    this.setState({
      isVisible: false,
    });
  };

  focus = () => {
    this.setState({
      isVisible: true,
    });
  };

  render() {
    const {style, editable, title, value} = this.props;
    const {isVisible} = this.state;

    return (
      <Touchable
        style={[base.wrap, style]}
        editable={editable}
        onPress={this.showDatePicker}>
        {value ? (
          <Text style={base.text1}>{moment(value).format('HH:mm')}</Text>
        ) : (
          <View style={base.flex} />
        )}
        <Text style={base.text2}>{title}</Text>
        <DateTimePickerModal
          isVisible={isVisible}
          mode="time"
          locale="ru-RU"
          onConfirm={this.handleConfirm}
          onCancel={this.hideDatePicker}
        />
      </Touchable>
    );
  }
}
