import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
  },
  wrap: {
    flexDirection: 'row',
    width: wp(100) - 32,
    height: 44,
    alignItems: 'center',
  },
  border: {
    borderBottomWidth: 0.5,
    borderBottomColor: 'rgba(0,0,0,0.6)',
  },
  text1: {
    flex: 1,
    fontFamily: 'SF Pro Text',
    fontSize: 17,
    color: 'black',
    marginRight: 4,
  },
  text2: {
    fontFamily: 'SF Pro Text',
    fontSize: 15,
    lineHeight: 22,
    color: '#979797',
    marginHorizontal: 4,
  },
  text3: {
    alignSelf: 'center',
    fontFamily: 'SF Pro Text',
    fontSize: 17,
    color: 'black',
    marginVertical: 2,
  },
  center: {
    alignItems: 'center',
  },
  modalWrap: {
    width: wp(50),
    backgroundColor: 'white',
    borderRadius: 5,
    padding: 16,
  },
  height: {
    maxHeight: wp(100),
  },
});

export default {base};
