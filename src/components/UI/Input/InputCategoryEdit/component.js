import React from 'react';
import {Text} from 'react-native';
import Image from 'react-native-scalable-image';

// Components
import Touchable from '../../Touchable';
import ModalCategory from '../../Modal/ModalCategory';

// Helpers
import * as Images from '../../../../helpers/images';

// Style
import {base} from './styles';

export default class InputCategoryEdit extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isVisible: false,
    };
  }

  onPress = () => this.setState({isVisible: true});
  onSelect = (value) => {
    this.props.onChange(value);
    this.onClose();
  };
  onClose = () => this.setState({isVisible: false});

  focus = () => this.setState({isVisible: true});

  render() {
    const {style, noBorder, editable, title, value} = this.props;
    const {isVisible} = this.state;

    return (
      <Touchable
        editable={editable}
        style={[base.wrap, style, !noBorder && base.border]}
        onPress={this.onPress}>
        <Text style={base.text1}>{value}</Text>
        <Text style={base.text2}>{title}</Text>
        {editable && <Image source={Images.selector} width={22} />}
        <ModalCategory
          isVisible={isVisible}
          onSelect={this.onSelect}
          onPressClose={this.onClose}
        />
      </Touchable>
    );
  }
}
