import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
    alignItems: 'center',
  },
  wrap: {
    flexDirection: 'row',
    width: wp(100) - 32,
    minHeight: 44,
    alignItems: 'center',
  },
  border: {
    borderBottomWidth: 0.5,
    borderBottomColor: 'rgba(0,0,0,0.6)',
  },
  input: {
    flex: 1,
    fontFamily: 'SF Pro Text',
    fontSize: 17,
    marginVertical: 8,
    padding: 0,
    color: 'black',
  },
  text1: {
    fontFamily: 'SF Pro Text',
    fontSize: 15,
    lineHeight: 22,
    color: '#979797',
    marginLeft: 4,
    marginVertical: 8,
  },
});

export default {base};
