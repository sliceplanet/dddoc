import React from 'react';
import {Text, TextInput, TouchableOpacity, View} from 'react-native';
import TextInputMask from 'react-native-text-input-mask';

// Style
import {base} from './styles';

export default class InputTextEdit extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isClear: false,
    };
  }

  ref = (ref) => {
    this.input = ref;
  };

  focus = () => {
    this.input.focus();
  };

  render() {
    const {
      style,
      inputStyle,
      title,
      value,
      editable,
      multiline,
      textContentType,
      autoCapitalize,
      secureTextEntry,
      keyboardType,
      returnKeyType,
      noBorder,
      mask,
      maxLength,
      onChangeText,
      onSubmitEditing,
      onBlur,
    } = this.props;

    if (mask) {
      return (
        <TouchableOpacity
          style={[base.wrap, style, !noBorder && base.border]}
          onPress={this.focus}>
          <TextInputMask
            refInput={this.ref}
            style={base.input}
            value={value}
            secureTextEntry={secureTextEntry}
            autoCapitalize={autoCapitalize || 'none'}
            textContentType={textContentType || 'none'}
            editable={editable}
            mask={mask}
            autoCorrect={false}
            underlineColorAndroid="transparent"
            clearButtonMode="while-editing"
            keyboardType={keyboardType ? keyboardType : 'default'}
            returnKeyType={returnKeyType ? returnKeyType : 'done'}
            maxLength={maxLength}
            onChangeText={onChangeText}
            onSubmitEditing={onSubmitEditing}
            onBlur={onBlur}
          />
          <Text style={base.text1}>{title}</Text>
        </TouchableOpacity>
      );
    }

    if (!editable) {
      return (
        <View style={[base.wrap, style, !noBorder && base.border]}>
          <Text style={[base.input, inputStyle]}>{value}</Text>
          <Text style={base.text1}>{title}</Text>
        </View>
      );
    }

    return (
      <TouchableOpacity
        style={[base.wrap, style, !noBorder && base.border]}
        onPress={this.focus}>
        <TextInput
          ref={this.ref}
          style={[base.input, inputStyle]}
          value={value}
          secureTextEntry={secureTextEntry}
          autoCapitalize={autoCapitalize || 'none'}
          textContentType={textContentType || 'none'}
          passwordRules={
            secureTextEntry &&
            'required: lower; required: upper; required: digit; required: [-]; minlength: 6; maxlength: 40;'
          }
          editable={editable}
          autoCorrect={false}
          multiline={multiline}
          underlineColorAndroid="transparent"
          clearButtonMode="while-editing"
          keyboardType={keyboardType ? keyboardType : 'default'}
          returnKeyType={returnKeyType ? returnKeyType : 'done'}
          maxLength={maxLength}
          onChangeText={onChangeText}
          onSubmitEditing={onSubmitEditing}
          onBlur={onBlur}
        />
        <Text style={base.text1}>{title}</Text>
      </TouchableOpacity>
    );
  }
}
