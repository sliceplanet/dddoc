import React from 'react';
import {Text, TouchableOpacity} from 'react-native';
import Image from 'react-native-scalable-image';
import CountryPicker from 'react-native-country-picker-modal';

// Helpers
import * as Images from '../../../../helpers/images';

// Style
import {base} from './styles';

export default class InputCountry extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isVisible: false,
    };
  }

  onPress = () => this.setState({isVisible: true});
  onSelect = (country) => this.props.onChange(country.name);
  onClose = () => this.setState({isVisible: false});

  focus = () => this.setState({isVisible: true});

  render() {
    const {style, title, value} = this.props;
    const {isVisible} = this.state;

    return (
      <TouchableOpacity style={[base.wrap, style]} onPress={this.onPress}>
        {value ? (
          <Text style={base.text1}>{value}</Text>
        ) : (
          <Text style={base.text2}>{title}</Text>
        )}
        <Image source={Images.selector} width={22} />
        <CountryPicker
          visible={isVisible}
          placeholder=""
          onSelect={this.onSelect}
          onClose={this.onClose}
        />
      </TouchableOpacity>
    );
  }
}
