import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
  },
  wrap: {
    flexDirection: 'row',
    width: wp(100) - 32,
    height: 60,
    borderBottomWidth: 0.5,
    borderBottomColor: 'rgba(0,0,0,0.6)',
    alignItems: 'center',
  },
  text1: {
    flex: 1,
    fontFamily: 'SF Pro Text',
    fontSize: 17,
    color: 'black',
    marginRight: 4,
  },
  text2: {
    flex: 1,
    fontFamily: 'SF Pro Text',
    fontSize: 15,
    lineHeight: 22,
    color: '#979797',
    marginRight: 4,
  },
});

export default {base};
