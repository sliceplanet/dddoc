import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
    alignItems: 'center',
  },
  wrap: {
    flexDirection: 'row',
    width: wp(100) - 32,
    minHeight: 60,
    borderBottomWidth: 0.5,
    borderBottomColor: '#3C3C43',
    alignItems: 'center',
  },
  input: {
    flex: 1,
    fontFamily: 'SF Pro Text',
    fontSize: 17,
    marginVertical: 8,
    padding: 0,
    color: 'black',
  },
  text1: {
    fontFamily: 'SF Pro Text',
    fontSize: 12,
    fontWeight: '500',
    textDecorationLine: 'underline',
    lineHeight: 18,
    color: '#D962A5',
    marginVertical: 8,
  },
});

export default {base};
