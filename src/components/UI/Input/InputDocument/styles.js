import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
  },
  wrap1: {
    flexDirection: 'row',
    width: wp(100),
    height: 44,
    backgroundColor: 'white',
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 1,
    marginBottom: 12,
    justifyContent: 'space-between',
  },
  wrap2: {
    height: 44,
    width: 96,
    backgroundColor: '#75C9C3',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  wrap3: {
    height: 44,
    width: 96,
    backgroundColor: '#D962A5',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  flexShrink: {
    flexShrink: 1,
    paddingHorizontal: 16,
  },
  text1: {
    fontFamily: 'SF Pro Text',
    fontSize: 15,
    color: 'black',
  },
  text2: {
    fontFamily: 'SF Pro Text',
    fontSize: 11,
    color: '#ACACAC',
  },
  text3: {
    fontFamily: 'SF Pro Text',
    fontSize: 16,
    color: 'white',
  },
  text4: {
    fontFamily: 'SF Pro Text',
    fontSize: 15,
    fontWeight: '600',
    color: '#75C9C3',
  },
});

export default {base};
