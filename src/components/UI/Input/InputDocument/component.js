import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import DocumentPicker from 'react-native-document-picker';
import ImagePicker from 'react-native-image-picker';

// Style
import {base} from './styles';

export default class InputDocument extends React.Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  onPressRemove = () => {
    const {onChange} = this.props;
    onChange([]);
  };

  showDocumentPicker = () => {
    const {setToast} = this.props;
    const options = {
      title: 'Выберите файл',
      noData: false,
      customButtons: [{name: 'document', title: 'Выбрать документ'}],
    };

    ImagePicker.showImagePicker(options, (response) => {
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        DocumentPicker.pick({
          type: [DocumentPicker.types.allFiles],
        }).then((result) => {
          const {value, onChange} = this.props;
          if (result.size > 5242880) {
            setToast('Размер файла превышает 5 мб.');
          }
          onChange([...value, {type: 'document', result}]);
        });
      } else if (response.fileSize > 5242880) {
        setToast('Размер файла превышает 5 мб.');
      } else {
        const {value, onChange} = this.props;
        onChange([...value, {type: 'image', response}]);
      }
    });
  };

  renderText = () => {
    const {value} = this.props;
    let result = [];
    value.forEach((e, i) => {
      switch (e.type) {
        case 'image': {
          const {fileName, type} = e.response;
          if (fileName) {
            result = [...result, `${fileName}.${type.split('/')[1]}`];
          } else {
            result = [...result, `image_${i + 1}.${type.split('/')[1]}`];
          }
          break;
        }
        case 'document': {
          const {name, type} = e.result;
          if (name) {
            result = [...result, name];
          } else {
            result = [...result, `doc_${i + 1}.${type.split('/')[1]}`];
          }
          break;
        }
      }
    });
    return result.join(', ');
  };

  render() {
    const {style, title, subTitle, subTitleGrey, value} = this.props;

    if (value.length > 0) {
      return (
        <TouchableOpacity
          style={[base.wrap1, style]}
          onPress={this.showDocumentPicker}>
          <View style={base.flexShrink}>
            <Text numberOfLines={1} style={base.text4}>
              {this.renderText()}
            </Text>
          </View>
          <TouchableOpacity style={base.wrap3} onPress={this.onPressRemove}>
            <Text style={base.text3}>Удалить</Text>
          </TouchableOpacity>
        </TouchableOpacity>
      );
    }

    return (
      <TouchableOpacity
        style={[base.wrap1, style]}
        onPress={this.showDocumentPicker}>
        <View style={base.flexShrink}>
          <Text numberOfLines={2} style={base.text1}>
            {title}
            {subTitleGrey && (
              <Text numberOfLines={1} style={base.text2}>
                {subTitleGrey}
              </Text>
            )}
          </Text>
          {subTitle && <Text style={base.text2}>{subTitle}</Text>}
        </View>
        <View style={base.wrap2}>
          <Text style={base.text3}>Выбрать</Text>
        </View>
      </TouchableOpacity>
    );
  }
}
