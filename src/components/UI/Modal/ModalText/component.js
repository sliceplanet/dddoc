import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import Modal from 'react-native-modal';

// Style
import {base} from './styles';

export default class ModalText extends React.Component {
  onPressItem = (e) => {
    const {onSelect} = this.props;
    onSelect(e);
  };

  render() {
    const {isVisible, items, onPressClose} = this.props;

    return (
      <Modal
        style={base.center}
        isVisible={isVisible}
        onBackButtonPress={onPressClose}
        onBackdropPress={onPressClose}>
        <View style={base.wrap}>
          {items.map((e, i) => {
            return (
              <TouchableOpacity
                key={i}
                style={[i < items.length - 1 && base.line, base.width]}
                onPress={() => this.onPressItem(e)}>
                <Text style={base.text}>{e.label}</Text>
              </TouchableOpacity>
            );
          })}
        </View>
      </Modal>
    );
  }
}
