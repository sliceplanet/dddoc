import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import Modal from 'react-native-modal';

// Style
import {base} from './styles';

const DATA = [15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75, 80, 85, 90];

export default class ModalDuration extends React.Component {
  onPressItem = (e) => {
    const {onSelect} = this.props;
    onSelect(e);
  };

  render() {
    const {isVisible, onPressClose} = this.props;

    return (
      <Modal
        style={base.center}
        isVisible={isVisible}
        onBackButtonPress={onPressClose}
        onBackdropPress={onPressClose}>
        <View style={base.wrap}>
          {DATA.map((e, i) => {
            return (
              <TouchableOpacity
                key={i}
                style={[i < DATA.length - 1 && base.line, base.width]}
                onPress={() => this.onPressItem(e)}>
                <Text style={base.text}>{e} мин.</Text>
              </TouchableOpacity>
            );
          })}
        </View>
      </Modal>
    );
  }
}
