import React from 'react';
import {View, Text, TouchableOpacity, ScrollView} from 'react-native';
import Modal from 'react-native-modal';

// Components
import CheckBox from '../../CheckBox';
import ButtonFull from '../../Button/ButtonFull';

// Helpers
import {countries} from '../../../../helpers/constant/countries';

// Style
import {base} from './styles';

export default class ModalCountryPicker extends React.Component {
  constructor(props) {
    super(props);

    const {value} = props;
    const city = countries.map((e) => {
      return {
        value: e.name.common,
        checked: value.filter((n) => n === e.name.common).length > 0,
      };
    });

    this.state = {
      city,
    };
  }

  onPressSelect = () => {
    const {onSelect} = this.props;
    const {city} = this.state;
    onSelect(city.filter((e) => e.checked).map((e) => e.value));
  };

  onPress = (i) => {
    let {city} = this.state;
    city[i].checked = !city[i].checked;
    this.setState({city: [...city]});
  };

  render() {
    const {isVisible, multi, onPressClose} = this.props;
    if (multi) {
      const {city} = this.state;
      return (
        <Modal
          style={base.center}
          isVisible={isVisible}
          onBackButtonPress={onPressClose}
          onBackdropPress={onPressClose}>
          <View style={base.wrap}>
            <ButtonFull title="Выбрать страны" onPress={this.onPressSelect} />
            <ScrollView>
              {city.map((e, i) => {
                return (
                  <TouchableOpacity
                    key={i}
                    style={base.row}
                    onPress={() => this.onPress(i)}>
                    <CheckBox
                      value={e.checked}
                      onChange={() => this.onPress(i)}
                    />
                    <Text numberOfLines={1} style={base.text}>
                      {e.value}
                    </Text>
                  </TouchableOpacity>
                );
              })}
            </ScrollView>
          </View>
        </Modal>
      );
    }
    return (
      <Modal
        style={base.center}
        isVisible={isVisible}
        onBackButtonPress={onPressClose}
        onBackdropPress={onPressClose}>
        <View style={base.wrap}>
          {countries.map((e, i) => {
            return null;
          })}
        </View>
      </Modal>
    );
  }
}
