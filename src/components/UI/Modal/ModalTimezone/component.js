import React from 'react';
import {View, Text, TouchableOpacity, ScrollView} from 'react-native';
import Modal from 'react-native-modal';
import Image from 'react-native-scalable-image';

// Helpers
import * as Images from '../../../../helpers/images';

// Constant
import {timezones} from '../../../../helpers/constant/timezones';

// Style
import {base} from './styles';

export default class ModalTimezone extends React.Component {
  onPressItem = (e) => {
    const {onSelect} = this.props;
    onSelect(e.timezone);
  };

  render() {
    const {isVisible, onPressClose} = this.props;

    return (
      <Modal
        style={base.center}
        isVisible={isVisible}
        onBackButtonPress={onPressClose}
        onBackdropPress={onPressClose}>
        <View style={base.wrap}>
          <Image source={Images.cross} width={32} onPress={onPressClose} />
          <ScrollView>
            {timezones.map((e, i) => {
              return (
                <TouchableOpacity
                  key={i}
                  style={[i < timezones.length - 1 && base.line]}
                  onPress={() => this.onPressItem(e)}>
                  <Text style={base.text} numberOfLines={1}>
                    {e.countries}
                  </Text>
                </TouchableOpacity>
              );
            })}
          </ScrollView>
        </View>
      </Modal>
    );
  }
}
