import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
    alignItems: 'center',
  },
  center: {
    alignItems: 'center',
    padding: 0,
    margin: 0,
  },
  width: {
    width: wp(80) - 32,
    alignItems: 'center',
  },
  backgroundColor: {
    backgroundColor: '#EFEFF4',
  },
  wrap: {
    backgroundColor: 'white',
    paddingTop: 10,
  },
  unread: {
    color: '#D962A5',
  },
  count1: {
    color: '#FF9B85',
  },
  count2: {
    color: '#D962A5',
  },
  row: {
    width: wp(100),
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'white',
    paddingVertical: 8,
    paddingHorizontal: 16,
  },
  line: {
    borderBottomColor: 'rgba(0,0,0,0.2)',
    borderBottomWidth: 1,
    marginHorizontal: 16,
  },
  text: {
    flex: 1,
    fontFamily: 'SF Pro Text',
    fontSize: 17,
    lineHeight: 20,
    color: 'black',
    marginLeft: 16,
  },
});

export default {base};
