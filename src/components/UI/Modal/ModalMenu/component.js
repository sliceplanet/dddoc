import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import Modal from 'react-native-modal';
import Image from 'react-native-scalable-image';

// Components
import Wrap from '../../Base/Wrap';
import Title from '../../Header/Title';
import Footer from '../../Footer';

// Helpers
import * as Images from '../../../../helpers/images';
import {navigate} from '../../../../helpers/navigation';

// Style
import {base} from './styles';

export default class ModalMenu extends React.Component {
  onPressProfile = () => {
    navigate('Profile');
    this.props.onPressClose();
  };

  onPressVideoCalls = () => {
    navigate('VideoCalls');
    this.props.onPressClose();
  };

  onPressMessages = () => {
    navigate('Messages');
    this.props.onPressClose();
  };

  onPressFavorite = () => {
    navigate('Favorite');
    this.props.onPressClose();
  };

  onPressBalance = () => {
    navigate('Balance');
    this.props.onPressClose();
  };

  onPressServices = () => {
    navigate('Services');
    this.props.onPressClose();
  };

  onPressReviews = () => {
    navigate('Reviews');
    this.props.onPressClose();
  };

  onPressSettings = () => {
    navigate('Settings');
    this.props.onPressClose();
  };

  onPressDoctorFinder = () => {
    navigate('DoctorFinder');
    this.props.onPressClose();
  };

  onPressLogoutUser = () => {
    const {user, playerId, fetchPostUserLogout} = this.props;
    const data = {
      playerId,
    };
    fetchPostUserLogout({user, data});
  };

  render() {
    const {isVisible, user, profile, chats, videos, onPressClose} = this.props;

    if (user.accessToken.length > 0) {
      const {role} = user.user;
      const {isVerify} = profile;

      let unread = 0;
      chats.forEach((e) => {
        if (e.unread) {
          unread += e.unread;
        }
      });
      const count1 = videos.items.filter((e) => !e.status).length;
      const count2 = videos.items.filter((e) => e.status === 1).length;

      return (
        <Modal
          style={base.center}
          isVisible={isVisible}
          animationIn="slideInRight"
          animationOut="slideOutLeft"
          swipeDirection="right"
          onBackButtonPress={onPressClose}
          onSwipeComplete={onPressClose}
          onBackdropPress={onPressClose}>
          <Wrap
            style={base.backgroundColor}
            titleView={<Title title="Меню" back onPressBack={onPressClose} />}>
            <View style={base.flex}>
              <View style={base.wrap}>
                <TouchableOpacity
                  style={base.row}
                  onPress={this.onPressProfile}>
                  <Image source={Images.menuProfile} width={29} />
                  <Text style={base.text}>Мой профиль</Text>
                  <Image source={Images.menuRight} width={8} />
                </TouchableOpacity>
                <View style={base.line} />
                <TouchableOpacity
                  style={base.row}
                  onPress={this.onPressVideoCalls}>
                  <Image source={Images.menuVideo} width={29} />
                  <Text style={base.text}>
                    Видео-звонок доктору{' '}
                    {count1 > 0 && (
                      <Text style={base.count1}>{`${count1}`}</Text>
                    )}
                    {count1 > 0 && count2 > 0 && (
                      <Text style={base.text}> / </Text>
                    )}
                    {count2 > 0 && (
                      <Text style={base.count2}>{`${count2}`}</Text>
                    )}
                  </Text>
                  <Image source={Images.menuRight} width={8} />
                </TouchableOpacity>
                <View style={base.line} />
                <TouchableOpacity
                  style={base.row}
                  onPress={this.onPressMessages}>
                  <Image source={Images.menuMessages} width={29} />
                  <Text style={base.text}>
                    Сообщения
                    {unread > 0 && (
                      <Text style={base.unread}>{` (${unread})`}</Text>
                    )}
                  </Text>
                  <Image source={Images.menuRight} width={8} />
                </TouchableOpacity>
                <View style={base.line} />
                <TouchableOpacity
                  style={base.row}
                  onPress={this.onPressFavorite}>
                  <Image source={Images.menuFavorite} width={29} />
                  <Text style={base.text}>Избранное</Text>
                  <Image source={Images.menuRight} width={8} />
                </TouchableOpacity>
                <View style={base.line} />
                <TouchableOpacity
                  style={base.row}
                  onPress={this.onPressBalance}>
                  <Image source={Images.menuPay} width={29} />
                  <Text style={base.text}>Мой счет</Text>
                  <Image source={Images.menuRight} width={8} />
                </TouchableOpacity>
                <View style={base.line} />
                <TouchableOpacity
                  style={base.row}
                  onPress={this.onPressReviews}>
                  <Image source={Images.menuReviews} width={29} />
                  <Text style={base.text}>Отзывы</Text>
                  <Image source={Images.menuRight} width={8} />
                </TouchableOpacity>
                <View style={base.line} />

                {role === 'doctor' && isVerify && (
                  <TouchableOpacity
                    style={base.row}
                    onPress={this.onPressServices}>
                    <Image source={Images.menuServices} width={29} />
                    <Text style={base.text}>Мои услуги</Text>
                    <Image source={Images.menuRight} width={8} />
                  </TouchableOpacity>
                )}
                {role === 'doctor' && isVerify && <View style={base.line} />}

                <TouchableOpacity
                  style={base.row}
                  onPress={this.onPressSettings}>
                  <Image source={Images.menuSettings} width={29} />
                  <Text style={base.text}>Настройки</Text>
                  <Image source={Images.menuRight} width={8} />
                </TouchableOpacity>
                <View style={base.line} />
                <TouchableOpacity
                  style={base.row}
                  onPress={this.onPressDoctorFinder}>
                  <Image source={Images.menuFind} width={29} />
                  <Text style={base.text}>Поиск доктора</Text>
                  <Image source={Images.menuRight} width={8} />
                </TouchableOpacity>
                <View style={base.line} />
                <TouchableOpacity
                  style={base.row}
                  onPress={this.onPressLogoutUser}>
                  <Image source={Images.menuLogout} width={29} />
                  <Text style={base.text}>Выход</Text>
                  <Image source={Images.menuRight} width={8} />
                </TouchableOpacity>
              </View>
            </View>
            <Footer onPress={onPressClose} />
          </Wrap>
        </Modal>
      );
    }
    return null;
  }
}
