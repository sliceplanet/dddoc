import {connect} from 'react-redux';
import component from './component';

import {fetchPostUserLogout} from '../../../../store/actions/auth';

function mapStateToProps(state) {
  return {
    user: state.user,
    profile: state.profile,
    playerId: state.playerId,
    chats: state.chats,
    videos: state.videos,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    fetchPostUserLogout: (data) => dispatch(fetchPostUserLogout(data)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(component);
