import React from 'react';
import {View, Text, TouchableOpacity, ScrollView} from 'react-native';
import Modal from 'react-native-modal';

// Components
import CheckBox from '../../CheckBox';
import ButtonFull from '../../Button/ButtonFull';

// Helpers
import {countries} from '../../../../helpers/constant/countries';

// Style
import {base} from './styles';

export default class ModalSpecialization extends React.Component {
  constructor(props) {
    super(props);

    const {value, specializations} = props;
    const specialization = specializations.map((e) => {
      return {...e, checked: value.filter((n) => n.id === e.id).length > 0};
    });

    this.state = {
      specialization,
    };
  }

  onPressSelect = () => {
    const {onSelect} = this.props;
    const {specialization} = this.state;
    onSelect(
      specialization
        .filter((e) => e.checked)
        .map((e) => {
          return {id: e.id, name: e.name};
        }),
    );
  };

  onPress = (i) => {
    let {specialization} = this.state;
    specialization[i].checked = !specialization[i].checked;
    this.setState({specialization: [...specialization]});
  };

  render() {
    const {isVisible, onPressClose} = this.props;
    const {specialization} = this.state;
    return (
      <Modal
        style={base.center}
        isVisible={isVisible}
        onBackButtonPress={onPressClose}
        onBackdropPress={onPressClose}>
        <View style={base.wrap}>
          <ButtonFull
            title="Выбрать специализации"
            onPress={this.onPressSelect}
          />
          <ScrollView>
            {specialization.map((e, i) => {
              return (
                <TouchableOpacity
                  key={i}
                  style={base.row}
                  onPress={() => this.onPress(i)}>
                  <CheckBox
                    value={e.checked}
                    onChange={() => this.onPress(i)}
                  />
                  <Text numberOfLines={1} style={base.text}>
                    {e.name}
                  </Text>
                </TouchableOpacity>
              );
            })}
          </ScrollView>
        </View>
      </Modal>
    );
  }
}
