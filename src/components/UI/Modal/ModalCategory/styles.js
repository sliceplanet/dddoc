import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
    alignItems: 'center',
  },
  center: {
    alignItems: 'center',
  },
  wrap: {
    alignItems: 'center',
    minWidth: wp(80),
    backgroundColor: 'white',
    borderRadius: 16,
    padding: 16,
  },
  width: {
    width: wp(80) - 32,
    alignItems: 'center',
  },
  line: {
    borderBottomColor: 'rgba(0,0,0,0.2)',
    borderBottomWidth: 0.5,
  },
  text: {
    fontFamily: 'SF Pro Text',
    fontSize: 16,
    lineHeight: 22,
    padding: 8,
  },
});

export default {base};
