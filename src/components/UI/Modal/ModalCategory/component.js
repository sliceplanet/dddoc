import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import Modal from 'react-native-modal';

// Style
import {base} from './styles';

const DATA = ['Без категории', 'Высшая', 'Первая', 'Вторая'];

export default class ModalCategory extends React.Component {
  onPressItem = (e) => {
    const {onSelect} = this.props;
    onSelect(e);
  };

  render() {
    const {isVisible, onPressClose} = this.props;

    return (
      <Modal
        style={base.center}
        isVisible={isVisible}
        onBackButtonPress={onPressClose}
        onBackdropPress={onPressClose}>
        <View style={base.wrap}>
          {DATA.map((e, i) => {
            return (
              <TouchableOpacity
                key={i}
                style={[i < DATA.length - 1 && base.line, base.width]}
                onPress={() => this.onPressItem(e)}>
                <Text style={base.text}>{e}</Text>
              </TouchableOpacity>
            );
          })}
        </View>
      </Modal>
    );
  }
}
