import React from 'react';
import {View} from 'react-native';
import Image from 'react-native-scalable-image';

// Helpers
import * as Images from '../../../helpers/images';

// Style
import {base} from './styles';

export default class Rating extends React.Component {
  render() {
    const {rating} = this.props;
    if (rating > 0) {
      let star = [];
      for (let i = 0; i < rating; i++) {
        star = [...star, <Image key={i} source={Images.star} width={12} />];
      }

      return <View style={base.wrap}>{star}</View>;
    }
    return null;
  }
}
