import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import Modal from 'react-native-modal';
import moment from 'moment';

// Components
import RatingLarge from '../../RatingLarge';
import InputText from '../../Input/InputText';

// Style
import {base} from './styles';

export default class Feedback extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      rating: 0,
      text: '',
    };
  }

  onChangeRating = (rating) => this.setState({rating});
  onChangeText = (text) => this.setState({text});

  onPressSend = () => {
    const {rating, text} = this.state;
    const {user, setToast, fetchPostReviewsId} = this.props;

    if (text.length === 0) {
      setToast('Оставить комментарий собеседнику');
      return;
    }

    if (rating === 0) {
      setToast('Оставьте оценку от 1 до 5');
      return;
    }

    const {id} = this.props.alert.extra;
    const path = {id};
    const data = {
      rating,
      text,
      date: moment().format(),
    };

    fetchPostReviewsId({user, path, data});
  };

  render() {
    const {rating, text} = this.state;

    return (
      <Modal style={base.center} isVisible>
        <View style={base.wrap1}>
          <Text style={base.text1}>Ваша оценка:</Text>
          <RatingLarge rating={rating} onChange={this.onChangeRating} />
          <InputText
            style={base.wrap2}
            value={text}
            title="Текст комментария"
            multiline
            onChangeText={this.onChangeText}
          />
          <TouchableOpacity style={base.wrap3} onPress={this.onPressSend}>
            <Text style={base.text2}>Отправить</Text>
          </TouchableOpacity>
        </View>
      </Modal>
    );
  }
}
