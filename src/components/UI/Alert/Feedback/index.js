import {connect} from 'react-redux';
import component from './component';

import {setToast} from '../../../../store/actions';
import {reduceOnPressClose} from '../../../../store/actions/alert';
import {fetchPostReviewsId} from '../../../../store/actions/reviews';

function mapStateToProps(state) {
  return {
    user: state.user,
    alert: state.alert,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setToast: (data) => dispatch(setToast(data)),
    fetchPostReviewsId: (data) => dispatch(fetchPostReviewsId(data)),
    reduceOnPressClose: () => dispatch(reduceOnPressClose()),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(component);
