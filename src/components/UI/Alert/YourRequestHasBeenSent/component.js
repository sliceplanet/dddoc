import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import Modal from 'react-native-modal';

// Style
import {base} from './styles';

export default class YourRequestHasBeenSent extends React.Component {
  render() {
    const {reduceOnPressClose} = this.props;

    return (
      <Modal style={base.center} isVisible={true}>
        <View style={base.wrap1}>
          <Text style={base.text1}>Ваш запрос отправлен</Text>
          <Text style={base.text3}>и ожидает подтверждения от доктора</Text>
          <TouchableOpacity style={base.wrap2} onPress={reduceOnPressClose}>
            <Text style={base.text2}>ОК</Text>
          </TouchableOpacity>
        </View>
      </Modal>
    );
  }
}
