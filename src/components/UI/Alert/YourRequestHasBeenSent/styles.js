import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
    alignItems: 'center',
  },
  center: {
    alignItems: 'center',
  },
  wrap1: {
    alignItems: 'center',
    minWidth: wp(80),
    backgroundColor: 'white',
    borderRadius: 16,
  },
  wrap2: {
    width: wp(80),
    alignItems: 'center',
    borderTopColor: '#EFEFF4',
    borderTopWidth: 1,
  },
  wrap3: {
    width: wp(80),
    alignItems: 'center',
  },
  text1: {
    fontFamily: 'SF Pro Text',
    fontSize: 17,
    lineHeight: 20,
    fontWeight: '600',
    color: 'black',
    paddingTop: 29,
    paddingBottom: 4,
  },
  text2: {
    fontFamily: 'SF Pro Text',
    fontSize: 15,
    fontWeight: '600',
    color: '#D962A5',
    paddingVertical: 14,
  },
  text3: {
    fontFamily: 'SF Pro Text',
    fontSize: 11,
    lineHeight: 13,
    color: 'black',
    paddingVertical: 14,
  },
});

export default {base};
