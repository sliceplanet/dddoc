import {connect} from 'react-redux';
import component from './component';

import {reduceOnPressClose} from '../../../../store/actions/alert';

function mapStateToProps(state) {
  return {
    alert: state.alert,
    user: state.user,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    reduceOnPressClose: () => dispatch(reduceOnPressClose()),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(component);
