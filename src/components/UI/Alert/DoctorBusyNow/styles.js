import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
    alignItems: 'center',
  },
  tintColor: {
    tintColor: '#ACACAC',
    alignSelf: 'flex-end',
  },
  center: {
    alignItems: 'center',
  },
  wrap1: {
    alignItems: 'center',
    minWidth: wp(80),
    backgroundColor: 'white',
    borderRadius: 16,
  },
  wrap2: {
    width: wp(80),
    alignItems: 'center',
    borderTopColor: '#EFEFF4',
    borderBottomColor: '#EFEFF4',
    borderTopWidth: 1,
    borderBottomWidth: 1,
  },
  wrap3: {
    width: wp(80),
    alignItems: 'center',
  },
  text1: {
    fontFamily: 'SF Pro Text',
    fontSize: 17,
    lineHeight: 20,
    fontWeight: '600',
    color: 'black',
    paddingTop: 18,
  },
  text2: {
    fontFamily: 'SF Pro Text',
    fontSize: 15,
    fontWeight: '600',
    color: '#75C9C3',
    paddingVertical: 14,
  },
  text3: {
    fontFamily: 'SF Pro Text',
    fontSize: 15,
    fontWeight: '600',
    color: '#D962A5',
    paddingVertical: 14,
  },
  text4: {
    fontFamily: 'SF Pro Text',
    fontSize: 13,
    lineHeight: 16,
    fontWeight: '500',
    color: 'black',
    paddingHorizontal: 22,
    paddingTop: 8,
    paddingBottom: 16,
  },
});

export default {base};
