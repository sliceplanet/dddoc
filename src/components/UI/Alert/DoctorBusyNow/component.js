import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import Modal from 'react-native-modal';
import Image from 'react-native-scalable-image';

// Helpers
import * as Images from '../../../../helpers/images';
import {navigate} from '../../../../helpers/navigation';

// Style
import {base} from './styles';

export default class DoctorBusyNow extends React.Component {
  onPressMessage = () => {
    const {_id, avatar, lastName, name, patronymic} = this.props.alert.extra;
    const user = {_id, avatar, lastName, name, patronymic};
    const {reduceOnPressClose} = this.props;
    navigate('Message', {user});
    reduceOnPressClose();
  };

  onPressBookNow = () => {
    const {_id, id} = this.props.alert.extra;
    const {user, reduceOnPressClose} = this.props;

    navigate(user.user.role === 'doctor' ? 'ServicesDoctor' : 'Services', {
      id: id ? id : _id,
      type: 'DateReservation',
    });
    reduceOnPressClose();
  };

  render() {
    const {reduceOnPressClose} = this.props;

    return (
      <Modal
        style={base.center}
        isVisible
        onBackdropPress={reduceOnPressClose}
        onBackButtonPress={reduceOnPressClose}>
        <View style={base.wrap1}>
          {/* <Image
            source={Images.cross}
            style={base.tintColor}
            width={16}
            onPress={reduceOnPressClose}
          /> */}
          <Text style={base.text1}>Доктор сейчас занят</Text>
          <Text style={base.text4}>
            Вы можете согласовать время звонка написав сообщение или нажав
            кнопку «Забронировать»
          </Text>
          <TouchableOpacity style={base.wrap2} onPress={this.onPressMessage}>
            <Text style={base.text2}>Написать сообщение</Text>
          </TouchableOpacity>
          <TouchableOpacity style={base.wrap3} onPress={this.onPressBookNow}>
            <Text style={base.text3}>Забронировать</Text>
          </TouchableOpacity>
        </View>
      </Modal>
    );
  }
}
