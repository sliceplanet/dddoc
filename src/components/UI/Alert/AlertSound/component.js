import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import Modal from 'react-native-modal';

// Helpers
import {navigate} from '../../../../helpers/navigation';

// Api
import {
  apiPutVideosIdAccept,
  apiPutVideosIdDecline,
} from '../../../../store/api/videos';

// Style
import {base} from './styles';

export default class AlertSound extends React.Component {
  onPressAccept = () => {
    const {alert, user, reduceOnPressClose} = this.props;

    apiPutVideosIdAccept({
      path: {id: alert.notify.extra.id},
      user,
    })
      .then((result) => console.log(result))
      .catch((e) => console.log(e.response));
    navigate('Call', {channelName: alert.notify.extra.id});
    global.whoosh.pause();
    reduceOnPressClose();
  };

  onPressCancel = () => {
    const {alert, user, reduceOnPressClose} = this.props;

    apiPutVideosIdDecline({
      path: {id: alert.notify.extra.id},
      user,
    })
      .then((result) => console.log(result))
      .catch((e) => console.log(e.response));
    global.whoosh.pause();
    reduceOnPressClose();
  };

  render() {
    return (
      <Modal style={base.center} isVisible>
        <View style={base.wrap1}>
          <Text style={base.text1}>Желаете принять звонок?</Text>
          <TouchableOpacity style={base.wrap2} onPress={this.onPressAccept}>
            <Text style={base.text2}>Да</Text>
          </TouchableOpacity>
          <TouchableOpacity style={base.wrap3} onPress={this.onPressCancel}>
            <Text style={base.text3}>Нет</Text>
          </TouchableOpacity>
        </View>
      </Modal>
    );
  }
}
