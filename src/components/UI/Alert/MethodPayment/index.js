import {connect} from 'react-redux';
import component from './component';

import {setToast} from '../../../../store/actions';
import {reduceOnPressClose} from '../../../../store/actions/alert';
import {
  fetchPutTransfer,
  fetchPutVideoIdPayment,
} from '../../../../store/actions/balance';

function mapStateToProps(state) {
  return {
    user: state.user,
    balance: state.balance.balance,
    alert: state.alert,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setToast: (data) => dispatch(setToast(data)),
    reduceOnPressClose: () => dispatch(reduceOnPressClose()),
    fetchPutTransfer: (data) => dispatch(fetchPutTransfer(data)),
    fetchPutVideoIdPayment: (data) => dispatch(fetchPutVideoIdPayment(data)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(component);
