import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
    alignItems: 'center',
  },
  tintColor: {
    tintColor: '#ACACAC',
    alignSelf: 'flex-end',
  },
  center: {
    alignItems: 'center',
  },
  btn: {
    width: wp(80) - 32,
    marginBottom: 16,
  },
  padding: {
    marginBottom: 20,
  },
  wrap1: {
    alignItems: 'center',
    minWidth: wp(80),
    backgroundColor: 'white',
    borderRadius: 16,
  },
  wrap2: {
    width: wp(80),
    alignItems: 'center',
    borderTopColor: '#EFEFF4',
    borderBottomColor: '#EFEFF4',
    borderTopWidth: 1,
    borderBottomWidth: 1,
    paddingHorizontal: 32,
    marginVertical: 16,
  },
  wrap3: {
    width: wp(80),
    alignItems: 'center',
    paddingHorizontal: 16,
    paddingVertical: 20,
    marginBottom: 20,
    borderTopColor: '#EFEFF4',
    borderBottomColor: '#EFEFF4',
    borderTopWidth: 1,
    borderBottomWidth: 1,
  },
  wrap4: {
    width: wp(80),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    paddingHorizontal: 16,
  },
  text1: {
    fontFamily: 'SF Pro Text',
    fontSize: 17,
    lineHeight: 20,
    fontWeight: '600',
    color: '#D962A5',
    paddingTop: 29,
  },
  text2: {
    fontFamily: 'SF Pro Text',
    fontSize: 11,
    lineHeight: 13,
    color: 'black',
    paddingVertical: 8,
  },
  text3: {
    fontFamily: 'SF Pro Text',
    fontSize: 11,
    lineHeight: 13,
    color: '#D962A5',
    marginTop: 20,
    textDecorationColor: '#D962A5',
    textDecorationLine: 'underline',
  },
  text4: {
    fontFamily: 'SF Pro Text',
    fontSize: 15,
    fontWeight: '600',
    lineHeight: 18,
    color: '#D962A5',
  },
  text5: {
    fontFamily: 'SF Pro Text',
    fontSize: 11,
    lineHeight: 13,
    color: '#ACACAC',
    marginTop: 4,
  },
});

export default {base};
