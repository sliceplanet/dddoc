import React from 'react';
import {View, Alert, Text, TouchableOpacity} from 'react-native';
import Modal from 'react-native-modal';
import Image from 'react-native-scalable-image';

// Helpers
import * as Images from '../../../../helpers/images';

// Style
import {base} from './styles';

export default class MethodPayment extends React.Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  onPressOtherPay = () => {
    const {alert, user, fetchPutVideoIdPayment} = this.props;
    const path = {
      videoId: alert.extra.id,
    };
    fetchPutVideoIdPayment({user, path});
  };

  onPressInteriorPay = () => {
    Alert.alert(
      'Оплата видео звонка',
      'Желаете оплатить видео звонок?',
      [{text: 'Да', onPress: this.interiorPay}, {text: 'Нет'}],
      {cancelable: false},
    );
  };

  interiorPay = () => {
    const {balance, alert, user, fetchPutTransfer} = this.props;
    if (balance > 0) {
      const data = {
        videoId: alert.extra.id,
      };
      fetchPutTransfer({user, data});
    }
  };

  render() {
    const {balance, reduceOnPressClose} = this.props;

    return (
      <Modal
        style={base.center}
        isVisible
        onBackdropPress={reduceOnPressClose}
        onBackButtonPress={reduceOnPressClose}>
        <View style={base.wrap1}>
          {/* <Image
            source={Images.cross}
            style={base.tintColor}
            width={16}
            onPress={reduceOnPressClose}
          /> */}
          <Text style={base.text1}>Методы оплаты</Text>
          <Text style={base.text2}>Выбор метода оплаты</Text>

          <View style={base.wrap3}>
            <View style={base.wrap4}>
              <Image source={Images.visa} width={41} />
              <Image source={Images.mastercard} width={26} />
              <Image source={Images.mir} width={44} />
              <Image source={Images.yandexPay} width={16} />
              <Image source={Images.qiwi} height={20} />
            </View>
            <TouchableOpacity onPress={this.onPressOtherPay}>
              <Text style={base.text3}>Выбрать другой способ оплаты</Text>
            </TouchableOpacity>
          </View>
          <TouchableOpacity
            style={base.padding}
            disabled={balance === 0}
            onPress={this.onPressInteriorPay}>
            <Text style={base.text4}>Внутренний счет</Text>
            {balance === 0 && (
              <Text style={base.text5}>(Недостаточно средств)</Text>
            )}
          </TouchableOpacity>
        </View>
      </Modal>
    );
  }
}
