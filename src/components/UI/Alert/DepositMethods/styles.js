import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
    alignItems: 'center',
  },
  tintColor: {
    tintColor: '#ACACAC',
  },
  center: {
    alignItems: 'center',
  },
  btn: {
    width: wp(80) - 32,
    marginBottom: 16,
  },
  wrap1: {
    alignItems: 'center',
    minWidth: wp(80),
    backgroundColor: 'white',
    borderRadius: 16,
  },
  wrap2: {
    width: wp(80),
    alignItems: 'center',
    borderTopColor: '#EFEFF4',
    borderBottomColor: '#EFEFF4',
    borderTopWidth: 1,
    borderBottomWidth: 1,
    paddingHorizontal: 32,
    marginVertical: 16,
  },
  wrap3: {
    width: wp(80),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    paddingHorizontal: 16,
    paddingBottom: 16,
  },
  text1: {
    fontFamily: 'SF Pro Text',
    fontSize: 17,
    lineHeight: 20,
    fontWeight: '600',
    color: '#D962A5',
    paddingTop: 29,
    paddingBottom: 4,
  },
  text2: {
    fontFamily: 'SF Pro Text',
    fontSize: 11,
    lineHeight: 13,
    color: 'black',
  },
});

export default {base};
