import React from 'react';
import {View, Text} from 'react-native';
import Modal from 'react-native-modal';
import Image from 'react-native-scalable-image';

// Components
import InputText from '../../Input/InputText';
import ButtonFull from '../../Button/ButtonFull';

// Helpers
import * as Images from '../../../../helpers/images';

// Style
import {base} from './styles';

export default class DepositMethods extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      pay: '',
    };
  }

  onPressReplenish = () => {
    const {user, setToast, fetchPostRefill} = this.props;
    const pay = parseFloat(this.state.pay);

    if (!pay || pay <= 0) {
      setToast('Сума не валидная');
      return;
    }

    const data = {
      sum: `${pay}`,
    };
    fetchPostRefill({user, data});
  };

  onChangePay = (pay) => this.setState({pay});

  render() {
    const {pay} = this.state;
    const {reduceOnPressClose} = this.props;

    return (
      <Modal
        style={base.center}
        isVisible
        onBackdropPress={reduceOnPressClose}
        onBackButtonPress={reduceOnPressClose}>
        <View style={base.wrap1}>
          {/* <Image
            source={Images.cross}
            style={base.tintColor}
            width={16}
            onPress={reduceOnPressClose}
          /> */}
          <Text style={base.text1}>Методы пополнения</Text>
          <Text style={base.text2}>
            Добавить необходимость ввода суммы пополнения
          </Text>
          <InputText
            style={base.wrap2}
            title="Введите сумму пополнения"
            value={pay}
            keyboardType="numeric"
            onChangeText={this.onChangePay}
          />
          <View style={base.wrap3}>
            <Image source={Images.visa} width={41} />
            <Image source={Images.mastercard} width={26} />
            <Image source={Images.mir} width={44} />
            <Image source={Images.yandexPay} width={16} />
            <Image source={Images.qiwi} height={20} />
          </View>
          <ButtonFull
            style={base.btn}
            title="Пополнить"
            onPress={this.onPressReplenish}
          />
        </View>
      </Modal>
    );
  }
}
