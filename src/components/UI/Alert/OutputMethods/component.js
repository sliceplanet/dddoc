import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import Modal from 'react-native-modal';
import Image from 'react-native-scalable-image';

// Helpers
import * as Images from '../../../../helpers/images';

// Style
import {base} from './styles';

export default class OutputMethods extends React.Component {
  onPressCard = () => {
    const {reducerMethodCard} = this.props;
    reducerMethodCard();
  };

  onPressPayeer = () => {
    const {reducerMethodPayeer} = this.props;
    reducerMethodPayeer();
  };

  render() {
    const {reduceOnPressClose} = this.props;

    return (
      <Modal
        style={base.center}
        isVisible
        onBackdropPress={reduceOnPressClose}
        onBackButtonPress={reduceOnPressClose}>
        <View style={base.wrap1}>
          {/* <Image
            source={Images.cross}
            style={base.tintColor}
            width={16}
            onPress={reduceOnPressClose}
          /> */}
          <Text style={base.text1}>Методы вывода</Text>
          <Text style={base.text2}>Выбор метода вывода средств</Text>

          <TouchableOpacity style={base.wrap4} onPress={this.onPressCard}>
            <View style={base.wrap3}>
              <Image source={Images.visa} width={41} />
              <Image source={Images.mastercard} width={26} />
            </View>
            <Text style={base.text3}>На карту (RUB)</Text>
            <Text style={base.text4}>(комиссия 3-5%)</Text>
          </TouchableOpacity>
          <TouchableOpacity style={base.wrap5} onPress={this.onPressPayeer}>
            <Image source={Images.payeer} height={16} />
            <Text style={base.text3}>На кошелек (USD)</Text>
            <Text style={base.text5}>
              рекомендованный метод (комиссия 0,5%)
            </Text>
          </TouchableOpacity>
        </View>
      </Modal>
    );
  }
}
