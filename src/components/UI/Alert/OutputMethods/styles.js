import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
    alignItems: 'center',
  },
  tintColor: {
    tintColor: '#ACACAC',
    alignSelf: 'flex-end',
  },
  center: {
    alignItems: 'center',
  },
  btn: {
    width: wp(80) - 32,
    marginBottom: 16,
  },
  wrap1: {
    alignItems: 'center',
    minWidth: wp(80),
    backgroundColor: 'white',
    borderRadius: 16,
  },
  wrap2: {
    width: wp(80),
    alignItems: 'center',
    borderTopColor: '#EFEFF4',
    borderBottomColor: '#EFEFF4',
    borderTopWidth: 1,
    borderBottomWidth: 1,
    paddingHorizontal: 32,
    marginVertical: 16,
  },
  wrap3: {
    width: wp(33),
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  wrap4: {
    width: wp(80),
    alignItems: 'center',
    borderTopColor: '#EFEFF4',
    borderBottomColor: '#EFEFF4',
    borderTopWidth: 1,
    borderBottomWidth: 1,
    paddingVertical: 16,
    marginVertical: 16,
  },
  wrap5: {
    width: wp(80),
    alignItems: 'center',
    marginBottom: 16,
  },
  text1: {
    fontFamily: 'SF Pro Text',
    fontSize: 17,
    lineHeight: 20,
    fontWeight: '600',
    color: '#75C9C3',
    paddingTop: 29,
    paddingBottom: 4,
  },
  text2: {
    fontFamily: 'SF Pro Text',
    fontSize: 11,
    lineHeight: 13,
    color: 'black',
  },
  text3: {
    fontFamily: 'SF Pro Text',
    fontSize: 15,
    lineHeight: 18,
    fontWeight: '600',
    color: '#D962A5',
    marginVertical: 4,
  },
  text4: {
    fontFamily: 'SF Pro Text',
    fontSize: 11,
    lineHeight: 13,
    color: '#ACACAC',
  },
  text5: {
    fontFamily: 'SF Pro Text',
    fontSize: 11,
    lineHeight: 13,
    color: '#ACACAC',
  },
});

export default {base};
