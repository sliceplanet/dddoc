import {connect} from 'react-redux';
import component from './component';

import {setToast} from '../../../../store/actions';
import {
  reduceOnPressClose,
  reducerMethodCard,
  reducerMethodPayeer,
} from '../../../../store/actions/alert';
import {fetchPostRefill} from '../../../../store/actions/balance';

function mapStateToProps(state) {
  return {
    user: state.user,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setToast: (data) => dispatch(setToast(data)),
    reduceOnPressClose: () => dispatch(reduceOnPressClose()),
    reducerMethodCard: () => dispatch(reducerMethodCard()),
    reducerMethodPayeer: () => dispatch(reducerMethodPayeer()),
    fetchPostRefill: (data) => dispatch(fetchPostRefill(data)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(component);
