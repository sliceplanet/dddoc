import {connect} from 'react-redux';
import component from './component';

import {reduceOnPressClose} from '../../../../store/actions/alert';

function mapDispatchToProps(dispatch) {
  return {
    reduceOnPressClose: () => dispatch(reduceOnPressClose()),
  };
}

export default connect(null, mapDispatchToProps)(component);
