import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import Modal from 'react-native-modal';

// Style
import {base} from './styles';

export default class AlertMessage extends React.Component {
  render() {
    const {reduceOnPressClose} = this.props;

    return (
      <Modal style={base.center} isVisible>
        <View style={base.wrap1}>
          <Text style={base.text1}>
            У вас есть непрочитанные сообщения. Для просмотра перейдите в раздел
            "Сообщения"
          </Text>
          <TouchableOpacity style={base.wrap2} onPress={reduceOnPressClose}>
            <Text style={base.text2}>ОК</Text>
          </TouchableOpacity>
        </View>
      </Modal>
    );
  }
}
