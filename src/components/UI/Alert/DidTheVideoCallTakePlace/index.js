import {connect} from 'react-redux';
import component from './component';

import {
  reduceOnPressClose,
  reducerGiveFeedback,
} from '../../../../store/actions/alert';

import {
  reducerAnswerClient,
  reducerAnswerDoctor,
} from '../../../../store/actions/videos';

function mapStateToProps(state) {
  return {
    alert: state.alert,
    user: state.user,
    videos: state.videos,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    reduceOnPressClose: () => dispatch(reduceOnPressClose()),
    reducerGiveFeedback: () => dispatch(reducerGiveFeedback()),
    reducerAnswerClient: (id) => dispatch(reducerAnswerClient(id)),
    reducerAnswerDoctor: (id) => dispatch(reducerAnswerDoctor(id)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(component);
