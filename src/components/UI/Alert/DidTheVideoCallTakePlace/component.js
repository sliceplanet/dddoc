import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import Modal from 'react-native-modal';

// Api
import {apiPutVideosId} from '../../../../store/api/videos';

// Style
import {base} from './styles';

export default class DidTheVideoCallTakePlace extends React.Component {
  onPressAccept = () => {
    const {
      user,
      reducerGiveFeedback,
      reducerAnswerDoctor,
      reducerAnswerClient,
    } = this.props;
    const {id} = this.props.alert.extra;
    const path = {
      id,
    };
    const data = {
      answer: true,
    };
    apiPutVideosId({user, path, data}).finally(() => {
      reducerGiveFeedback();

      const video = this.props.videos.items.filter((e) => e.id === id)[0];
      if (video.clientId === user.user.id) {
        reducerAnswerClient(id);
      } else {
        reducerAnswerDoctor(id);
      }
    });
  };

  onPressCancel = () => {
    const {user, reduceOnPressClose} = this.props;
    const {id} = this.props.alert.extra;
    const path = {
      id,
    };
    const data = {
      answer: false,
    };
    apiPutVideosId({user, path, data}).finally(reduceOnPressClose);
  };

  render() {
    return (
      <Modal style={base.center} isVisible>
        <View style={base.wrap1}>
          <Text style={base.text1}>Состоялся ли видео-звонок?</Text>
          <TouchableOpacity style={base.wrap2} onPress={this.onPressAccept}>
            <Text style={base.text2}>ДА</Text>
          </TouchableOpacity>
          <TouchableOpacity style={base.wrap3} onPress={this.onPressCancel}>
            <Text style={base.text3}>НЕТ</Text>
          </TouchableOpacity>
        </View>
      </Modal>
    );
  }
}
