import React from 'react';

// Components
import AlertVideoCallRequest from './AlertVideoCallRequest';
import YourRequestHasBeenSent from './YourRequestHasBeenSent';
import DidTheVideoCallTakePlace from './DidTheVideoCallTakePlace';
import GiveFeedback from './GiveFeedback';
import BookingConfirmation from './BookingConfirmation';
import DoctorBusyNow from './DoctorBusyNow';
import Feedback from './Feedback';
import DepositMethods from './DepositMethods';
import OutputMethods from './OutputMethods';
import MethodCard from './MethodCard';
import MethodPayeer from './MethodPayeer';
import AlertMessage from './AlertMessage';
import BookingConfirmed from './BookingConfirmed';
import CallConfirmed from './CallConfirmed';
import MethodPayment from './MethodPayment';
import ClientPay from './ClientPay';
import DoctorPay from './DoctorPay';
import AlertSound from './AlertSound';

export default class Alert extends React.Component {
  render() {
    const {alert} = this.props;
    switch (alert.type) {
      case 'AlertVideoCallRequest': {
        return <AlertVideoCallRequest />;
      }
      case 'AlertSound': {
        return <AlertSound />;
      }
      case 'YourRequestHasBeenSent': {
        return <YourRequestHasBeenSent />;
      }
      case 'DidTheVideoCallTakePlace': {
        return <DidTheVideoCallTakePlace />;
      }
      case 'GiveFeedback': {
        return <GiveFeedback />;
      }
      case 'BookingConfirmation': {
        return <BookingConfirmation />;
      }
      case 'DoctorBusyNow': {
        return <DoctorBusyNow />;
      }
      case 'Feedback': {
        return <Feedback />;
      }
      case 'DepositMethods': {
        return <DepositMethods />;
      }
      case 'OutputMethods': {
        return <OutputMethods />;
      }
      case 'MethodCard': {
        return <MethodCard />;
      }
      case 'MethodPayeer': {
        return <MethodPayeer />;
      }
      case 'AlertMessage': {
        return <AlertMessage />;
      }
      case 'BookingConfirmed': {
        return <BookingConfirmed />;
      }
      case 'CallConfirmed': {
        return <CallConfirmed />;
      }
      case 'MethodPayment': {
        return <MethodPayment />;
      }
      case 'ClientPay': {
        return <ClientPay />;
      }
      case 'DoctorPay': {
        return <DoctorPay />;
      }
    }

    return null;
  }
}
