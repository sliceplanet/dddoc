import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
    alignItems: 'center',
  },
  center: {
    alignItems: 'center',
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  tintColor: {
    tintColor: '#ACACAC',
  },
  bv: {
    borderTopWidth: 0.7,
    borderTopColor: '#EFEFF4',
    borderBottomWidth: 0.7,
    borderBottomColor: '#EFEFF4',
    marginTop: 12,
  },
  wrap1: {
    minWidth: wp(80),
    backgroundColor: 'white',
    borderRadius: 16,
    overflow: 'hidden',
  },
  wrap2: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: 16,
    paddingRight: 8,
    paddingTop: 7,
    paddingBottom: 3,
    backgroundColor: '#EFEFF4',
  },
  wrap3: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 16,
    paddingVertical: 8,
  },
  wrap4: {
    paddingHorizontal: 16,
  },
  wrap5: {
    paddingHorizontal: 16,
    flexDirection: 'row',
  },
  text1: {
    fontFamily: 'SF Pro Text',
    fontSize: 11,
    lineHeight: 13,
    color: 'rgba(0, 0, 0, 0.4)',
  },
  text2: {
    fontFamily: 'SF Pro Text',
    fontSize: 13,
    lineHeight: 15,
    color: 'black',
    fontWeight: '600',
  },
  text3: {
    fontFamily: 'SF Pro Text',
    fontSize: 13,
    lineHeight: 15,
    color: '#D962A5',
    marginTop: 2,
  },
  text4: {
    fontFamily: 'SF Pro Text',
    fontSize: 11,
    lineHeight: 18,
    color: 'black',
  },
  text5: {
    fontFamily: 'SF Pro Text',
    fontSize: 11,
    lineHeight: 18,
    fontWeight: '600',
    color: '#D962A5',
  },
  text6: {
    alignSelf: 'center',
    fontFamily: 'SF Pro Text',
    fontSize: 15,
    lineHeight: 18,
    fontWeight: '600',
    color: '#D962A5',
    paddingVertical: 15,
  },
  text7: {
    alignSelf: 'center',
    fontFamily: 'SF Pro Text',
    fontSize: 15,
    lineHeight: 18,
    fontWeight: '600',
    color: '#75C9C3',
    paddingVertical: 15,
  },
});

export default {base};
