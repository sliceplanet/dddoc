import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import Modal from 'react-native-modal';
import Image from 'react-native-scalable-image';
import moment from 'moment';

// Components
import AvatarDoctorFinder from '../../Avatar/AvatarDoctorFinder';

// Helpers
import * as Images from '../../../../helpers/images';
import {URL} from '../../../../store/api';

// Style
import {base} from './styles';

export default class BookingConfirmation extends React.Component {
  onPressAccept = () => {
    const {
      alert,
      user,
      fetchPutVideosIdConfirm,
      reducerVideosConfirm,
      reduceOnPressClose,
    } = this.props;
    const {id} = alert.extra;
    const path = {
      id,
    };

    fetchPutVideosIdConfirm({user, path});
    reducerVideosConfirm(alert);
    reduceOnPressClose();
  };

  onPressCancel = () => {
    const {
      alert,
      user,
      fetchPutVideosIdCancel,
      reducerVideosCancel,
      reduceOnPressClose,
    } = this.props;
    const {id} = alert.extra;
    const path = {
      id,
    };

    fetchPutVideosIdCancel({user, path});
    reducerVideosCancel(alert);
    reduceOnPressClose();
  };

  render() {
    const {alert, reduceOnPressClose} = this.props;
    const {avatar, name, lastName, patronymic, date} = alert.extra;
    console.log(alert.extra);

    return (
      <Modal style={base.center} isVisible>
        <View style={base.wrap1}>
          <View style={base.wrap2}>
            <Text style={base.text1}>УВЕДОМЛЕНИЕ</Text>
            <View style={base.flex} />
            <Image
              source={Images.cross}
              style={base.tintColor}
              width={16}
              onPress={reduceOnPressClose}
            />
          </View>
          <View style={base.wrap3}>
            <AvatarDoctorFinder
              source={avatar && {uri: `${URL}${avatar}`}}
              online
            />
            <View style={base.wrap4}>
              <Text style={base.text2}>
                {name} {lastName}
                {'\n'}
                {patronymic}
              </Text>
              <Text style={base.text3}>забронировал{'\n'}видео-звонок</Text>
            </View>
          </View>
          <View style={base.wrap5}>
            <Text style={base.text4}>Дата консультации:</Text>
            <View style={base.flex} />
            <Text style={base.text5}>{moment(date).format('DD.MM.YYYY')}</Text>
          </View>
          <View style={base.wrap5}>
            <Text style={base.text4}>Время консультации:</Text>
            <View style={base.flex} />
            <Text style={base.text5}>{moment(date).format('HH:mm')}</Text>
          </View>
          <TouchableOpacity style={base.bv} onPress={this.onPressAccept}>
            <Text style={base.text6}>Подтвердить</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={this.onPressCancel}>
            <Text style={base.text7}>Отклонить</Text>
          </TouchableOpacity>
        </View>
      </Modal>
    );
  }
}
