import {connect} from 'react-redux';
import component from './component';

import {
  reduceOnPressClose,
  reducerFeedback,
} from '../../../../store/actions/alert';

function mapDispatchToProps(dispatch) {
  return {
    reduceOnPressClose: () => dispatch(reduceOnPressClose()),
    reducerFeedback: () => dispatch(reducerFeedback()),
  };
}

export default connect(null, mapDispatchToProps)(component);
