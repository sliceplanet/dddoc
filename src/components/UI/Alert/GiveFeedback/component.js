import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import Modal from 'react-native-modal';

// Style
import {base} from './styles';

export default class GiveFeedback extends React.Component {
  onPressAccept = () => {
    const {reducerFeedback} = this.props;
    reducerFeedback();
  };

  render() {
    const {reduceOnPressClose} = this.props;

    return (
      <Modal style={base.center} isVisible>
        <View style={base.wrap1}>
          <Text style={base.text1}>Оставить отзыв собеседнику?</Text>
          <TouchableOpacity style={base.wrap2} onPress={this.onPressAccept}>
            <Text style={base.text2}>ДА</Text>
          </TouchableOpacity>
          <TouchableOpacity style={base.wrap3} onPress={reduceOnPressClose}>
            <Text style={base.text3}>НЕТ</Text>
          </TouchableOpacity>
        </View>
      </Modal>
    );
  }
}
