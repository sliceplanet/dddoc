import {connect} from 'react-redux';
import component from './component';

import {setToast} from '../../../../store/actions';
import {reduceOnPressClose} from '../../../../store/actions/alert';
import {
  fetchPostRefill,
  fetchPostPayout,
} from '../../../../store/actions/balance';

function mapStateToProps(state) {
  return {
    user: state.user,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setToast: (data) => dispatch(setToast(data)),
    reduceOnPressClose: () => dispatch(reduceOnPressClose()),
    fetchPostRefill: (data) => dispatch(fetchPostRefill(data)),
    fetchPostPayout: (data) => dispatch(fetchPostPayout(data)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(component);
