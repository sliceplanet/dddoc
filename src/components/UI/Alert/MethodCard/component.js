import React from 'react';
import {View, Text, TouchableOpacity, Keyboard} from 'react-native';
import Modal from 'react-native-modal';
import Image from 'react-native-scalable-image';

// Components
import InputText from '../../Input/InputText';
import ButtonFullSmall from '../../Button/ButtonFullSmall';

// Helpers
import * as Images from '../../../../helpers/images';

import {apiGetBinlist} from '../../../../store/api/binlookup';

// Style
import {base} from './styles';

export default class MethodCard extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      pay: -1,
      card: '',
      sum: '',
    };
  }

  onChangeCard = (mask, card) => this.setState({card});
  onChangeSum = (sum) => this.setState({sum});

  refSum = (ref) => (this.sum = ref);

  nextSum = () => this.sum.focus();

  done = () => {
    Keyboard.dismiss();
    const {user, setToast, fetchPostPayout} = this.props;
    const {card, sum} = this.state;
    const pay = parseFloat(this.state.pay);
    if (card.length === 0) {
      setToast('Введите номер карты');
      return;
    }
    // if (!validator.card(card)) {
    //   setToast('Введите номер карты');
    //   return;
    // }
    if (pay === -1) {
      setToast('Выберите систему оплаты');
      return;
    }
    if (!sum || sum <= 0) {
      setToast('Сума не валидная');
      return;
    }

    const path = {
      // curOut: 'RUB',
      accountNumber: card,
      sum,
    };
    fetchPostPayout({user, path, pay});
  };

  render() {
    const {card, sum, pay} = this.state;
    const {reduceOnPressClose} = this.props;

    return (
      <Modal
        style={base.center}
        isVisible
        onBackdropPress={reduceOnPressClose}
        onBackButtonPress={reduceOnPressClose}>
        <View style={base.wrap1}>
          {/* <Image
            source={Images.cross}
            style={base.tintColor}
            width={16}
            onPress={reduceOnPressClose}
          /> */}
          <Text style={base.text1}>Методы вывода</Text>
          <Text style={base.text2}>Выбор метода вывода средств</Text>

          <View style={base.wrap4} onPress={this.onPressCard}>
            <View style={base.wrap3}>
              <TouchableOpacity
                onPress={() => this.setState({pay: 0})}
                style={[base.padding, pay === 0 && base.select]}>
                <Image source={Images.visa} width={41} />
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => this.setState({pay: 1})}
                style={[base.padding, pay === 1 && base.select]}>
                <Image source={Images.mastercard} width={26} />
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => this.setState({pay: 2})}
                style={[base.padding, pay === 2 && base.select]}>
                <Image source={Images.mir} width={44} />
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => this.setState({pay: 3})}
                style={[base.padding, pay === 3 && base.select]}>
                <Image source={Images.yandexPay} width={16} />
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => this.setState({pay: 4})}
                style={[base.padding, pay === 4 && base.select]}>
                <Image source={Images.qiwi} height={20} />
              </TouchableOpacity>
            </View>
          </View>

          <InputText
            style={base.input}
            inputStyle={base.inputStyle}
            title={`Введите номер ${pay === 3 ? 'кошелька' : 'карты'}`}
            value={card}
            mask="[0000] [0000] [0000] [0000]"
            onChangeText={this.onChangeCard}
            onSubmitEditing={this.nextSum}
          />

          <InputText
            ref={this.refSum}
            style={base.input}
            inputStyle={base.inputStyle}
            title="Введите сумму"
            value={sum}
            onChangeText={this.onChangeSum}
            onSubmitEditing={this.done}
          />

          <ButtonFullSmall
            style={base.btn}
            title="Вывести"
            onPress={this.done}
          />
        </View>
      </Modal>
    );
  }
}
