import {connect} from 'react-redux';
import component from './component';

import {setToast} from '../../../../store/actions';
import {reduceOnPressClose} from '../../../../store/actions/alert';
import {
  fetchPostRefill,
  fetchPostTransfer,
} from '../../../../store/actions/balance';

function mapStateToProps(state) {
  return {
    user: state.user,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setToast: (data) => dispatch(setToast(data)),
    reduceOnPressClose: () => dispatch(reduceOnPressClose()),
    fetchPostRefill: (data) => dispatch(fetchPostRefill(data)),
    fetchPostTransfer: (data) => dispatch(fetchPostTransfer(data)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(component);
