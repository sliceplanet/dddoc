import React from 'react';
import {View, Text, Keyboard} from 'react-native';
import Modal from 'react-native-modal';
import Image from 'react-native-scalable-image';

// Components
import InputText from '../../Input/InputText';
import ButtonFullSmall from '../../Button/ButtonFullSmall';

// Helpers
import * as Images from '../../../../helpers/images';
import * as validator from '../../../../helpers/validator';

// Style
import {base} from './styles';

export default class MethodPayeer extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      card: '',
      sum: '',
    };
  }

  onChangeCard = (card) => this.setState({card});
  onChangeSum = (sum) => this.setState({sum});

  done = () => {
    Keyboard.dismiss();
    const {user, setToast, fetchPostTransfer} = this.props;
    const {card} = this.state;
    const sum = parseFloat(this.state.sum);

    if (!validator.card(card)) {
      setToast('Введите номер счета');
      return;
    }
    if (!sum || sum <= 0) {
      setToast('Сума не валидная');
      return;
    }

    const path = {
      curOut: 'USD',
      to: card,
      sum,
    };
    fetchPostTransfer({user, path});
  };

  render() {
    const {card, sum} = this.state;
    const {reduceOnPressClose} = this.props;

    return (
      <Modal
        style={base.center}
        isVisible
        onBackdropPress={reduceOnPressClose}
        onBackButtonPress={reduceOnPressClose}>
        <View style={base.wrap1}>
          {/* <Image
            source={Images.cross}
            style={base.tintColor}
            width={16}
            onPress={reduceOnPressClose}
          /> */}
          <Text style={base.text1}>Методы вывода</Text>
          <Text style={base.text2}>Выбор метода вывода средств</Text>

          <View style={base.wrap4} onPress={this.onPressCard}>
            <View style={base.wrap3}>
              <Image source={Images.payeer} height={20} />
            </View>
          </View>

          <InputText
            style={base.input}
            inputStyle={base.inputStyle}
            title="Введите ваш номер счета"
            value={card}
            onChangeText={this.onChangeCard}
          />
          <InputText
            style={base.input}
            inputStyle={base.inputStyle}
            title="Введите сумму"
            value={sum}
            onChangeText={this.onChangeSum}
            onSubmitEditing={this.done}
          />

          <ButtonFullSmall
            style={base.btn}
            title="Вывести"
            onPress={this.done}
          />
        </View>
      </Modal>
    );
  }
}
