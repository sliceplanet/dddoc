import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import Modal from 'react-native-modal';

// Style
import {base} from './styles';

export default class ClientPay extends React.Component {
  onPress = () => {
    const {reduceOnPressClose} = this.props;
    reduceOnPressClose();
  };

  render() {
    return (
      <Modal style={base.center} isVisible>
        <View style={base.wrap1}>
          <Text style={base.text1}>Проведена оплата</Text>
          <Text style={base.text4}>
            Оплата проведена, теперь можно совершить звонок, нажав на кнопку
            “Видео вызов”
          </Text>
          <TouchableOpacity style={base.wrap3} onPress={this.onPress}>
            <Text style={base.text3}>Ок</Text>
          </TouchableOpacity>
        </View>
      </Modal>
    );
  }
}
