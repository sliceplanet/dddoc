import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import Modal from 'react-native-modal';
// import Sound from 'react-native-sound';

// Style
import {base} from './styles';

export default class AlertVideoCallRequest extends React.Component {
  onPressAccept = () => {
    const {
      alert,
      user,
      fetchPutVideosIdConfirm,
      reducerVideosConfirm,
      reduceOnPressClose,
    } = this.props;
    const {id} = alert.extra;
    const path = {
      id,
    };

    fetchPutVideosIdConfirm({user, path});
    reducerVideosConfirm(alert);
    reduceOnPressClose();
  };

  onPressCancel = () => {
    const {
      alert,
      user,
      fetchPutVideosIdCancel,
      reducerVideosCancel,
      reduceOnPressClose,
    } = this.props;
    const {id} = alert.extra;
    const path = {
      id,
    };

    fetchPutVideosIdCancel({user, path});
    reducerVideosCancel(alert);
    reduceOnPressClose();
  };

  render() {
    return (
      <Modal style={base.center} isVisible>
        <View style={base.wrap1}>
          <Text style={base.text1}>Запрос на видео-звонок</Text>
          <TouchableOpacity style={base.wrap2} onPress={this.onPressAccept}>
            <Text style={base.text2}>Подтвердить</Text>
          </TouchableOpacity>
          <TouchableOpacity style={base.wrap3} onPress={this.onPressCancel}>
            <Text style={base.text3}>Отклонить</Text>
          </TouchableOpacity>
        </View>
      </Modal>
    );
  }
}
