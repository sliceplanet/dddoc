import {connect} from 'react-redux';
import component from './component';

import {reduceOnPressClose} from '../../../../store/actions/alert';
import {
  reducerVideosConfirm,
  reducerVideosCancel,
  fetchPutVideosIdCancel,
  fetchPutVideosIdConfirm,
} from '../../../../store/actions/videos';

function mapStateToProps(state) {
  return {
    alert: state.alert,
    user: state.user,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    reduceOnPressClose: () => dispatch(reduceOnPressClose()),
    fetchPutVideosIdCancel: (data) => dispatch(fetchPutVideosIdCancel(data)),
    fetchPutVideosIdConfirm: (data) => dispatch(fetchPutVideosIdConfirm(data)),
    reducerVideosConfirm: (notify) => dispatch(reducerVideosConfirm(notify)),
    reducerVideosCancel: (notify) => dispatch(reducerVideosCancel(notify)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(component);
