import {connect} from 'react-redux';
import component from './component';

import {
  reduceOnPressClose,
  reducerMethodPayment,
} from '../../../../store/actions/alert';

function mapStateToProps(state) {
  return {
    alert: state.alert,
    user: state.user,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    reduceOnPressClose: () => dispatch(reduceOnPressClose()),
    reducerMethodPayment: (id) => dispatch(reducerMethodPayment(id)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(component);
