import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import Modal from 'react-native-modal';

// Style
import {base} from './styles';

export default class CallConfirmed extends React.Component {
  onPressPay = () => {
    const {reducerMethodPayment} = this.props;
    const {id} = this.props.alert.extra;
    reducerMethodPayment(id);
  };

  onPress = () => {
    const {reduceOnPressClose} = this.props;
    reduceOnPressClose();
  };

  render() {
    return (
      <Modal style={base.center} isVisible>
        <View style={base.wrap1}>
          <Text style={base.text1}>Звонок подтвержден</Text>
          <Text style={base.text4}>
            До начала видеосвязи в сообщениях можно отправить доктору
            необходимую информацию и файлы
          </Text>
          <TouchableOpacity style={base.wrap2} onPress={this.onPressPay}>
            <Text style={base.text2}>Оплатить</Text>
          </TouchableOpacity>
          <TouchableOpacity style={base.wrap3} onPress={this.onPress}>
            <Text style={base.text3}>Ок</Text>
          </TouchableOpacity>
        </View>
      </Modal>
    );
  }
}
