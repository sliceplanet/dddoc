import React from 'react';
import {View, Text} from 'react-native';
import Image from 'react-native-scalable-image';

// Helpers
import * as Images from '../../../helpers/images';

// Style
import {base} from './styles';

export default class Card extends React.Component {
  render() {
    const {children, title, verify} = this.props;

    return (
      <View style={base.wrap1}>
        <View style={base.row}>
          <Text style={base.text1}>{title.toUpperCase()}</Text>
          <View style={base.flex} />
          {verify && (
            <Image source={Images.check} height={18} style={base.tintColor} />
          )}
        </View>
        <View style={base.wrap2}>{children}</View>
      </View>
    );
  }
}
