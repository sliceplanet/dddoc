import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
    alignItems: 'center',
  },
  tintColor: {
    tintColor: '#D962A5',
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    width: wp(100),
    backgroundColor: '#EFEFF4',
    paddingHorizontal: 16,
    paddingVertical: 6,
  },
  wrap1: {
    alignItems: 'center',
    backgroundColor: 'white',
    shadowColor: 'rgba(0, 0, 0, 0.25)',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.5,
    shadowRadius: 2,
    elevation: 1,
    marginBottom: 8,
  },
  wrap2: {
    paddingVertical: 8,
  },
  text1: {
    // width: wp(100),
    fontFamily: 'SF Pro Text',
    fontSize: 13,
    lineHeight: 18,
    color: 'rgba(60, 60, 67, 0.6)',
    backgroundColor: '#EFEFF4',
  },
});

export default {base};
