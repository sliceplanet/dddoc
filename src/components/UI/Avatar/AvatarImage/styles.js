import {StyleSheet} from 'react-native';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
    alignItems: 'center',
  },
  wrap1: {
    width: 80,
    height: 80,
    alignItems: 'center',
    borderRadius: 80,
    borderColor: '#D962A5',
    borderWidth: 1,
    overflow: 'hidden',
  },
  wrap2: {
    width: 80,
    height: 80,
    borderRadius: 80,
    borderColor: '#D962A5',
    borderWidth: 1,
    backgroundColor: 'white',
    alignItems: 'center',
  },
  image: {
    width: 80,
    height: 80,
  },
  position: {
    width: 80,
    height: 80,
    position: 'absolute',
  },
  text1: {
    fontFamily: 'SF Pro Text',
    fontSize: 12,
    lineHeight: 18,
    color: '#D962A5',
    marginVertical: 2,
  },
});

export default {base};
