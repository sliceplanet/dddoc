import React from 'react';
import {View, Text, Image, ActivityIndicator} from 'react-native';
import ScalableImage from 'react-native-scalable-image';
import ImagePicker from 'react-native-image-picker';
import ImageResizer from 'react-native-image-resizer';
import RNFS from 'react-native-fs';

// Components
import Touchable from '../../Touchable';

// Helpers
import * as Images from '../../../../helpers/images';

// Style
import {base} from './styles';

export default class AvatarImage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      load: false,
    };
  }

  onPress = () => {
    const options = {
      title: 'Select Avatar',
      noData: false,
    };

    ImagePicker.showImagePicker(options, (response) => {
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        const {setToast} = this.props;
        setToast(response.error);
      } else {
        const {onPress} = this.props;
        const {uri} = response;

        ImageResizer.createResizedImage(uri, 200, 200, 'JPEG', 100)
          .then((result) => {
            RNFS.readFile(result.uri, 'base64')
              .then((data) => {
                onPress(data, uri, 'image/jpeg');
              })
              .catch((RnfsError) => console.log(RnfsError));
          })
          .catch((resizedError) => {
            console.log(resizedError);
          });
      }
    });
  };

  onLoadStart = () => this.setState({load: true});
  onLoadEnd = () => this.setState({load: false});
  onError = ({nativeEvent: {error}}) => {
    const {setToast} = this.props;
    this.setState({load: false});
    setToast(error);
  };

  render() {
    const {source, editable} = this.props;
    const {load} = this.state;

    if (source) {
      return (
        <Touchable
          editable={editable}
          style={base.wrap1}
          onPress={this.onPress}>
          <Image
            onLoadStart={this.onLoadStart}
            onLoadEnd={this.onLoadEnd}
            onError={this.onError}
            source={{uri: source}}
            style={base.image}
          />
          {load && (
            <ActivityIndicator
              style={base.position}
              size="large"
              color="#D962A5"
            />
          )}
        </Touchable>
      );
    }

    return (
      <Touchable editable={editable} style={base.wrap2} onPress={this.onPress}>
        <View style={base.flex} />
        <ScalableImage source={Images.photo} width={22} />
        <Text style={base.text1}>Фото</Text>
        <View style={base.flex} />
      </Touchable>
    );
  }
}
