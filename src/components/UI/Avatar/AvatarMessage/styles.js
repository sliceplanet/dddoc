import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
  },
  wrap1: {
    width: 64,
    height: 64,
    alignItems: 'center',
    borderRadius: 64,
    borderColor: '#D962A5',
    borderWidth: 1,
    overflow: 'hidden',
  },
  wrap2: {
    width: 64,
    height: 64,
    alignItems: 'center',
    borderRadius: 64,
    borderColor: '#ACACAC',
    borderWidth: 1,
    overflow: 'hidden',
  },
  wrap3: {
    width: 64,
    height: 64,
    borderRadius: 64,
    borderColor: '#D962A5',
    borderWidth: 1,
    backgroundColor: 'white',
    alignItems: 'center',
  },
  image: {
    width: 64,
    height: 64,
  },
  unread: {
    position: 'absolute',
    top: 0,
    right: 0,
    width: 25,
    height: 25,
    justifyContent: 'space-around',
    borderRadius: 17,
    backgroundColor: '#D962A5',
    alignItems: 'center',
    paddingVertical: 2.5,
  },
  position: {
    width: 64,
    height: 64,
    position: 'absolute',
  },
  text1: {
    fontFamily: 'SF Pro Text',
    fontSize: 11,
    color: 'white',
  },
  text2: {
    fontFamily: 'SF Pro Text',
    fontSize: 12,
    lineHeight: 18,
    color: '#D962A5',
    marginVertical: 2,
  },
});

export default {base};
