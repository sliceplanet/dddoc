import React from 'react';
import {View, Text, Image, ActivityIndicator} from 'react-native';
import ScalableImage from 'react-native-scalable-image';

// Helpers
import * as Images from '../../../../helpers/images';

// Style
import {base} from './styles';

export default class AvatarMessage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      load: false,
    };
  }

  onLoadStart = () => this.setState({load: true});
  onLoadEnd = () => this.setState({load: false});
  onError = ({nativeEvent: {error}}) => {
    const {setToast} = this.props;
    this.setState({load: false});
    setToast(error);
  };

  render() {
    const {source, unread} = this.props;
    const {load} = this.state;

    if (source) {
      return (
        <View>
          <View style={unread > 0 ? base.wrap1 : base.wrap2}>
            <Image
              onLoadStart={this.onLoadStart}
              onLoadEnd={this.onLoadEnd}
              onError={this.onError}
              source={source}
              style={base.image}
            />

            {load && (
              <ActivityIndicator
                style={base.position}
                size="large"
                color={unread > 0 ? '#D962A5' : '#ACACAC'}
              />
            )}
          </View>
          {unread > 0 && (
            <View style={base.unread}>
              <Text style={base.text1}>{unread > 9 ? '9+' : unread}</Text>
            </View>
          )}
        </View>
      );
    }

    return (
      <View style={base.wrap3}>
        <View style={base.flex} />
        <ScalableImage source={Images.photo} width={22} />
        <Text style={base.text2}>Фото</Text>
        <View style={base.flex} />
        {unread > 0 && (
          <View style={base.unread}>
            <Text style={base.text1}>{unread > 9 ? '9+' : unread}</Text>
          </View>
        )}
      </View>
    );
  }
}
