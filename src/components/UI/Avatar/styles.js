import {StyleSheet} from 'react-native';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
    alignItems: 'center',
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  avatar: {
    alignSelf: 'flex-start',
    paddingLeft: 23,
    paddingTop: 22,
  },
  text1: {
    fontFamily: 'SF Pro Text',
    fontSize: 17,
    fontWeight: '600',
    lineHeight: 22,
    color: '#D962A5',
    marginHorizontal: 20,
  },
});

export default {base};
