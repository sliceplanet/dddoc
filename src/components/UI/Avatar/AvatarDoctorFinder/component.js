import React from 'react';
import {View, Image, ActivityIndicator, Text} from 'react-native';
import ScalableImage from 'react-native-scalable-image';

// Helpers
import * as Images from '../../../../helpers/images';

// Style
import {base} from './styles';

export default class AvatarDoctorFinder extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      load: false,
    };
  }

  onLoadStart = () => this.setState({load: true});
  onLoadEnd = () => this.setState({load: false});
  onError = ({nativeEvent: {error}}) => {
    const {setToast} = this.props;
    this.setState({load: false});
    setToast(error);
  };

  render() {
    const {source, online} = this.props;
    const {load} = this.state;

    return (
      <View>
        <View style={online ? base.wrap1 : base.wrap2}>
          {source ? (
            <Image
              onLoadStart={this.onLoadStart}
              onLoadEnd={this.onLoadEnd}
              onError={this.onError}
              source={source}
              style={base.image}
            />
          ) : (
            <View style={base.wrap3}>
              <View style={base.flex} />
              <ScalableImage source={Images.photo} width={22} />
              <Text style={base.text2}>Фото</Text>
              <View style={base.flex} />
            </View>
          )}

          {load && (
            <ActivityIndicator
              style={base.position}
              size="large"
              color={online ? '#D962A5' : '#ACACAC'}
            />
          )}
        </View>
      </View>
    );
  }
}
