import React from 'react';
import {View, TextInput} from 'react-native';

// Components
import AvatarImage from './AvatarImage';

// Style
import {base} from './styles';

export default class Avatar extends React.Component {
  render() {
    const {source, nickname, editable, onPress, onChangeText} = this.props;
    return (
      <View style={[base.row, base.avatar]}>
        <AvatarImage source={source} editable={editable} onPress={onPress} />
        <TextInput
          style={base.text1}
          value={nickname}
          editable={editable}
          autoCorrect={false}
          underlineColorAndroid="transparent"
          onChangeText={onChangeText}
        />
      </View>
    );
  }
}
