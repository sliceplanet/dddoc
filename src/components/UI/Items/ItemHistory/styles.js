import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
    alignItems: 'center',
  },
  image: {
    marginLeft: 16,
    marginRight: 10,
  },
  wrap1: {
    width: wp(100),
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 6,
    paddingHorizontal: 16,
    backgroundColor: 'white',
  },
  text1: {
    fontFamily: 'SF Pro Text',
    fontSize: 9,
    lineHeight: 11,
    color: 'rgba(0, 0, 0, 0.54)',
  },
  text2: {
    fontFamily: 'SF Pro Text',
    fontSize: 13,
    lineHeight: 16,
    color: 'rgba(0, 0, 0, 0.87)',
  },
  text3: {
    fontFamily: 'SF Pro Text',
    fontSize: 12,
    lineHeight: 16,
    color: '#75C9C3',
  },
  text4: {
    fontFamily: 'SF Pro Text',
    fontSize: 12,
    lineHeight: 16,
    color: '#D962A5',
  },
});

export default {base};
