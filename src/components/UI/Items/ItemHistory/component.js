import React from 'react';
import {View, Text} from 'react-native';
import Image from 'react-native-scalable-image';
import moment from 'moment';

// Components

// Helpers
import * as Images from '../../../../helpers/images';

// Style
import {base} from './styles';

export default class ItemHistory extends React.Component {
  renderText = () => {
    const {operationType} = this.props;
    switch (operationType) {
      case 2: {
        return 'Вывод со счета';
      }
      case 3: {
        return 'Оплата за видео-звонок';
      }
      case 4: {
        return 'Возврат средств за видео звонок';
      }
      default: {
        return 'Пополнение счета';
      }
    }
  };

  render() {
    const {date, sum} = this.props;
    return (
      <View style={base.wrap1}>
        <Text style={base.text1}>
          {moment(date).format('DD.MM')}
          {'\n'}
          {moment(date).format('HH:mm')}
        </Text>
        <Image
          style={base.image}
          source={sum > 0 ? Images.down : Images.up}
          width={10}
        />
        <Text style={base.text2}>{this.renderText()}</Text>
        <View style={base.flex} />
        {sum > 0 ? (
          <Text style={base.text3}>{sum} $</Text>
        ) : (
          <Text style={base.text4}>{sum} $</Text>
        )}
      </View>
    );
  }
}
