import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
    alignItems: 'center',
  },
  wrap1: {
    width: wp(100),
    flexDirection: 'row',
    alignItems: 'center',
    padding: 16,
    backgroundColor: 'white',
  },
  wrap2: {
    flex: 1,
    marginHorizontal: 16,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 2,
  },
  online: {
    width: 11,
    height: 11,
    borderRadius: 11,
    backgroundColor: '#14D378',
  },
  text1: {
    fontFamily: 'SF Pro Text',
    fontSize: 15,
    lineHeight: 18,
    fontWeight: 'bold',
    color: 'black',
  },
  text2: {
    fontFamily: 'SF Pro Text',
    fontSize: 15,
    lineHeight: 18,
    color: '#D962A5',
    paddingTop: 4,
  },
  text3: {
    fontFamily: 'SF Pro Text',
    fontSize: 15,
    lineHeight: 18,
    fontStyle: 'italic',
    color: '#14D378',
    marginHorizontal: 4,
  },
  text4: {
    fontFamily: 'SF Pro Text',
    fontSize: 15,
    lineHeight: 18,
    fontStyle: 'italic',
    color: '#ACACAC',
    marginRight: 4,
  },
  text5: {
    fontFamily: 'SF Pro Text',
    fontSize: 15,
    lineHeight: 18,
    color: '#ACACAC',
    marginLeft: 5,
  },
  text6: {
    fontFamily: 'SF Pro Text',
    fontSize: 16,
    lineHeight: 19,
    fontWeight: 'bold',
    color: '#D962A5',
    marginLeft: 5,
  },
});

export default {base};
