import {connect} from 'react-redux';
import component from './component';

import {fetchDeleteServicesId} from '../../../../store/actions/services';

function mapStateToProps(state) {
  return {
    user: state.user,
    specializations: state.specializations,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    fetchDeleteServicesId: (data) => dispatch(fetchDeleteServicesId(data)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(component);
