import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
    alignItems: 'center',
  },
  wrap1: {
    width: wp(100),
    padding: 16,
    backgroundColor: 'white',
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  text1: {
    fontFamily: 'SF Pro Text',
    fontSize: 15,
    lineHeight: 20,
    color: 'black',
  },
  text2: {
    fontFamily: 'SF Pro Text',
    fontSize: 16,
    fontWeight: 'bold',
    lineHeight: 19,
    color: 'black',
    marginRight: 35,
  },
});

export default {base};
