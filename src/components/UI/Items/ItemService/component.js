import React from 'react';
import {View, Text} from 'react-native';
import Image from 'react-native-scalable-image';

// Helpers
import * as Images from '../../../../helpers/images';
import {navigate} from '../../../../helpers/navigation';

// Style
import {base} from './styles';

export default class ItemService extends React.Component {
  onPressEdit = () => {
    const {
      duration,
      description,
      id,
      price,
      specializationId,
      specializations,
    } = this.props;

    navigate('ServiceEdit', {
      props: {
        duration,
        description,
        id,
        price,
        specializationId,
        specializations,
      },
    });
  };

  onPressTrash = () => {
    const {fetchDeleteServicesId, user, id} = this.props;
    const path = {
      id,
    };
    fetchDeleteServicesId({user, path});
  };

  render() {
    const {
      canRemove,
      specializationId,
      specializations,
      description,
      duration,
      price,
    } = this.props;
    console.log(this.props);
    const specialization = specializations.filter(
      (e) => e.id === specializationId,
    );

    return (
      <View style={base.wrap1}>
        <View style={base.row}>
          <Text numberOfLines={1} style={base.text1}>
            {specialization.length > 0 && specialization[0].name}
          </Text>
          <View style={base.flex} />
          <Image source={Images.edit} width={16} onPress={this.onPressEdit} />
        </View>
        <Text style={base.text1}>{description}</Text>
        <View style={base.row}>
          <Text numberOfLines={1} style={base.text1}>
            {`${duration / 60} мин.`}
          </Text>
          <View style={base.flex} />
          <Text style={base.text2}>{`${price} $`}</Text>
          {canRemove && (
            <Image
              source={Images.trash}
              width={14}
              onPress={this.onPressTrash}
            />
          )}
        </View>
      </View>
    );
  }
}
