import React from 'react';
import {View, Text} from 'react-native';
import moment from 'moment';

// Components
import AvatarDoctorFinder from '../../../Avatar/AvatarDoctorFinder';
import Rating from '../../../Rating';

// Helpers
import * as Images from '../../../../../helpers/images';
import {navigate} from '../../../../../helpers/navigation';
import {URL} from '../../../../../store/api';

// Style
import {base} from './styles';

export default class ItemReviewsMy extends React.Component {
  render() {
    const {online, photo, name, lastName, patronymic} = this.props.reviewer;
    const {rating, date, text} = this.props;
    const user = [lastName && lastName, name && name, patronymic && patronymic];

    return (
      <View style={base.wrap1}>
        <View style={base.row}>
          <AvatarDoctorFinder
            source={photo && {uri: `${URL}${photo}`}}
            online={online}
          />
          <View style={base.wrap2}>
            <View style={base.flex} />
            <Text style={base.text1}>{user.join(' ')}</Text>
            <View style={[base.row, base.flex]}>
              <Rating rating={rating} />
              <View style={base.flex} />
              <Text style={base.text2}>
                {moment(date).format('DD.MM.YYYY в HH:mm')}
              </Text>
            </View>
            <View style={base.flex} />
          </View>
        </View>
        <Text style={base.text3}>{text}</Text>
      </View>
    );
  }
}
