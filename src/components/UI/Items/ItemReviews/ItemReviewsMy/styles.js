import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
    alignItems: 'center',
  },
  wrap1: {
    width: wp(100),
    padding: 16,
    backgroundColor: 'white',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 1,
    marginBottom: 10,
  },
  wrap2: {
    flex: 1,
    paddingLeft: 16,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  text1: {
    fontFamily: 'SF Pro Text',
    fontSize: 15,
    lineHeight: 18,
    fontWeight: 'bold',
    color: 'black',
    marginBottom: 12,
  },
  text2: {
    fontFamily: 'SF Pro Text',
    fontSize: 11,
    lineHeight: 18,
    color: '#ACACAC',
  },
  text3: {
    fontFamily: 'SF Pro Text',
    fontSize: 15,
    lineHeight: 18,
    color: 'black',
    marginTop: 14,
  },
});

export default {base};
