import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import Image from 'react-native-scalable-image';

// Components
import AvatarDoctorFinder from '../../Avatar/AvatarDoctorFinder';
import Rating from '../../Rating';

// Helpers
import * as Images from '../../../../helpers/images';
import {navigate} from '../../../../helpers/navigation';
import {URL} from '../../../../store/api';

// Style
import {base} from './styles';

export default class ItemFavorite extends React.Component {
  onPress = () =>
    navigate('Doctor', {props: {...this.props, isFavourite: true}});

  renderName = (isShort = false) => {
    const {name, lastName, patronymic} = this.props;
    let result = [];

    if (lastName) {
      result = [...result, lastName];
    }
    if (name) {
      result = [...result, name];
    }
    if (!isShort && patronymic) {
      result = [...result, patronymic];
    }

    return result.join(' ');
  };

  render() {
    const {
      photo,
      specializations,
      online,
      rating,
      videoCount,
      minPrice,
      maxPrice,
    } = this.props;

    return (
      <TouchableOpacity style={base.wrap1} onPress={this.onPress}>
        <AvatarDoctorFinder
          source={photo && {uri: `${URL}${photo}`}}
          online={online}
        />
        <View style={base.wrap2}>
          <Text style={base.text1}>{this.renderName()}</Text>
          <Text style={base.text2}>{specializations.join(', ')}</Text>
          <View style={base.row}>
            {online && <View style={base.online} />}
            <Text style={online ? base.text3 : base.text4}>
              {online ? 'online' : 'offline'}
            </Text>
            <Rating rating={rating} />
          </View>
          <View style={base.row}>
            <Image source={Images.video} width={20} />
            <Text style={base.text5}>{videoCount}</Text>
            <View style={base.flex} />
            <Text style={base.text6}>
              {minPrice === maxPrice
                ? `${minPrice} $`
                : `${minPrice}-${maxPrice} $`}
            </Text>
          </View>
        </View>
        <Image source={Images.right} width={12} />
      </TouchableOpacity>
    );
  }
}
