import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import Image from 'react-native-scalable-image';
import moment from 'moment';

// Components
import AvatarVideoCall from '../../Avatar/AvatarVideoCall';

// Helpers
import * as Images from '../../../../helpers/images';
import {navigate} from '../../../../helpers/navigation';
import {getDate} from '../../../../helpers';
import {URL} from '../../../../store/api';

// Style
import {base} from './styles';

export default class ItemVideoCallClient extends React.Component {
  componentDidMount() {
    this.interval = setInterval(() => {
      const {isPaid, date} = this.props;
      if (isPaid && moment(date) <= moment()) {
        this.forceUpdate();
      }
    }, 1000);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  onPressVideoCall = () => {
    const {user2, id, fetchPutVideosIdStart} = this.props;
    const path = {
      id,
    };
    fetchPutVideosIdStart({user: user2, path});
  };

  onPressPay = () => {
    const {id, reducerMethodPayment} = this.props;
    reducerMethodPayment(id);
  };

  onPressCancel = () => {
    const {id, user2, fetchPutVideosIdCancel} = this.props;
    const path = {
      id,
    };
    fetchPutVideosIdCancel({user: user2, path});
  };

  onPressMessage = () => navigate('Message', {user: this.props.user});

  renderStatus = () => {
    const {status} = this.props;
    switch (status) {
      case 0: {
        return <Text style={base.text3}>Ожидает подтверждения</Text>;
      }
      case 1: {
        return <Text style={base.text7}>Запланирован</Text>;
      }
      case 2: {
        return <Text style={base.text8}>Состоялся</Text>;
      }
      case 3: {
        return <Text style={base.text8}>Не состоялся</Text>;
      }
    }
  };

  renderUri = () => {
    const {photo, avatar} = this.props.user;
    if (photo) {
      return {uri: `${URL}${photo}`};
    }
    if (avatar) {
      return {uri: `${URL}${avatar}`};
    }
  };

  renderName = (isShort = false) => {
    const {name, lastName, patronymic} = this.props.user;
    let result = [];

    if (lastName) {
      result = [...result, lastName];
    }
    if (name) {
      result = [...result, name];
    }
    if (!isShort && patronymic) {
      result = [...result, patronymic];
    }

    return result.join(' ');
  };

  render() {
    const {status, isPaid, videoCount, duration} = this.props;
    const date = moment(this.props.date);

    switch (status) {
      case 1: {
        return (
          <View style={base.shadow}>
            <View style={base.wrap1}>
              <AvatarVideoCall source={this.renderUri()} />
              <View style={base.wrap2}>
                <Text style={base.text1}>{this.renderName()}</Text>
                <View style={[base.row, base.flex]}>
                  <Text style={base.text2}>
                    {`${date.format('DD MMM HH:mm')}    ${getDate(duration)}`}
                  </Text>
                  <View style={base.flex} />
                  <Image
                    source={Images.messages}
                    width={24}
                    onPress={this.onPressMessage}
                  />
                </View>
                <Text style={base.text7}>Запланирован</Text>
              </View>
            </View>
            <View style={base.wrap3}>
              {isPaid ? (
                <View style={[base.wrap4, base.br]}>
                  <Text style={base.text11}>Оплатить</Text>
                </View>
              ) : (
                <TouchableOpacity
                  style={[base.wrap4, base.br]}
                  onPress={this.onPressPay}>
                  <Text style={base.text9}>Оплатить</Text>
                </TouchableOpacity>
              )}

              {videoCount ? (
                <View style={[base.wrap4, base.br]}>
                  <Text style={base.text11}>Отменить</Text>
                </View>
              ) : (
                <TouchableOpacity
                  style={[base.wrap4, base.br]}
                  onPress={this.onPressCancel}>
                  <Text style={base.text10}>Отменить</Text>
                </TouchableOpacity>
              )}

              {isPaid && date <= moment() ? (
                <TouchableOpacity
                  style={base.wrap4}
                  onPress={this.onPressVideoCall}>
                  <Text style={base.text9}>Видео-вызов</Text>
                </TouchableOpacity>
              ) : (
                <View style={base.wrap4}>
                  <Text style={base.text11}>Видео-вызов</Text>
                </View>
              )}
            </View>
          </View>
        );
      }
      case 2: {
        return (
          <View style={[base.wrap1, base.shadow]}>
            <AvatarVideoCall source={this.renderUri()} />
            <View style={base.wrap2}>
              <Text style={base.text1}>{this.renderName()}</Text>
              <View style={[base.row, base.flex]}>
                <Text style={base.text2}>
                  {`${date.format('DD MMM HH:mm')}    ${getDate(duration)}`}
                </Text>
                <View style={base.flex} />
                <Image
                  source={Images.messages}
                  width={24}
                  onPress={this.onPressMessage}
                />
              </View>
              <Text style={base.text8}>Состоялся</Text>
            </View>
          </View>
        );
      }
      case 3: {
        return (
          <View style={[base.wrap1, base.shadow]}>
            <AvatarVideoCall source={this.renderUri()} />
            <View style={base.wrap2}>
              <Text style={base.text1}>{this.renderName()}</Text>
              <View style={[base.row, base.flex]}>
                <Text style={base.text2}>
                  {`${date.format('DD MMM HH:mm')}    ${getDate(duration)}`}
                </Text>
                <View style={base.flex} />
                <Image
                  source={Images.messages}
                  width={24}
                  onPress={this.onPressMessage}
                />
              </View>
              <Text style={base.text8}>Не состоялся</Text>
            </View>
          </View>
        );
      }
    }

    return (
      <View style={base.shadow}>
        <View style={base.wrap1}>
          <AvatarVideoCall source={this.renderUri()} />
          <View style={base.wrap2}>
            <Text style={base.text1}>{this.renderName()}</Text>
            <View style={[base.row, base.flex]}>
              <Text style={base.text2}>
                {`${date.format('DD MMM HH:mm')}    ${getDate(duration)}`}
              </Text>
              <View style={base.flex} />
              <Image
                source={Images.messages}
                width={24}
                onPress={this.onPressMessage}
              />
            </View>
            <Text style={base.text3}>Ожидает подтверждения</Text>
          </View>
        </View>
        <View style={base.wrap3}>
          <TouchableOpacity style={base.wrap4} onPress={this.onPressCancel}>
            <Text style={base.text10}>Отменить</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
