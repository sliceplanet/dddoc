import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
    alignItems: 'center',
  },
  br: {
    borderRightColor: 'rgba(0, 0, 0, 0.3)',
    borderRightWidth: 0.7,
  },
  shadow: {
    backgroundColor: 'white',
    marginBottom: 12,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 1,
  },
  wrap1: {
    width: wp(100),
    flexDirection: 'row',
    alignItems: 'center',
    padding: 16,
    borderBottomColor: 'rgba(0, 0, 0, 0.3)',
    borderBottomWidth: 0.7,
  },
  wrap2: {
    flex: 1,
    marginLeft: 16,
  },
  wrap3: {
    width: wp(100),
    flexDirection: 'row',
    alignItems: 'center',
  },
  wrap4: {
    flex: 1,
    alignItems: 'center',
    paddingVertical: 12,
    paddingHorizontal: 4,
    margin: 4,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 2,
  },
  text1: {
    fontFamily: 'SF Pro Text',
    fontSize: 15,
    lineHeight: 18,
    fontWeight: 'bold',
    color: 'black',
  },
  text2: {
    fontFamily: 'SF Pro Text',
    fontSize: 15,
    lineHeight: 18,
    color: 'black',
    paddingRight: 6,
  },
  text3: {
    fontFamily: 'SF Pro Text',
    fontSize: 15,
    lineHeight: 18,
    color: '#FF9B85',
  },
  text4: {
    fontFamily: 'SF Pro Text',
    fontSize: 15,
    lineHeight: 18,
    fontStyle: 'italic',
    color: '#ACACAC',
    marginRight: 4,
  },
  text5: {
    fontFamily: 'SF Pro Text',
    fontSize: 15,
    lineHeight: 18,
    color: '#ACACAC',
    marginLeft: 5,
  },
  text6: {
    fontFamily: 'SF Pro Text',
    fontSize: 16,
    lineHeight: 19,
    fontWeight: 'bold',
    color: '#D962A5',
    marginLeft: 5,
  },
  text7: {
    fontFamily: 'SF Pro Text',
    fontSize: 15,
    lineHeight: 18,
    color: '#D962A5',
  },
  text8: {
    fontFamily: 'SF Pro Text',
    fontSize: 15,
    lineHeight: 18,
    color: 'black',
  },
  text9: {
    fontFamily: 'SF Pro Text',
    fontSize: 15,
    lineHeight: 22,
    fontWeight: '600',
    color: '#D962A5',
  },
  text10: {
    fontFamily: 'SF Pro Text',
    fontSize: 15,
    lineHeight: 22,
    color: '#75C9C3',
  },
  text11: {
    fontFamily: 'SF Pro Text',
    fontSize: 15,
    lineHeight: 22,
    color: '#ACACAC',
  },
});

export default {base};
