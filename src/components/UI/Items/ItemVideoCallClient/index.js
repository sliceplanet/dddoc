import {connect} from 'react-redux';
import component from './component';

import {
  fetchPutVideosIdCancel,
  fetchPutVideosIdConfirm,
  fetchPutVideosIdStart,
} from '../../../../store/actions/videos';
import {reducerMethodPayment} from '../../../../store/actions/alert';

function mapStateToProps(state) {
  return {
    user2: state.user,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    fetchPutVideosIdCancel: (data) => dispatch(fetchPutVideosIdCancel(data)),
    fetchPutVideosIdConfirm: (data) => dispatch(fetchPutVideosIdConfirm(data)),
    fetchPutVideosIdStart: (data) => dispatch(fetchPutVideosIdStart(data)),
    reducerMethodPayment: (id) => dispatch(reducerMethodPayment(id)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(component);
