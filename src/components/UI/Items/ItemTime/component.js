import React from 'react';
import {Text, TouchableOpacity} from 'react-native';

// Style
import {base} from './styles';

export default class ItemMessage extends React.Component {
  onPress = () => {
    const {index, onPress} = this.props;
    onPress(index);
  };

  render() {
    const {title, checked} = this.props;

    return (
      <TouchableOpacity onPress={this.onPress} style={checked && base.checked}>
        <Text style={base.text1}>{title}</Text>
      </TouchableOpacity>
    );
  }
}
