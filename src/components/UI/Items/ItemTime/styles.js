import {StyleSheet} from 'react-native';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
  },
  checked: {
    backgroundColor: 'rgba(0, 0, 0, 0.1)',
  },
  disabled: {},
  text1: {
    fontFamily: 'SF Pro Text',
    fontSize: 17,
    lineHeight: 22,
    color: 'black',
    paddingLeft: 16,
    paddingVertical: 11,
  },
});

export default {base};
