import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import Image from 'react-native-scalable-image';

// Helpers
import * as Images from '../../../../helpers/images';

// Style
import {base} from './styles';

export default class ItemServiceSelect extends React.Component {
  render() {
    const {
      specializationId,
      specializations,
      description,
      duration,
      price,
      checked,
      onPress,
    } = this.props;
    const specialization = specializations.filter(
      (e) => e.id === specializationId,
    );

    return (
      <TouchableOpacity style={base.wrap1} onPress={onPress}>
        {checked ? (
          <Image style={base.image} source={Images.select} width={24} />
        ) : (
          <Image style={base.image} source={Images.unselect} width={24} />
        )}
        <View style={base.wrap2}>
          <Text numberOfLines={1} style={base.text1}>
            {specialization.length > 0 && specialization[0].name}
          </Text>
          <Text style={base.text1}>{description}</Text>
          <View style={base.row}>
            <Text numberOfLines={1} style={base.text1}>
              {`${duration / 60} мин.`}
            </Text>
            <View style={base.flex} />
            <Text style={base.text2}>{`${price} $`}</Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  }
}
