import {connect} from 'react-redux';
import component from './component';

function mapStateToProps(state) {
  return {
    user: state.user,
    specializations: state.specializations,
  };
}

export default connect(mapStateToProps, null)(component);
