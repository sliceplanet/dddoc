import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
    alignItems: 'center',
  },
  wrap1: {
    width: wp(100),
    flexDirection: 'row',
    alignItems: 'center',
    padding: 16,
    backgroundColor: 'white',
  },
  wrap2: {
    flex: 1,
    alignItems: 'flex-start',
  },
  row: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  image: {
    marginRight: 14,
  },
  text1: {
    fontFamily: 'SF Pro Text',
    fontSize: 15,
    lineHeight: 20,
    color: 'black',
  },
  text2: {
    fontFamily: 'SF Pro Text',
    fontSize: 16,
    fontWeight: 'bold',
    lineHeight: 19,
    color: 'black',
  },
});

export default {base};
