import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import Image from 'react-native-scalable-image';
import moment from 'moment';

// Components
import AvatarMessage from '../../Avatar/AvatarMessage';

// Helpers
import * as Images from '../../../../helpers/images';
import {navigate} from '../../../../helpers/navigation';
import {URL} from '../../../../store/api';

// Style
import {base} from './styles';

export default class ItemMessage extends React.Component {
  onPress = () => {
    const {user, id, isAdmin, isSupport} = this.props;
    navigate('Message', {user, chatId: id, isAdmin, isSupport});
  };

  renderName = (isShort = false) => {
    const {name, lastName, patronymic} = this.props.user;
    let result = [];

    if (lastName) {
      result = [...result, lastName];
    }
    if (name) {
      result = [...result, name];
    }
    if (!isShort && patronymic) {
      result = [...result, patronymic];
    }

    return result.join(' ');
  };

  render() {
    const {unread, lastUpdate} = this.props;
    const {avatar} = this.props.user;

    return (
      <TouchableOpacity style={base.wrap1} onPress={this.onPress}>
        <AvatarMessage
          source={avatar ? {uri: `${URL}${avatar}`} : Images.avatar}
          unread={unread}
        />
        <View style={base.wrap2}>
          <Text style={base.text1}>{this.renderName()}</Text>
          <Text style={base.text2}>{moment(lastUpdate).format('HH:mm')}</Text>
        </View>
        <Image
          source={Images.right}
          width={12}
          style={!unread && base.tintColor}
        />
      </TouchableOpacity>
    );
  }
}
