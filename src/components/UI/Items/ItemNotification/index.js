import {connect} from 'react-redux';
import component from './component';

import {setToast} from '../../../../store/actions';
import {fetchPutNotifications} from '../../../../store/actions/notifications';

function mapStateToProps(state) {
  return {
    user: state.user,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setToast: (data) => dispatch(setToast(data)),
    fetchPutNotifications: (data) => dispatch(fetchPutNotifications(data)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(component);
