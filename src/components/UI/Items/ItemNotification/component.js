import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import moment from 'moment';

// Helpers
import {navigate} from '../../../../helpers/navigation';

// Style
import {base} from './styles';

export default class ItemNotification extends React.Component {
  componentDidMount() {
    const {id, isRead, fetchPutNotifications} = this.props;
    if (!isRead) {
      const {user} = this.props;
      const path = {
        id,
      };

      fetchPutNotifications({user, path});
    }
  }

  onPress = () => {
    navigate('Notification', {props: this.props});
  };

  onPressVerification = () => {
    navigate('Verification');
  };

  renderName = (isShort = false) => {
    const {name, lastName, patronymic} = this.props.from;
    let result = [];

    if (lastName) {
      result = [...result, lastName];
    }
    if (name) {
      result = [...result, name];
    }
    if (!isShort && patronymic) {
      result = [...result, patronymic];
    }

    return result.join(' ');
  };

  renderNotify = () => {
    const {type} = this.props;
    switch (type) {
      case 1: {
        return 'Верификация пройдена';
      }
      case 2: {
        return 'Запрос на бронирование нового видео звонка';
      }
      case 3: {
        return 'Запрос на срочный видео звонок';
      }
      case 4: {
        return 'Видео звонок отменен';
      }
      case 5: {
        return 'Видео звонок подтвержден';
      }
      case 6: {
        return 'Видео звонок оплачен';
      }
      case 11: {
        return 'Вызов видео звонка';
      }
      case 12: {
        return 'Для возможности предоставления услуг необходимо верифицировать аккаунт';
      }
      case 13: {
        return 'Видео звонок закончен';
      }
      case 14: {
        return 'Необходимо оплатить видео звонок';
      }
      case 15: {
        return 'Пришло время консультации';
      }
      case 16: {
        return 'Консультация состоялось';
      }
      case 17: {
        return 'Консультация не состоялась';
      }
      case 18: {
        return 'Новый отзыв';
      }
      case 19: {
        return 'Изменение баланса';
      }
      case 20: {
        return 'Отклонить видео звонок';
      }
      case 21: {
        return 'Подтвердить видео звонок ';
      }
    }
    return 'Текст уведомления';
  };

  render() {
    let {createdAt} = this.props;
    switch (this.props.type) {
      case 1: {
        return (
          <View style={base.wrap1}>
            <Text style={base.text1}>{this.renderName()}</Text>
            <View style={base.row}>
              <Text style={base.text2}>{this.renderNotify()}</Text>
              <View style={base.flex} />
              <Text style={base.text3}>{moment(createdAt).fromNow()}</Text>
            </View>
          </View>
        );
      }
      case 12: {
        return (
          <TouchableOpacity
            style={base.wrap1}
            onPress={this.onPressVerification}>
            <Text style={base.text1}>{this.renderName()}</Text>
            <View style={base.row}>
              <Text style={base.text2}>{this.renderNotify()}</Text>
              <View style={base.flex} />
              <Text style={base.text3}>{moment(createdAt).fromNow()}</Text>
            </View>
          </TouchableOpacity>
        );
      }
      default: {
        return (
          <TouchableOpacity style={base.wrap1} onPress={this.onPress}>
            <Text style={base.text1}>{this.renderName()}</Text>
            <View style={base.row}>
              <Text style={base.text2}>{this.renderNotify()}</Text>
              <View style={base.flex} />
              <Text style={base.text3}>{moment(createdAt).fromNow()}</Text>
            </View>
          </TouchableOpacity>
        );
      }
    }
  }
}
