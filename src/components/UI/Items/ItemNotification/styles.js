import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
    alignItems: 'center',
  },
  wrap1: {
    width: wp(100),
    paddingVertical: 16,
    paddingLeft: 16,
    paddingRight: 16,
    backgroundColor: 'white',
  },
  row: {
    flexDirection: 'row',
    alignItems: 'flex-end',
  },
  text1: {
    fontFamily: 'SF Pro Text',
    fontSize: 15,
    lineHeight: 20,
    fontWeight: '600',
    color: 'black',
  },
  text2: {
    flexShrink: 1,
    fontFamily: 'SF Pro Text',
    fontSize: 15,
    lineHeight: 20,
    color: '#D962A5',
  },
  text3: {
    textAlign: 'right',
    minWidth: 80,
    flexShrink: 1,
    fontFamily: 'SF Pro Text',
    fontSize: 11,
    lineHeight: 18,
    fontStyle: 'italic',
    color: 'rgba(60, 60, 67, 0.6)',
  },
});

export default {base};
