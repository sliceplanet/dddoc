import {connect} from 'react-redux';
import component from './component';

import {reducePlayerId} from '../../../../store/actions/playerId';

function mapStateToProps(state) {
  return {
    playerId: state.playerId,
    user: state.user,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    reducePlayerId: (data) => dispatch(reducePlayerId(data)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(component);
