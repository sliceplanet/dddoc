import React from 'react';
import {Alert} from 'react-native';
import OneSignal from 'react-native-onesignal';
import Sound from 'react-native-sound';

import {navigate} from '../../../../helpers/navigation';

import {
  apiPutVideosIdDecline,
  apiPutVideosIdAccept,
} from '../../../../store/api/videos';

export default class OS extends React.Component {
  constructor(properties) {
    super(properties);

    OneSignal.setLogLevel(6, 0);

    OneSignal.init('3b6bb54b-1f4f-4c69-9f4e-95711a19aecd');
    OneSignal.inFocusDisplaying(2);

    OneSignal.addEventListener('received', this.onReceived);
    OneSignal.addEventListener('opened', this.onOpened);
    OneSignal.addEventListener('ids', this.onIds);
  }

  componentWillUnmount() {
    OneSignal.removeEventListener('received', this.onReceived);
    OneSignal.removeEventListener('opened', this.onOpened);
    OneSignal.removeEventListener('ids', this.onIds);
  }

  onReceived(notification) {
    console.log('Notification received: ', notification);
  }

  onOpened = (openResult) => {
    const {id, consultationId} = openResult.notification.payload.additionalData;

    if (id === 22) {
      // Sound.setCategory('Playback');
      const {user} = this.props;

      // this.whoosh = new Sound('skype.mp3', Sound.MAIN_BUNDLE, (error) => {
      //   if (error) {
      //     console.log('failed to load the sound', error);
      //     return;
      //   }

      //   this.whoosh.play((success) => {
      //     if (success) {
      //       console.log('successfully finished playing');
      //     } else {
      //       console.log('playback failed due to audio decoding errors');
      //     }
      //   });
      //   this.whoosh.stop(() => {
      //     this.whoosh.play();
      //   });
      // });

      Sound.setCategory('Playback');
      global.whoosh = new Sound('skype.mp3', Sound.MAIN_BUNDLE, (error) => {
        if (error) {
          console.log('failed to load the sound', error);
          return;
        }

        if (!global.whoosh.isPlaying()) {
          global.whoosh.play((success) => {
            if (success) {
              console.log('successfully finished playing');
            } else {
              console.log('playback failed due to audio decoding errors');
            }
          });
        }

        global.whoosh.setNumberOfLoops(-1);
      });

      setTimeout(() => {
        Alert.alert(
          'Входящий звонок',
          'Желаете принять звонок?',
          [
            {
              text: 'Да',
              onPress: () => {
                apiPutVideosIdAccept({
                  path: {id: consultationId},
                  user,
                });
                navigate('Call', {channelName: consultationId});
                global.whoosh.pause();
              },
            },
            {
              text: 'Нет',
              onPress: () => {
                apiPutVideosIdDecline({
                  path: {id: consultationId},
                  user,
                });
                global.whoosh.pause();
              },
            },
          ],
          {cancelable: false},
        );
      }, 1000);
    }
    console.log('Message: ', openResult.notification.payload.body);
    console.log('Data: ', openResult.notification.payload.additionalData);
    console.log('isActive: ', openResult.notification.isAppInFocus);
    console.log('openResult: ', openResult);
  };

  onIds = (device) => {
    const {userId} = device;
    const {playerId, reducePlayerId} = this.props;
    console.log(device);
    if (userId && playerId.length === 0) {
      reducePlayerId(userId);
    }
  };

  render() {
    return null;
  }
}
