import {StyleSheet} from 'react-native';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
    alignItems: 'center',
  },
  wrap: {
    alignItems: 'center',
  },
  wrap2: {
    flexGrow: 1,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  backgroundColor: {
    backgroundColor: 'white',
  },
  safe: {
    flex: 1,
    backgroundColor: '#75C9C3',
  },
});

export default {base};
