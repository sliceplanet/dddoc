import React from 'react';
import {View, TouchableOpacity} from 'react-native';

export default class Touchable extends React.Component {
  render() {
    const {editable, children} = this.props;

    if (editable) {
      return <TouchableOpacity {...this.props}>{children}</TouchableOpacity>;
    }
    return <View {...this.props}>{children}</View>;
  }
}
