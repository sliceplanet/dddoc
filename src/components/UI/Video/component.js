import React from 'react';

import {View, NativeModules, Platform} from 'react-native';
import {RtcEngine, AgoraView} from 'react-native-agora';

const {Agora} = NativeModules; //Define Agora object as a native module

const {FPS30, AudioProfileDefault, AudioScenarioDefault, Adaptative} = Agora;

// Style
import {base} from './styles';

export default class Video extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      peerIds: [],
      uid: Math.floor(Math.random() * 100),
      appID: props.appID,
      channelName: props.channelName,
      vidMute: false,
      audMute: false,
      joinSucceed: false,
    };

    if (Platform.OS === 'android') {
      const config = {
        appid: props.appID,
        channelProfile: 0,
        videoEncoderConfig: {
          width: 720,
          height: 1080,
          bitrate: 1,
          frameRate: FPS30,
          orientationMode: Adaptative,
        },
        audioProfile: AudioProfileDefault,
        audioScenario: AudioScenarioDefault,
      };
      RtcEngine.init(config);
    }
  }

  componentDidMount() {
    RtcEngine.on('userJoined', (data) => {
      console.log('userJoined->', data);
      const {peerIds} = this.state;
      if (peerIds.indexOf(data.uid) === -1) {
        this.setState({
          peerIds: [...peerIds, data.uid],
        });
        this.props.onUserJoined();
      }
    });
    RtcEngine.on('userOffline', (data) => {
      console.log('userOffline', data);
      this.setState({
        peerIds: this.state.peerIds.filter((uid) => uid !== data.uid),
      });
      this.props.onChangePeer();
    });
    RtcEngine.on('joinChannelSuccess', (data) => {
      RtcEngine.startPreview();
      console.log('joinChannelSuccess->', data);
      this.setState({
        joinSucceed: true,
      });
    });

    RtcEngine.joinChannel(this.state.channelName, this.state.uid);
    RtcEngine.enableAudio();
  }

  componentWillUnmount() {
    this.endCall();
  }

  endCall() {
    RtcEngine.destroy();
  }

  render() {
    return (
      <View style={base.flex}>
        {this.state.peerIds.length > 0 && (
          <>
            <AgoraView
              style={base.flex}
              remoteUid={this.state.peerIds[0]}
              mode={1}
            />
            <AgoraView
              style={base.localVideoStyle}
              zOrderMediaOverlay={true}
              showLocalVideo={true}
              mode={1}
            />
          </>
        )}
      </View>
    );
  }
}
