import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
    alignItems: 'center',
  },
  wrap: {
    width: wp(100) - 16,
    flexDirection: 'row',
    alignItems: 'center',
    padding: 8,
  },
  text1: {
    flexShrink: 1,
    fontFamily: 'SF Pro Text',
    fontSize: 16,
    color: 'black',
    marginHorizontal: 4,
  },
  text2: {
    fontFamily: 'SF Pro Text',
    fontSize: 15,
    lineHeight: 20,
    color: '#979797',
    marginHorizontal: 8,
  },
});

export default {base};
