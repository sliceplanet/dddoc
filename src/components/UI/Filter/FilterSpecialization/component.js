import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import Image from 'react-native-scalable-image';

// Components
import ModalSpecialization from '../../Modal/ModalSpecialization';

// Helpers
import * as Images from '../../../../helpers/images';

// Style
import {base} from './styles';

export default class FilterSpecialization extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isVisible: false,
    };
  }

  onPress = () => this.setState({isVisible: true});

  onSelect = (select) => {
    this.setState({isVisible: false});
    this.props.onSelect(select);
  };

  onClose = () => this.setState({isVisible: false});

  render() {
    const {value} = this.props;
    const {isVisible} = this.state;

    return (
      <TouchableOpacity style={base.wrap} onPress={this.onPress}>
        <Text numberOfLines={1} style={base.text1}>
          {value.length > 0 ? value.map((e) => e.name).join(', ') : 'Все'}
        </Text>
        <View style={base.flex} />
        <Text style={base.text2}>Специализация</Text>
        <Image source={Images.selector} width={22} />
        <ModalSpecialization
          isVisible={isVisible}
          value={value}
          onSelect={this.onSelect}
          onPressClose={this.onClose}
        />
      </TouchableOpacity>
    );
  }
}
