import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import Image from 'react-native-scalable-image';

// Components
import ModalText from '../../Modal/ModalText';

// Helpers
import * as Images from '../../../../helpers/images';

// Style
import {base} from './styles';

const SORT = [
  {label: 'Все', value: 'all'},
  {label: 'По рейтингу', value: 'rating'},
  {label: 'По стоимости видео звонка', value: 'price'},
  {label: 'По количеству видео звонков', value: 'videoCount'},
  {label: 'Кто сейчас онлайн', value: 'online'},
];

export default class FilterSort extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isVisible: false,
    };
  }

  onPress = () => this.setState({isVisible: true});

  onSelect = (select) => {
    this.setState({isVisible: false});
    this.props.onSelect(select);
  };

  onClose = () => this.setState({isVisible: false});

  render() {
    const {value} = this.props;
    const {isVisible} = this.state;

    return (
      <TouchableOpacity style={base.wrap} onPress={this.onPress}>
        <Text style={base.text1}>{value ? value.label : 'Все'}</Text>
        <View style={base.flex} />
        <Text style={base.text2}>Сортировка</Text>
        <Image source={Images.selector} width={22} />
        <ModalText
          isVisible={isVisible}
          items={SORT}
          onSelect={this.onSelect}
          onPressClose={this.onClose}
        />
      </TouchableOpacity>
    );
  }
}
