import {StyleSheet} from 'react-native';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
    alignItems: 'center',
  },
  wrap: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 8,
  },
  text1: {
    fontFamily: 'SF Pro Text',
    fontSize: 16,
    color: 'black',
    marginHorizontal: 4,
  },
  text2: {
    fontFamily: 'SF Pro Text',
    fontSize: 15,
    lineHeight: 20,
    color: '#979797',
    marginRight: 8,
  },
});

export default {base};
