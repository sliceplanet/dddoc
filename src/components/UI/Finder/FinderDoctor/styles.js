import {StyleSheet, Platform} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
    alignItems: 'center',
  },
  wrap1: {
    width: wp(100),
    flexDirection: 'row',
    alignItems: 'center',
    padding: 8,
    backgroundColor: 'white',
  },
  wrap2: {
    flex: 1,
    minHeight: 36,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'rgba(142, 142, 147, 0.12)',
    paddingHorizontal: 8,
    paddingVertical: Platform.OS === 'ios' ? 8 : 0,
    marginHorizontal: 8,
    marginVertical: Platform.OS === 'ios' ? 8 : 0,
    borderRadius: 8,
  },
  wrap3: {
    backgroundColor: 'white',
    padding: 8,
  },
  shadow: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 1,
  },
  btn: {
    alignSelf: 'flex-end',
  },
  border: {
    flexDirection: 'row',
    marginHorizontal: 8,
    borderBottomColor: 'rgba(0,0,0,0.2)',
    borderBottomWidth: 0.5,
  },
  input: {
    flex: 1,
    padding: 0,
    paddingLeft: 8,
    fontFamily: 'SF Pro Text',
    fontSize: 17,
    lineHeight: 20,
    color: 'rgba(0,0,0,0.4)',
  },
  image: {
    margin: 8,
  },
  imageHorizontal: {
    marginHorizontal: 8,
  },
});

export default {base};
