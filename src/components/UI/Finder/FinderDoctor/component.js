import React from 'react';
import {View, TextInput, Platform} from 'react-native';
import Image from 'react-native-scalable-image';

// Components
import FilterSort from '../../Filter/FilterSort';
import FilterCity from '../../Filter/FilterCity';
import FilterSpecialization from '../../Filter/FilterSpecialization';
import ButtonUnderline from '../../Button/ButtonUnderline';

// Helpers
import * as Images from '../../../../helpers/images';

// Style
import {base} from './styles';

export default class FinderDoctor extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      value: '',
      filter: false,
      sort: null,
      city: [],
      specialization: [],
    };
  }

  onPressFilter = () => {
    const {filter} = this.state;
    this.setState({filter: !filter});
  };

  onChangeText = (value) => this.setState({value});
  onSubmitEditing = () => {
    const {sort, city, specialization, value} = this.state;
    const {onFind} = this.props;

    let result = {
      text: value,
    };
    let filter = {};
    if (sort && sort.value !== 'all') {
      result = {
        ...result,
        sort: sort.value,
      };
    }
    if (city.length > 0) {
      filter = {
        ...filter,
        countries: city,
      };
    }
    if (specialization.length > 0) {
      filter = {
        ...filter,
        specializations: specialization.map((e) => e.id),
      };
    }

    result = {
      ...result,
      filter,
    };

    onFind(result);
  };

  onSelectSort = (sort) => this.setState({sort}, this.onSubmitEditing);
  onSelectCity = (city) => this.setState({city}, this.onSubmitEditing);
  onSelectSpecialization = (specialization) =>
    this.setState({specialization}, this.onSubmitEditing);
  onPressClean = () => this.setState({value: ''});
  onPressCleanFilter = () =>
    this.setState(
      {
        filter: false,
        sort: null,
        city: [],
        specialization: [],
      },
      this.onSubmitEditing,
    );

  render() {
    const {value, filter, sort, city, specialization} = this.state;

    return (
      <View style={filter && base.shadow}>
        <View style={base.wrap1}>
          <Image
            source={filter ? Images.cross : Images.filter}
            width={24}
            style={base.image}
            onPress={this.onPressFilter}
          />
          <View style={base.wrap2}>
            <Image source={Images.search} height={14} />
            <TextInput
              style={base.input}
              value={value}
              placeholder="Поиск"
              placeholderTextColor="rgba(0,0,0,0.4)"
              underlineColorAndroid="transparent"
              returnKeyType="search"
              clearButtonMode="always"
              onChangeText={this.onChangeText}
              onSubmitEditing={this.onSubmitEditing}
            />
            {Platform.OS === 'android' && value.length > 0 && (
              <Image
                source={Images.cross}
                width={22}
                onPress={this.onPressClean}
              />
            )}
          </View>
        </View>
        {filter && (
          <View style={base.wrap3}>
            <FilterSpecialization
              value={specialization}
              onSelect={this.onSelectSpecialization}
            />
            <View style={base.border} />
            <FilterCity value={city} onSelect={this.onSelectCity} />
            <View style={base.border} />
            <FilterSort value={sort} onSelect={this.onSelectSort} />
            <ButtonUnderline
              style={base.btn}
              title="Очистить фильтр"
              onPress={this.onPressCleanFilter}
            />
          </View>
        )}
      </View>
    );
  }
}
