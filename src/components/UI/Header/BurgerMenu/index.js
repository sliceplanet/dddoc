import {connect} from 'react-redux';
import component from './component';

function mapStateToProps(state) {
  return {
    chats: state.chats,
  };
}

export default connect(mapStateToProps, null)(component);
