import React from 'react';
import {View, TouchableOpacity} from 'react-native';
import Image from 'react-native-scalable-image';

// Components
import ModalMenu from '../../Modal/ModalMenu';

// Helpers
import * as Images from '../../../../helpers/images';

// Style
import {base} from './styles';

export default class BurgerMenu extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isVisible: false,
    };
  }

  onPressMenu = () => this.setState({isVisible: true});

  onPressClose = () => this.setState({isVisible: false});

  render() {
    const {isVisible} = this.state;
    const {chats} = this.props;
    let unread = 0;
    chats.forEach((e) => {
      if (e.unread) {
        unread += e.unread;
      }
    });

    return (
      <TouchableOpacity onPress={this.onPressMenu}>
        <Image source={Images.menu} style={base.image} height={16} />
        {unread > 0 && <View style={base.point} />}
        <ModalMenu isVisible={isVisible} onPressClose={this.onPressClose} />
      </TouchableOpacity>
    );
  }
}
