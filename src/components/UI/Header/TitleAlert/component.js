import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import Image from 'react-native-scalable-image';

// Helpers
import * as Images from '../../../../helpers/images';
import {navigate} from '../../../../helpers/navigation';

// Style
import {base} from './styles';

export default class TitleAvatar extends React.Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  onPress = () => navigate('Notifications');

  render() {
    const {notifications} = this.props;
    const count = notifications.filter((e) => !e.isRead).length;

    return (
      <TouchableOpacity onPress={this.onPress}>
        <View style={base.wrap}>
          <Image source={Images.alert} height={24} />
        </View>
        {count > 0 && (
          <View style={base.position}>
            <View style={base.flex} />
            <Text style={base.text}>{count > 9 ? '9+' : count}</Text>
            <View style={base.flex} />
          </View>
        )}
      </TouchableOpacity>
    );
  }
}
