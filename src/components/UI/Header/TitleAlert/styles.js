import {StyleSheet} from 'react-native';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
  },
  wrap: {
    margin: 8,
  },
  position: {
    position: 'absolute',
    right: 0,
    top: 8,
    width: 17,
    height: 17,
    alignItems: 'center',
    borderRadius: 17,
    backgroundColor: '#D962A5',
  },
  text: {
    fontFamily: 'SF Pro Text',
    fontSize: 9,
    color: 'white',
  },
});

export default {base};
