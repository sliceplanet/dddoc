import React from 'react';
import {
  TouchableOpacity,
  Text,
  Image,
  ActivityIndicator,
  View,
} from 'react-native';
import ScalableImage from 'react-native-scalable-image';

// Helpers
import * as Images from '../../../../helpers/images';

// Style
import {base} from './styles';

export default class TitleAvatar extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      load: false,
    };
  }

  onLoadStart = () => this.setState({load: true});
  onLoadEnd = () => this.setState({load: false});
  onError = ({nativeEvent: {error}}) => {
    const {setToast} = this.props;
    this.setState({load: false});
    setToast(error);
  };

  render() {
    const {source} = this.props;
    const {load} = this.state;

    if (source) {
      return (
        <TouchableOpacity>
          <View style={base.wrap1}>
            <Image
              onLoadStart={this.onLoadStart}
              onLoadEnd={this.onLoadEnd}
              onError={this.onError}
              source={source}
              style={base.image}
            />

            {load && (
              <ActivityIndicator
                style={base.position}
                size="small"
                color={'#D962A5'}
              />
            )}
          </View>
        </TouchableOpacity>
      );
    }

    return (
      <View style={base.wrap2}>
        <View style={base.flex} />
        <ScalableImage source={Images.photo} width={12} />
        <Text style={base.text1}>Фото</Text>
        <View style={base.flex} />
      </View>
    );
  }
}
