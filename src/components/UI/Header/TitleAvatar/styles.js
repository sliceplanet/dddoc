import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
  },
  wrap1: {
    width: 36,
    height: 36,
    alignItems: 'center',
    borderRadius: 36,
    borderColor: '#D962A5',
    borderWidth: 1,
    overflow: 'hidden',
    margin: 8,
  },
  wrap2: {
    width: 36,
    height: 36,
    borderRadius: 36,
    borderColor: '#D962A5',
    borderWidth: 1,
    backgroundColor: 'white',
    alignItems: 'center',
  },
  image: {
    width: 36,
    height: 36,
  },
  position: {
    width: 36,
    height: 36,
    position: 'absolute',
  },
  text1: {
    fontFamily: 'SF Pro Text',
    fontSize: 8,
    color: '#D962A5',
    marginVertical: 2,
  },
});

export default {base};
