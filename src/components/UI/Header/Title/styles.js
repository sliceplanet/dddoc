import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
    alignItems: 'center',
  },
  wrap: {
    width: wp(100),
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#75C9C3',
    paddingHorizontal: 8,
    paddingVertical: 8,
    minHeight: 68,
  },
  position: {
    width: wp(100),
    alignItems: 'center',
    position: 'absolute',
    zIndex: -999,
  },
  image: {
    margin: 8,
  },
  text1: {
    fontFamily: 'SF Pro Text',
    fontSize: 17,
    lineHeight: 22,
    fontWeight: '600',
    color: 'white',
  },
  text2: {
    fontFamily: 'SF Pro Text',
    fontSize: 17,
    lineHeight: 22,
    color: 'white',
    margin: 8,
  },
});

export default {base};
