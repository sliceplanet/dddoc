import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import Image from 'react-native-scalable-image';

// Components
import TitleAvatar from '../TitleAvatar';
import TitleAlert from '../TitleAlert';
import ButtonBack from '../../Button/ButtonBack';
import BurgerMenu from '../BurgerMenu';

// Helpers
import * as Images from '../../../../helpers/images';

// Style
import {base} from './styles';

export default class Title extends React.Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    const {
      title,
      avatar,
      button,
      menu,
      back,
      alert,
      avatarSource,
      onPressButton,
      onPressBack,
    } = this.props;

    return (
      <View style={base.wrap}>
        {!back && <ButtonBack />}
        {back && (
          <Image
            source={Images.back}
            style={base.image}
            height={20}
            onPress={onPressBack}
          />
        )}

        {alert && <TitleAlert />}

        <View style={base.position}>
          <Text style={base.text1}>{title}</Text>
        </View>
        <View style={base.flex} />

        {avatar && <TitleAvatar source={avatarSource} />}

        {button && (
          <TouchableOpacity onPress={onPressButton}>
            <Text style={base.text2}>{button}</Text>
          </TouchableOpacity>
        )}

        {menu && <BurgerMenu />}
      </View>
    );
  }
}
