import {Keyboard} from 'react-native';
import * as validator from '../../../../helpers/validator';

export function constructorState(_this) {
  _this.state = {
    name: '',
    lastName: '',
    patronymic: '',
    country: '',
    phone: '',
    email: '',
    password: '',
    confPassword: '',
    picker: 0,
    check: false,
  };
}

export function done(_this) {
  Keyboard.dismiss();
  const {
    fetchPostClientRegister,
    fetchPostDoctorRegister,
    setToast,
  } = _this.props;
  const {
    picker,
    name,
    lastName,
    patronymic,
    country,
    phone,
    email,
    password,
    confPassword,
    check,
  } = _this.state;

  if (name.length === 0) {
    setToast('Введите имя');
    return;
  }
  if (picker === 1) {
    if (lastName.length === 0) {
      setToast('Введите фамилию');
      return;
    }
    // if (patronymic.length === 0) {
    //   setToast('Введите отчество');
    //   return;
    // }
    if (country.length === 0) {
      setToast('Введите страну');
      return;
    }
  }
  if (!validator.phone(phone)) {
    setToast('Введите телефон');
    return;
  }
  if (!validator.email(email)) {
    setToast('Введите e-mail');
    return;
  }
  if (password !== confPassword) {
    setToast('Пароль не совпадает');
    return;
  }
  if (!validator.password(password)) {
    setToast(
      'Пароль должен обязательно содержать одну заглавную, одну строчную букву минимальное количество символов - 6',
    );
    return;
  }
  if (!check) {
    setToast('Примите условия пользовательского соглашения');
    return;
  }

  if (picker === 0) {
    fetchPostClientRegister({
      name,
      phone,
      email,
      password,
    });
  } else {
    fetchPostDoctorRegister({
      country,
      lastName,
      patronymic,
      name,
      phone,
      email,
      password,
    });
  }
}
