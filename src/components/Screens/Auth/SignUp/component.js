import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';

// Components
import Wrap from '../../../UI/Base/Wrap';
import Title from '../../../UI/Header/Title';
import PickerUser from '../../../UI/Picker/PickerUser';
import InputText from '../../../UI/Input/InputText';
import InputCountry from '../../../UI/Input/InputCountry';
import ButtonFull from '../../../UI/Button/ButtonFull';
import CheckBox from '../../../UI/CheckBox';

// Helpers
import * as functions from './functions';
import {replace, navigate} from '../../../../helpers/navigation';

// Style
import {base} from './styles';

export default class SignUp extends React.Component {
  constructor(props) {
    super(props);

    functions.constructorState(this);
  }

  onPressSignIn = () => replace('SignIn');
  onPressTerms = () => navigate('Terms');

  onChangePickerUser = (picker) => this.setState({picker});
  onChangeName = (name) => this.setState({name});
  onChangeLastName = (lastName) => this.setState({lastName});
  onChangePatronymic = (patronymic) => this.setState({patronymic});
  onChangeCountry = (country) => this.setState({country});
  onChangePhone = (phone) => this.setState({phone});
  onChangeEmail = (email) => this.setState({email});
  onChangePassword = (password) => this.setState({password});
  onChangeConfPassword = (confPassword) => this.setState({confPassword});
  onChangeCheck = () => {
    const {check} = this.state;
    this.setState({check: !check});
  };

  refLastName = (ref) => (this.lastName = ref);
  refPatronymic = (ref) => (this.patronymic = ref);
  refCountry = (ref) => (this.country = ref);
  refPhone = (ref) => (this.phone = ref);
  refEmail = (ref) => (this.email = ref);
  refPassword = (ref) => (this.password = ref);
  refConfPassword = (ref) => (this.confPassword = ref);

  nextLastName = () => this.lastName.focus();
  nextPatronymic = () => this.patronymic.focus();
  nextPhone = () => this.phone.focus();
  nextEmail = () => this.email.focus();
  nextPassword = () => this.password.focus();
  nextConfPassword = () => this.confPassword.focus();

  done = () => functions.done(this);

  render() {
    const {
      picker,
      name,
      lastName,
      patronymic,
      country,
      phone,
      email,
      password,
      confPassword,
      check,
    } = this.state;

    return (
      <Wrap titleView={<Title title="Регистрация" />}>
        <PickerUser index={picker} onChange={this.onChangePickerUser} />
        <InputText
          title="Имя"
          returnKeyType="next"
          textContentType="name"
          value={name}
          autoCapitalize="sentences"
          onChangeText={this.onChangeName}
          onSubmitEditing={picker === 1 ? this.nextLastName : this.nextPhone}
        />
        {picker === 1 && (
          <InputText
            ref={this.refLastName}
            title="Фамилия"
            textContentType="namePrefix"
            returnKeyType="next"
            autoCapitalize="sentences"
            value={lastName}
            onChangeText={this.onChangeLastName}
            onSubmitEditing={this.nextPatronymic}
          />
        )}
        {picker === 1 && (
          <InputText
            ref={this.refPatronymic}
            title="Отчество"
            textContentType="nameSuffix"
            returnKeyType="next"
            autoCapitalize="sentences"
            value={patronymic}
            onChangeText={this.onChangePatronymic}
            onSubmitEditing={this.nextPhone}
          />
        )}
        {picker === 1 && (
          <InputCountry
            ref={this.refCountry}
            title="Страна"
            value={country}
            onChange={this.onChangeCountry}
          />
        )}
        <InputText
          ref={this.refPhone}
          title="Телефон"
          textContentType="telephoneNumber"
          returnKeyType="next"
          keyboardType="phone-pad"
          // mask="[0000000000999]"
          value={phone}
          onChangeText={this.onChangePhone}
          onSubmitEditing={this.nextEmail}
        />
        <InputText
          ref={this.refEmail}
          title="E-mail"
          textContentType="emailAddress"
          returnKeyType="next"
          keyboardType="email-address"
          value={email}
          onChangeText={this.onChangeEmail}
          onSubmitEditing={this.nextPassword}
        />
        <InputText
          ref={this.refPassword}
          title="Пароль"
          textContentType="password"
          returnKeyType="next"
          value={password}
          secureTextEntry
          onChangeText={this.onChangePassword}
          onSubmitEditing={this.nextConfPassword}
        />
        <InputText
          ref={this.refConfPassword}
          title="Подтверждение пароля"
          textContentType="password"
          value={confPassword}
          secureTextEntry
          onChangeText={this.onChangeConfPassword}
          onSubmitEditing={this.done}
        />
        <View style={[base.row, base.check]}>
          <CheckBox value={check} onChange={this.onChangeCheck} />
          <TouchableOpacity
            style={base.row}
            activeOpacity={1}
            onPress={this.onChangeCheck}>
            <Text style={base.text1}>Принимаю условия</Text>
            <TouchableOpacity onPress={this.onPressTerms}>
              <Text style={base.text2}> пользовательского соглашения</Text>
            </TouchableOpacity>
          </TouchableOpacity>
        </View>
        <ButtonFull title="Зарегистрироваться" onPress={this.done} />
        <View style={base.flex} />
        <View style={[base.row, base.marginVertical]}>
          <Text style={base.text3}>У вас уже есть аккаунт?</Text>
          <TouchableOpacity onPress={this.onPressSignIn}>
            <Text style={[base.text3, base.text4]}>Войти</Text>
          </TouchableOpacity>
        </View>
      </Wrap>
    );
  }
}
