import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  check: {
    width: wp(100) - 32,
    marginVertical: 20,
  },
  marginVertical: {
    marginVertical: 28,
  },
  text1: {
    fontFamily: 'SF Pro Text',
    fontSize: 12,
    lineHeight: 15,
    color: 'rgba(60, 60, 67, 0.6)',
    marginLeft: 8,
  },
  text2: {
    fontFamily: 'SF Pro Text',
    fontSize: 12,
    lineHeight: 15,
    marginRight: 8,
    fontWeight: '500',
    color: '#D962A5',
    marginHorizontal: 4,
  },
  text3: {
    fontFamily: 'SF Pro Text',
    fontSize: 15,
    lineHeight: 15,
    color: 'rgba(60, 60, 67, 0.6)',
  },
  text4: {
    fontWeight: '500',
    color: '#D962A5',
    marginHorizontal: 4,
  },
});

export default {base};
