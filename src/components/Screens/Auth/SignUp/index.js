import {connect} from 'react-redux';
import component from './component';

import {setToast} from '../../../../store/actions';
import {
  fetchPostClientRegister,
  fetchPostDoctorRegister,
} from '../../../../store/actions/register';

function mapDispatchToProps(dispatch) {
  return {
    setToast: (data) => dispatch(setToast(data)),
    fetchPostClientRegister: (data) => dispatch(fetchPostClientRegister(data)),
    fetchPostDoctorRegister: (data) => dispatch(fetchPostDoctorRegister(data)),
  };
}

export default connect(null, mapDispatchToProps)(component);
