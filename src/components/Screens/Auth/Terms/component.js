import React from 'react';
import {WebView} from 'react-native-webview';

// Components
import Wrap from '../../../UI/Base/Wrap';
import Title from '../../../UI/Header/Title';

// Api
import {apiGetTerms} from '../../../../store/api/terms';

// Style
import {base} from './styles';
import {View} from 'react-native';

export default class Terms extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      terms: '',
    };
  }

  componentDidMount() {
    const {setNetworkIndicator} = this.props;
    setNetworkIndicator(true);
    apiGetTerms()
      .then((result) => {
        this.setState({
          terms: result.data.text,
        });
        setNetworkIndicator(false);
      })
      .catch((e) => {
        setNetworkIndicator(false);
        console.log(e);
      });
  }

  render() {
    const {terms} = this.state;
    console.log(terms);
    return (
      <Wrap titleView={<Title title="Пользовательское соглашение" />}>
        <View style={base.padding}>
          <WebView
            style={base.wrap}
            originWhitelist={['*']}
            source={{html: terms}}
          />
        </View>
      </Wrap>
    );
  }
}
