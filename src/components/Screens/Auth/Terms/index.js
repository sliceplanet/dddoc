import {connect} from 'react-redux';
import component from './component';

import {setNetworkIndicator} from '../../../../store/actions';

function mapDispatchToProps(dispatch) {
  return {
    setNetworkIndicator: (data) => dispatch(setNetworkIndicator(data)),
  };
}

export default connect(null, mapDispatchToProps)(component);
