import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
  },
  wrap: {
    flex: 1,
    width: wp(100) - 32,
  },
  padding: {
    padding: 16,
  },
});

export default {base};
