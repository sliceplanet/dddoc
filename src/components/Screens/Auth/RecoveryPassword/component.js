import React from 'react';
import {View, Text} from 'react-native';
import Image from 'react-native-scalable-image';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

// Components
import Wrap from '../../../UI/Base/Wrap';
import Title from '../../../UI/Header/Title';
import InputText from '../../../UI/Input/InputText';
import ButtonFull from '../../../UI/Button/ButtonFull';
import ButtonEmpty from '../../../UI/Button/ButtonEmpty';

// Helpers
import * as functions from './functions';
import * as Images from '../../../../helpers/images';
import {goBack} from '../../../../helpers/navigation';

// Style
import {base} from './styles';

export default class RecoveryPassword extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      email: '',
    };
  }

  onChangeEmail = (email) => this.setState({email});

  done = () => functions.done(this);

  render() {
    const {email} = this.state;

    return (
      <Wrap titleView={<Title title="Восстановление пароля" />}>
        <View style={base.flex} />
        <Image source={Images.logo} height={wp(20)} />
        <View style={base.flex} />
        <InputText
          title="E-mail"
          keyboardType="email-address"
          textContentType="emailAddress"
          value={email}
          onChangeText={this.onChangeEmail}
          // onSubmitEditing={this.done}
        />
        <View style={base.flex} />
        <ButtonFull title="Восстановить пароль" onPress={this.done} />
        <ButtonEmpty title="Отмена" onPress={goBack} />
        <View style={base.flex} />
      </Wrap>
    );
  }
}
