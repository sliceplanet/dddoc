import {connect} from 'react-redux';
import component from './component';

import {setToast} from '../../../../store/actions';
import {fetchPostUserForgotPassword} from '../../../../store/actions/auth';

function mapDispatchToProps(dispatch) {
  return {
    setToast: (data) => dispatch(setToast(data)),
    fetchPostUserForgotPassword: (data) =>
      dispatch(fetchPostUserForgotPassword(data)),
  };
}

export default connect(null, mapDispatchToProps)(component);
