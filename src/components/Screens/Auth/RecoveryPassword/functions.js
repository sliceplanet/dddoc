import * as validator from '../../../../helpers/validator';
import {Keyboard} from 'react-native';

export function done(_this) {
  Keyboard.dismiss();
  const {setToast, fetchPostUserForgotPassword} = _this.props;
  const {email} = _this.state;

  if (!validator.email(email)) {
    setToast('Введите e-mail');
    return;
  }

  fetchPostUserForgotPassword({email});
}
