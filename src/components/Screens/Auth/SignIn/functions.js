import {Keyboard} from 'react-native';
import * as validator from '../../../../helpers/validator';

export function done(_this) {
  Keyboard.dismiss();
  const {playerId, fetchPostUserLogin, setToast} = _this.props;
  const {email, password} = _this.state;

  if (!validator.email(email)) {
    setToast('Введите e-mail');
    return;
  }

  if (playerId && playerId.length === 0) {
    setToast('playerId не найден');
    return;
  }

  fetchPostUserLogin({
    email,
    password,
    playerId,
  });
}
