import {connect} from 'react-redux';
import component from './component';

import {setToast} from '../../../../store/actions';
import {fetchPostUserLogin} from '../../../../store/actions/auth';

function mapStateToProps(state) {
  return {
    playerId: state.playerId,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setToast: (data) => dispatch(setToast(data)),
    fetchPostUserLogin: (data) => dispatch(fetchPostUserLogin(data)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(component);
