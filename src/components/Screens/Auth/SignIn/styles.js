import {StyleSheet} from 'react-native';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 2,
  },
  text2: {
    fontFamily: 'SF Pro Text',
    fontSize: 15,
    color: 'rgba(60, 60, 67, 0.6)',
    marginRight: 4,
  },
  text3: {
    fontFamily: 'SF Pro Text',
    fontSize: 15,
    fontWeight: '600',
    color: '#D962A5',
  },
});

export default {base};
