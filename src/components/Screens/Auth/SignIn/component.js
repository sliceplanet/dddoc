import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import Image from 'react-native-scalable-image';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

// Components
import Wrap from '../../../UI/Base/Wrap';
import Title from '../../../UI/Header/Title';
import InputText from '../../../UI/Input/InputText';
import ButtonFull from '../../../UI/Button/ButtonFull';

// Helpers
import * as functions from './functions';
import * as Images from '../../../../helpers/images';
import {navigate, replace} from '../../../../helpers/navigation';

// Style
import {base} from './styles';

export default class SignIn extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      email: '',
      password: '',
    };
  }

  onPressForgotPassword = () => navigate('RecoveryPassword');

  onPressSignUp = () => replace('SignUp');

  onChangeEmail = (email) => this.setState({email});
  onChangePassword = (password) => this.setState({password});

  refPassword = (ref) => (this.password = ref);
  nextPassword = () => this.password.focus();

  done = () => functions.done(this);

  render() {
    const {email, password} = this.state;

    return (
      <Wrap titleView={<Title title="Авторизация" />}>
        <View style={base.flex} />
        <Image source={Images.logo} height={wp(20)} />
        <View style={base.flex} />
        <InputText
          title="E-mail"
          keyboardType="email-address"
          returnKeyType="next"
          textContentType="emailAddress"
          value={email}
          onChangeText={this.onChangeEmail}
          onSubmitEditing={this.nextPassword}
        />
        <InputText
          ref={this.refPassword}
          title="Пароль"
          textContentType="password"
          value={password}
          secureTextEntry
          forgetPassword
          returnKeyType="done"
          onPressForgotPassword={this.onPressForgotPassword}
          onChangeText={this.onChangePassword}
          onSubmitEditing={this.done}
        />
        <View style={base.flex} />
        <ButtonFull title="Войти" onPress={this.done} />
        <View style={base.row}>
          <Text style={base.text2}>Еще нет аккаунта?</Text>
          <TouchableOpacity onPress={this.onPressSignUp}>
            <Text style={base.text3}>Зарегистрироваться</Text>
          </TouchableOpacity>
        </View>
        <View style={base.flex} />
      </Wrap>
    );
  }
}
