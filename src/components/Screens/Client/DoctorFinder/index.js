import {connect} from 'react-redux';
import component from './component';

import {setToast, setNetworkIndicator} from '../../../../store/actions';
import {fetchPostDoctorFind} from '../../../../store/actions/doctor';

function mapStateToProps(state) {
  return {
    user: state.user,
    doctorFind: state.doctorFind,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setToast: (data) => dispatch(setToast(data)),
    setNetworkIndicator: (data) => dispatch(setNetworkIndicator(data)),
    fetchPostDoctorFind: (data) => dispatch(fetchPostDoctorFind(data)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(component);
