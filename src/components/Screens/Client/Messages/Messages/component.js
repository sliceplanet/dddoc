import React from 'react';
import {View, FlatList} from 'react-native';

// Components
import Wrap from '../../../../UI/Base/Wrap';
import Title from '../../../../UI/Header/Title';
import Footer from '../../../../UI/Footer';
import ItemMessage from '../../../../UI/Items/ItemMessage';

// Helpers
import * as functions from './functions';

// Style
import {base} from './styles';

export default class Messages extends React.Component {
  constructor(props) {
    super(props);

    functions.constructorState(this, props);
  }

  componentDidMount() {
    const {user, fetchGetChats} = this.props;
    const data = {
      id: 0,
      count: 20,
    };
    fetchGetChats({user, data});
  }

  onRefresh = () => {
    const {user, fetchGetChats} = this.props;
    const data = {
      id: 0,
      count: 20,
    };
    fetchGetChats({user, data});
  };

  renderItem = ({item, index}) => <ItemMessage key={index} {...item} />;

  renderSeparator = () => {
    return <View style={base.separator} />;
  };

  render() {
    const {chats} = this.props;
    let unread = 0;
    chats.forEach((e) => {
      if (e.unread) {
        unread += e.unread;
      }
    });

    return (
      <Wrap
        noScroll
        style={base.backgroundColor}
        titleView={
          <Title title={unread > 0 ? `Сообщения ${unread}` : 'Сообщения'} />
        }>
        <View style={base.flex}>
          <FlatList
            data={chats}
            extraData={this.props}
            renderItem={this.renderItem}
            onRefresh={this.onRefresh}
            refreshing={false}
            ItemSeparatorComponent={this.renderSeparator}
            keyExtractor={(item, index) => index.toString()}
          />
        </View>
        <Footer />
      </Wrap>
    );
  }
}
