import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
  },
  backgroundColor: {
    backgroundColor: '#EFEFF4',
  },
  separator: {
    width: wp(100),
    height: 0.7,
    backgroundColor: 'rgba(0, 0, 0, 0.3)',
  },
});

export default {base};
