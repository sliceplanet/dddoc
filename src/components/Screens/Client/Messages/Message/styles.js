import {StyleSheet, Platform} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
  },
  backgroundColor: {
    backgroundColor: '#EFEFF4',
  },
  wrap1: {
    width: wp(100),
    flexDirection: 'row',
    backgroundColor: 'white',
    paddingVertical: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 1,
  },
  wrap2: {
    flex: 1,
    width: wp(100),
    paddingBottom: 10,
  },
  toolbar: {
    paddingHorizontal: 14,
    paddingVertical: Platform.OS === 'ios' ? 10 : 0,
    marginHorizontal: 16,
    borderRadius: 16,
  },
  primary: {
    alignItems: 'center',
  },
  textInput: {
    borderLeftColor: '#EFEFF4',
    borderRightColor: '#EFEFF4',
    borderLeftWidth: 1,
    borderRightWidth: 1,
    paddingHorizontal: 10,
    marginHorizontal: 10,
    maxHeight: 160,
  },
  bubbleLeft: {
    borderRadius: 6,
    borderColor: '#D87BB0',
    borderWidth: 1,
    backgroundColor: 'white',
    minHeight: 20,
    justifyContent: 'flex-end',
    marginLeft: 8,
    marginBottom: 8,
  },
  bubbleRight: {
    borderRadius: 6,
    borderColor: '#75C9C3',
    borderWidth: 1,
    backgroundColor: 'white',
    minHeight: 20,
    justifyContent: 'flex-end',
    marginRight: 8,
    marginBottom: 8,
  },
  bubble: {
    borderRadius: 6,
    borderColor: '#ACACAC',
    borderWidth: 1,
  },
  textStyle: {
    fontFamily: 'SF Pro Text',
    fontSize: 14,
    lineHeight: 18,
    color: 'black',
  },
  timeTextStyle: {
    fontFamily: 'SF Pro Text',
    fontStyle: 'italic',
    fontSize: 11,
    lineHeight: 18,
    color: 'rgba(60, 60, 67, 0.6)',
  },
  dayWrapperStyle: {
    backgroundColor: '#75C9C3',
    borderRadius: 15,
  },
  dayTextStyle: {
    fontFamily: 'SF Pro Text',
    fontWeight: 'bold',
    fontSize: 11,
    lineHeight: 18,
    color: 'white',
    paddingHorizontal: 8,
  },
});

export default {base};
