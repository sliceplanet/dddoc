import React from 'react';
import {View, Keyboard} from 'react-native';
import {
  GiftedChat,
  InputToolbar,
  Composer,
  Bubble,
  Message as MessageView,
  Day,
  MessageText,
} from 'react-native-gifted-chat';

// Components
import Wrap from '../../../../UI/Base/Wrap';
import Title from '../../../../UI/Header/Title';
import Footer from '../../../../UI/Footer';
import ButtonMessage from '../../../../UI/Button/ButtonMessage';
import CustomActions from '../../../../UI/Message/CustomActions';
import CustomSend from '../../../../UI/Message/CustomSend';
import CustomMessageText from '../../../../UI/Message/CustomMessageText';
import CustomMessageVideo from '../../../../UI/Message/CustomMessageVideo';

// Helpers
import {navigate} from '../../../../../helpers/navigation';
import * as functions from './functions';
import {URL} from '../../../../../store/api';

// Style
import {base} from './styles';

export default class Message extends React.Component {
  constructor(props) {
    super(props);

    functions.constructorState(this, props);
  }

  componentDidMount() {
    const {chatId} = this.state;
    if (chatId !== 0) {
      const {user, fetchPostChatsId} = this.props;
      const path = {chatId};
      const data = {
        id: 0,
        count: 20,
      };
      fetchPostChatsId({data, user, path});
    }
  }

  componentDidUpdate(prevProps) {
    if (prevProps.chats.length !== this.props.chats.length) {
      const {_id, id} = this.state.user;
      const chat = this.props.chats.filter((e) => e.user._id === _id || id);
      if (chat.length > 0) {
        this.setState({
          chatId: chat[0].id,
        });
      }
    }
  }

  onSend = (messages = []) => {
    const {reducerAddMessage} = this.props;
    const {chatId} = this.state;
    const {_id, text, user} = messages[0];

    const {connection} = global;
    connection.invoke('send', {
      id: _id,
      chatId,
      text,
      to: user,
    });

    if (chatId !== 0) {
      reducerAddMessage(messages, chatId, user);
    }
    Keyboard.dismiss();
  };

  onPress = () => {
    const {_id, id} = this.state.user;
    navigate('Services', {id: id ? id : _id, type: 'DateReservation'});
  };

  onRefresh = () => {
    const {chatId} = this.state;
    if (chatId !== 0) {
      const {user, fetchPostChatsId} = this.props;
      const path = {chatId};
      const data = {
        id: 0,
        count: 20,
      };
      fetchPostChatsId({data, user, path});
    }
  };

  renderInputToolbar = (props) => {
    const {isAdmin} = this.state;
    if (isAdmin) {
      return null;
    }

    return (
      <InputToolbar
        {...props}
        containerStyle={base.toolbar}
        primaryStyle={base.primary}
      />
    );
  };

  renderCustomActions = (props) => {
    const {chatId} = this.state;
    const {isAdmin} = this.state;
    if (isAdmin) {
      return null;
    }

    return <CustomActions {...props} chatId={chatId} />;
  };

  renderComposer = (props) => {
    const {isAdmin} = this.state;
    if (isAdmin) {
      return null;
    }

    return (
      <Composer
        {...props}
        composerHeight="auto"
        placeholderTextColor="#ACACAC"
        textInputStyle={base.textInput}
      />
    );
  };

  renderSend = (props) => {
    const {isAdmin} = this.state;
    if (isAdmin) {
      return null;
    }

    return <CustomSend {...props} />;
  };

  renderBubble = (props) => {
    const {type} = props.currentMessage;
    return (
      <Bubble
        {...props}
        wrapperStyle={{
          left: [base.bubbleLeft, type === 1 && base.bubble],
          right: [base.bubbleRight, type === 1 && base.bubble],
        }}
      />
    );
  };

  renderMessage = (props) => {
    const {position, currentMessage} = props;

    if (position === 'left' && !currentMessage.isReaded) {
      const {chatId} = this.state;
      const {reducerReadedMessage} = this.props;
      const {connection} = global;

      connection.invoke('read', {
        messageId: currentMessage._id,
      });

      reducerReadedMessage(currentMessage._id, chatId);
    }

    return <MessageView {...props} />;
  };

  renderCustomView = (props) => {
    const {currentMessage} = props;

    if (currentMessage.type === 1) {
      return <CustomMessageVideo {...props} />;
    }
    return null;
  };

  renderMessageText = (props) => {
    const {currentMessage} = props;

    if (currentMessage.attachments) {
      return <CustomMessageText {...props} />;
    }

    return <MessageText {...props} />;
  };

  renderDay = (props) => {
    return (
      <Day
        {...props}
        wrapperStyle={base.dayWrapperStyle}
        textStyle={base.dayTextStyle}
      />
    );
  };

  renderAvatarSource = () => {
    const {chatId} = this.state;
    const {photo, avatar} = this.state.user;

    if (chatId === 0) {
      if (photo) {
        return {uri: `${URL}${photo}`};
      }
      if (avatar) {
        return {uri: `${URL}${avatar}`};
      }
      return null;
    } else {
      if (avatar) {
        return {uri: `${URL}${avatar}`};
      }
      if (photo) {
        return {uri: `${URL}${photo}`};
      }
      return null;
    }
  };

  renderName = () => {
    const {name, lastName} = this.state.user;
    let result = [];

    if (name) {
      result = [...result, name];
    }
    if (lastName) {
      result = [...result, lastName];
    }

    return result.join(' ');
  };

  renderBlock = () => {
    const {isSupport, isAdmin, user} = this.state;
    if (isSupport) {
      return null;
    }
    if (isAdmin) {
      return null;
    }
    if (user.role === 'client') {
      return null;
    }

    return (
      <View style={base.wrap1}>
        <View style={base.flex} />
        <ButtonMessage
          title="Забронировать видео-звонок"
          onPress={this.onPress}
        />
        <View style={base.flex} />
      </View>
    );
  };

  render() {
    const {chatId} = this.state;
    const {_id, id} = this.state.user;
    const chat = this.props.chats.filter((e) => e.id === chatId);
    const {messages} = chat.length > 0 ? chat[0] : {messages: []};
    return (
      <Wrap
        noScroll
        style={base.backgroundColor}
        titleView={
          <Title
            title={this.renderName()}
            avatar
            avatarSource={this.renderAvatarSource()}
          />
        }>
        <View style={base.wrap2}>
          {this.renderBlock()}
          <GiftedChat
            messages={messages}
            onSend={this.onSend}
            user={{_id: _id || id}}
            placeholder="Сообщение"
            timeFormat="HH:mm"
            dateFormat="DD.MM.YYYY"
            locale="ru"
            alwaysShowSend
            keyboardShouldPersistTaps="handled"
            renderInputToolbar={this.renderInputToolbar}
            renderActions={this.renderCustomActions}
            renderSend={this.renderSend}
            renderComposer={this.renderComposer}
            renderBubble={this.renderBubble}
            renderMessage={this.renderMessage}
            renderMessageText={this.renderMessageText}
            renderCustomView={this.renderCustomView}
            renderDay={this.renderDay}
            customTextStyle={base.textStyle}
            timeTextStyle={{
              left: base.timeTextStyle,
              right: base.timeTextStyle,
            }}
            listViewProps={{
              onRefresh: this.onRefresh,
              refreshing: false,
            }}
          />
        </View>
        <Footer />
      </Wrap>
    );
  }
}
