export function constructorState(_this, props) {
  const isSupport = props.route.params?.isSupport ?? false;
  const isAdmin = props.route.params?.isAdmin ?? false;
  const chatId = props.route.params?.chatId ?? null;
  const user = props.route.params?.user ?? null;
  const id = isSupport ? 0 : user.id || user._id;
  const chat = props.chats.filter((e) => e.user._id === id);

  _this.state = {
    chatId: isSupport ? chatId : chat.length > 0 ? chat[0].id : 0,
    isAdmin,
    isSupport,
    user,
  };
}
