import React from 'react';
import {View} from 'react-native';
import {Calendar} from 'react-native-calendars';

// Components
import Wrap from '../../../../UI/Base/Wrap';
import Title from '../../../../UI/Header/Title';
import Footer from '../../../../UI/Footer';
import Arrow from '../../../../UI/Arrow';
import ButtonFull from '../../../../UI/Button/ButtonFull';

// Helpers
import * as functions from './functions';
import {navigate} from '../../../../../helpers/navigation';

// Style
import {base} from './styles';

export default class DateReservation extends React.Component {
  constructor(props) {
    super(props);

    functions.constructorState(this, props);
  }

  onPress = () => {
    const {selected, serviceId, userId} = this.state;
    navigate('TimeReservation', {day: selected, serviceId, userId});
  };

  render() {
    const {selected} = this.state;
    return (
      <Wrap
        noScroll
        style={base.backgroundColor}
        titleView={<Title title="Выбор даты" />}>
        <View style={base.wrap1}>
          <Calendar
            style={base.width}
            minDate={new Date()}
            monthFormat={'MMMM'}
            firstDay={1}
            markedDates={{
              [selected]: {
                selected: true,
              },
            }}
            theme={{
              textSectionTitleColor: '#999999',
              todayTextColor: '#444444',
              dayTextColor: '#444444',
              textDisabledColor: '#CCCCCC',
              selectedDayTextColor: 'white',
              monthTextColor: 'black',
              textDayHeaderFontSize: 16,
              textDayFontSize: 16,
              textMonthFontSize: 24,
              textDayFontWeight: '600',
              textMonthFontWeight: '600',
              selectedDayBackgroundColor: '#D962A5',
            }}
            renderArrow={(direction) => <Arrow direction={direction} />}
            onDayPress={(day) => {
              this.setState({selected: day.dateString});
            }}
          />
          <View style={base.flex} />
          <ButtonFull
            disabled={!selected}
            title="Далее"
            style={base.marginVertical}
            onPress={this.onPress}
          />
        </View>
        <Footer />
      </Wrap>
    );
  }
}
