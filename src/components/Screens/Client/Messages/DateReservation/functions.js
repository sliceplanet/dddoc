export function constructorState(_this, props) {
  _this.state = {
    serviceId: props.route.params?.serviceId ?? -1,
    userId: props.route.params?.userId ?? 0,
    selected: '',
  };
}
