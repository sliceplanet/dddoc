import {connect} from 'react-redux';
import component from './component';

import {fetchPostVideosNew} from '../../../../../store/actions/videos';

function mapStateToProps(state) {
  return {
    user: state.user,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    fetchPostVideosNew: (data) => dispatch(fetchPostVideosNew(data)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(component);
