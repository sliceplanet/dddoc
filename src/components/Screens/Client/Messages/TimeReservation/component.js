import React from 'react';
import {View, Text, FlatList, Keyboard} from 'react-native';
import moment from 'moment';

// Components
import Wrap from '../../../../UI/Base/Wrap';
import Title from '../../../../UI/Header/Title';
import Footer from '../../../../UI/Footer';
import ItemTime from '../../../../UI/Items/ItemTime';
import ButtonFull from '../../../../UI/Button/ButtonFull';

// Helpers
import * as functions from './functions';
import {times} from '../../../../../helpers/constant';

// Style
import {base} from './styles';

export default class TimeReservation extends React.Component {
  constructor(props) {
    super(props);

    functions.constructorState(this, props);
  }

  onPressItem = (index) => this.setState({index});

  renderItem = ({item, index}) => {
    const {date} = this.state;

    if (moment(`${date} ${item}`) > moment()) {
      return (
        <ItemTime
          key={index}
          title={item}
          index={index}
          disabled={moment(`${date} ${item}`) <= moment()}
          checked={index === this.state.index}
          onPress={this.onPressItem}
        />
      );
    }
    return null;
  };

  renderSeparator = () => {
    return <View style={base.separator} />;
  };

  done = () => {
    Keyboard.dismiss();
    const {serviceId, userId, date, index} = this.state;
    const {user, fetchPostVideosNew} = this.props;

    const data = {
      date: moment(`${date} ${times[index]}`).format(),
      userId,
      serviceId,
      cito: false,
    };
    const goBack = 3;

    fetchPostVideosNew({user, data, goBack});
  };

  render() {
    const {day, index} = this.state;
    return (
      <Wrap
        noScroll
        style={base.backgroundColor}
        titleView={<Title title="Выбор времени (по МСК)" />}>
        <View style={base.wrap1}>
          <Text style={base.text1}>{day}</Text>
          <View style={base.wrap2}>
            <FlatList
              data={times}
              renderItem={this.renderItem}
              ItemSeparatorComponent={this.renderSeparator}
              keyExtractor={(item, i) => i.toString()}
            />
          </View>
          <ButtonFull
            style={base.marginVertical}
            disabled={index === -1}
            title="Забронировать"
            onPress={this.done}
          />
        </View>
        <Footer />
      </Wrap>
    );
  }
}
