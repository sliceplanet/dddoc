import React from 'react';
import {View, Text, Alert} from 'react-native';
import moment from 'moment';
import Image from 'react-native-scalable-image';

// Components
import Wrap from '../../../../UI/Base/Wrap';
import Title from '../../../../UI/Header/Title';
import Footer from '../../../../UI/Footer';
import AvatarDoctorFinder from '../../../../UI/Avatar/AvatarDoctorFinder';

// Helpers
import * as functions from './functions';
import * as Images from '../../../../../helpers/images';
import {URL} from '../../../../../store/api';

// Style
import {base} from './styles';

export default class Notification extends React.Component {
  constructor(props) {
    super(props);
    functions.constructorState(this, props);
  }

  onPressRemove = () => {
    const {user, fetchDeleteNotifications} = this.props;
    const {id} = this.state;
    const path = {
      id,
    };

    Alert.alert(
      'Удалить уведомление',
      'Желаете удалить уведомление?',
      [
        {text: 'Да', onPress: () => fetchDeleteNotifications({user, path})},
        {text: 'Нет'},
      ],
      {cancelable: false},
    );
  };

  renderNotify = () => {
    const {type} = this.state;

    switch (type) {
      case 1: {
        return 'Верификация пройдена';
      }
      case 2: {
        return 'Запрос на бронирование нового видео звонка';
      }
      case 3: {
        return 'Запрос на срочный видео звонок';
      }
      case 4: {
        return 'Видео звонок отменен';
      }
      case 5: {
        return 'Видео звонок подтвержден';
      }
      case 6: {
        return 'Видео звонок оплачен';
      }
      case 11: {
        return 'Вызов видео звонка';
      }
      case 12: {
        return 'Для возможности предоставления услуг необходимо верифицировать аккаунт';
      }
      case 13: {
        return 'Видео звонок закончен';
      }
      case 14: {
        return 'Необходимо оплатить видео звонок';
      }
      case 15: {
        return 'Пришло время консультации';
      }
      case 16: {
        return 'Консультация состоялось';
      }
      case 17: {
        return 'Консультация не состоялась';
      }
      case 18: {
        return 'Новый отзыв';
      }
      case 19: {
        return 'Изменение баланса';
      }
      case 20: {
        return 'Отклонить видео звонок';
      }
      case 21: {
        return 'Подтвердить видео звонок ';
      }
    }
    return 'Текст уведомления';
  };

  renderName = () => {
    const {name, lastName, patronymic} = this.state.from;
    let result = [];

    if (name) {
      result = [...result, name];
    }
    if (lastName) {
      result = [...result, lastName];
    }
    if (patronymic) {
      result = [...result, patronymic];
    }

    return result.join(' ');
  };

  render() {
    const {avatar} = this.state.from;
    const {date} = this.state.extra;
    return (
      <Wrap
        noScroll
        style={base.backgroundColor}
        titleView={<Title title="Уведомление" />}>
        <View style={base.wrap1}>
          <View style={base.wrap2}>
            <View style={base.row}>
              <AvatarDoctorFinder source={avatar && {uri: `${URL}${avatar}`}} />
              <View style={base.wrap3}>
                <Text style={base.text1} numberOfLines={2}>
                  {this.renderName()}
                </Text>
                <Text style={base.text2}>{this.renderNotify()}</Text>
              </View>
              <View style={base.flex} />
              <View style={base.cross}>
                <Image
                  source={Images.cross}
                  width={24}
                  onPress={this.onPressRemove}
                />
              </View>
            </View>
            <View style={[base.row, base.marginTop]}>
              <View style={base.wrap4}>
                <Text style={base.text3}>Дата консультации:</Text>
                <Text style={base.text3}>Время консультации:</Text>
              </View>
              <View style={base.wrap4}>
                <Text style={base.text4}>
                  {moment(date).format('DD.MM.YYYY')}
                </Text>
                <Text style={base.text4}>{moment(date).format('HH:mm')}</Text>
              </View>
            </View>
          </View>
        </View>
        <Footer />
      </Wrap>
    );
  }
}
