import {connect} from 'react-redux';
import component from './component';

import {setToast} from '../../../../../store/actions';
import {fetchGetNotifications} from '../../../../../store/actions/notifications';

function mapStateToProps(state) {
  return {
    user: state.user,
    notifications: state.notifications,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setToast: (data) => dispatch(setToast(data)),
    fetchGetNotifications: (data) => dispatch(fetchGetNotifications(data)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(component);
