import React from 'react';
import {View, FlatList} from 'react-native';

// Components
import Wrap from '../../../../UI/Base/Wrap';
import Title from '../../../../UI/Header/Title';
import Footer from '../../../../UI/Footer';
import ItemNotification from '../../../../UI/Items/ItemNotification';

// Helpers
import * as functions from './functions';

// Style
import {base} from './styles';

export default class Notifications extends React.Component {
  constructor(props) {
    super(props);
    functions.constructorState(this, props);
  }

  componentDidMount() {
    const {user, fetchGetNotifications} = this.props;
    const path = {
      page: 1,
      pageSize: 100,
    };
    fetchGetNotifications({user, path});
  }

  renderItem = ({item, index}) => <ItemNotification key={index} {...item} />;

  renderSeparator = () => {
    return <View style={base.separator} />;
  };

  render() {
    const {notifications} = this.props;

    return (
      <Wrap
        noScroll
        style={base.backgroundColor}
        titleView={<Title title="Уведомления" />}>
        <View style={base.wrap1}>
          <FlatList
            data={notifications}
            extraData={this.props}
            renderItem={this.renderItem}
            onRefresh={this.onRefresh}
            refreshing={false}
            ItemSeparatorComponent={this.renderSeparator}
            keyExtractor={(item, index) => index.toString()}
          />
        </View>
        <Footer />
      </Wrap>
    );
  }
}
