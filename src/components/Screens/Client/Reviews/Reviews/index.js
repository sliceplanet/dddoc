import {connect} from 'react-redux';
import component from './component';

import {setToast} from '../../../../../store/actions';
import {
  fetchGetReviewsMy,
  fetchGetReviewsFrom,
} from '../../../../../store/actions/reviews';

function mapStateToProps(state) {
  return {
    user: state.user,
    reviews: state.reviews,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    fetchGetReviewsMy: (data) => dispatch(fetchGetReviewsMy(data)),
    fetchGetReviewsFrom: (data) => dispatch(fetchGetReviewsFrom(data)),
    setToast: (data) => dispatch(setToast(data)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(component);
