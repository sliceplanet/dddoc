import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
  },
  wrap1: {
    width: wp(100),
    alignItems: 'center',
    paddingVertical: 6,
    backgroundColor: 'white',
  },
  wrap2: {
    width: wp(100) - 28,
    backgroundColor: 'rgba(118, 118, 128, 0.12)',
    flexDirection: 'row',
    borderRadius: 9,
    padding: 2,
  },
  wrap3: {
    flex: 1,
    backgroundColor: '#75C9C3',
    borderRadius: 8,
  },
  text1: {
    flex: 1,
    paddingVertical: 7,
    fontFamily: 'SF Pro Text',
    fontSize: 13,
    lineHeight: 18,
    fontWeight: '500',
    color: 'black',
    textAlign: 'center',
  },
  text2: {
    paddingVertical: 7,
    fontFamily: 'SF Pro Text',
    fontSize: 14,
    lineHeight: 18,
    fontWeight: '600',
    color: 'white',
    textAlign: 'center',
  },
});

export default {base};
