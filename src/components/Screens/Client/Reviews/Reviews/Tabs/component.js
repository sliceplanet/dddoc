import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';

// Helpers
import * as functions from './functions';

// Style
import {base} from './styles';

export default class Reviews extends React.Component {
  constructor(props) {
    super(props);
    functions.constructorState(this, props);
  }

  renderText = (text, index) => {
    const {tabIndex, onChange} = this.props;

    if (tabIndex !== index) {
      return (
        <TouchableOpacity style={base.flex} onPress={() => onChange(index)}>
          <Text style={base.text1}>{text}</Text>
        </TouchableOpacity>
      );
    }
    return (
      <TouchableOpacity style={base.wrap3} onPress={() => onChange(index)}>
        <Text style={base.text2}>{text}</Text>
      </TouchableOpacity>
    );
  };

  render() {
    return (
      <View style={base.wrap1}>
        <View style={base.wrap2}>
          {this.renderText('О себе', 0)}
          {this.renderText('Оставленные', 1)}
        </View>
      </View>
    );
  }
}
