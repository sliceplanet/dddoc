import {StyleSheet} from 'react-native';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
  },
  backgroundColor: {
    backgroundColor: '#EFEFF4',
  },
  wrap1: {
    flex: 1,
    alignItems: 'center',
  },
});

export default {base};
