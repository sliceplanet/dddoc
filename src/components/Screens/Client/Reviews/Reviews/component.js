import React from 'react';
import {View, FlatList} from 'react-native';

// Components
import Wrap from '../../../../UI/Base/Wrap';
import Title from '../../../../UI/Header/Title';
import Footer from '../../../../UI/Footer';
import ItemReviewsMy from '../../../../UI/Items/ItemReviews/ItemReviewsMy';
import ItemReviewsFrom from '../../../../UI/Items/ItemReviews/ItemReviewsFrom';
import Tabs from './Tabs';

// Helpers
import * as functions from './functions';

// Style
import {base} from './styles';

export default class Reviews extends React.Component {
  constructor(props) {
    super(props);
    functions.constructorState(this, props);
  }

  componentDidMount() {
    const {user, fetchGetReviewsMy, fetchGetReviewsFrom} = this.props;
    const path = {
      page: 1,
      pageSize: 100,
    };
    fetchGetReviewsMy({user, path});
    fetchGetReviewsFrom({user, path});
  }

  onChangeTabs = (tabIndex) => this.setState({tabIndex});

  onRefresh = () => {};

  renderItemMy = ({item, index}) => <ItemReviewsMy key={index} {...item} />;
  renderItemFrom = ({item, index}) => <ItemReviewsFrom key={index} {...item} />;

  render() {
    const {tabIndex} = this.state;
    const {my, from} = this.props.reviews;

    return (
      <Wrap style={base.backgroundColor} titleView={<Title title="Отзывы" />}>
        <View style={base.wrap1}>
          <Tabs tabIndex={tabIndex} onChange={this.onChangeTabs} />
          <FlatList
            data={tabIndex === 0 ? my.items : from.items}
            extraData={this.props}
            renderItem={
              tabIndex === 0 ? this.renderItemMy : this.renderItemFrom
            }
            onRefresh={this.onRefresh}
            refreshing={false}
            keyExtractor={(item, index) => index.toString()}
          />
        </View>
        <Footer />
      </Wrap>
    );
  }
}
