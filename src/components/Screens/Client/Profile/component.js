import React from 'react';

// Components
import Wrap from '../../../UI/Base/Wrap';
import Title from '../../../UI/Header/Title';
import PickerGender from '../../../UI/Picker/PickerGender';
import Card from '../../../UI/Card';
import Footer from '../../../UI/Footer';
import Avatar from '../../../UI/Avatar';
import InputTextEdit from '../../../UI/Input/InputTextEdit';
import InputDate from '../../../UI/Input/InputDate';
import InputCountryEdit from '../../../UI/Input/InputCountryEdit';

// Helpers
import * as functions from './functions';

// Style
import {base} from './styles';

export default class Profile extends React.Component {
  constructor(props) {
    super(props);
    functions.constructorState(this, props);
  }

  componentDidUpdate(prevProps) {
    if (prevProps.profile !== this.props.profile) {
      functions.profileComponentDidUpdate(this);
    }
  }

  onPressAvatar = (base64, photo, contentType) =>
    this.setState({base64, photo, contentType});
  onPressCancel = () => functions.onPressCancel(this);
  onPressButton = () => functions.onPressButton(this);

  onChangePickerGender = (gender) => this.setState({gender});

  onChangeLastName = (lastName) => this.setState({lastName});
  onChangeName = (name) => this.setState({name});
  onChangePatronymic = (patronymic) => this.setState({patronymic});

  onChangeBirthDate = (birthDate) => this.setState({birthDate});

  onChangePhone = (phone) => this.setState({phone});
  onChangeEmail = (email) => this.setState({email});
  onChangeAddress = (address) => this.setState({address});

  onChangeCountry = (country) => this.setState({country});

  onChangeUserName = (userName) => this.setState({userName});

  onChangeCity = (city) => this.setState({city});
  onChangeDescription = (description) => this.setState({description});
  onChangeOldPassword = (oldPassword) => this.setState({oldPassword});
  onChangeNewPassword = (newPassword) => this.setState({newPassword});
  onChangeConfNewPassword = (confNewPassword) =>
    this.setState({confNewPassword});

  onBlur = () => {
    functions.onPressSave(this);
  };

  refName = (ref) => (this.name = ref);
  refPatronymic = (ref) => (this.patronymic = ref);
  refBirthDate = (ref) => (this.birthDate = ref);
  refEmail = (ref) => (this.email = ref);
  refCountry = (ref) => (this.country = ref);
  refNewPassword = (ref) => (this.newPassword = ref);
  refConfNewPassword = (ref) => (this.confNewPassword = ref);

  nextName = () => this.name.focus();
  nextPatronymic = () => this.patronymic.focus();
  nextEmail = () => this.email.focus();
  nextCountry = () => this.country.focus();
  nextCity = () => this.city.focus();
  nextNewPassword = () => this.newPassword.focus();
  nextConfNewPassword = () => this.confNewPassword.focus();

  render() {
    const {
      userName,
      gender,
      photo,
      lastName,
      name,
      patronymic,
      birthDate,
      phone,
      email,
      address,
      country,
      city,
      description,
      oldPassword,
      newPassword,
      confNewPassword,
    } = this.state;

    return (
      <Wrap
        style={base.backgroundColor}
        titleView={
          <Title
            title="Профиль"
            button="Сохранить"
            onPressButton={this.onBlur}
          />
        }>
        <Card title="Пол">
          <PickerGender
            index={gender}
            editable
            onChange={this.onChangePickerGender}
          />
        </Card>
        <Card title="Пользователь">
          <Avatar
            source={photo}
            nickname={userName}
            editable
            onPress={this.onPressAvatar}
            onChangeText={this.onChangeUserName}
          />
          <InputTextEdit
            title="Фамилия"
            textContentType="namePrefix"
            returnKeyType="next"
            autoCapitalize="sentences"
            value={lastName}
            editable
            onChangeText={this.onChangeLastName}
            onSubmitEditing={this.nextName}
          />
          <InputTextEdit
            ref={this.refName}
            title="Имя"
            textContentType="name"
            returnKeyType="next"
            autoCapitalize="sentences"
            value={name}
            editable
            onChangeText={this.onChangeName}
            onSubmitEditing={this.nextPatronymic}
          />
          <InputTextEdit
            ref={this.refPatronymic}
            title="Отчество"
            textContentType="nameSuffix"
            returnKeyType="next"
            autoCapitalize="sentences"
            value={patronymic}
            editable
            onChangeText={this.onChangePatronymic}
          />
          <InputDate
            ref={this.refBirthDate}
            title="Дата рождения"
            value={birthDate}
            editable
            onChange={this.onChangeBirthDate}
          />
        </Card>
        <Card title="Контакты">
          <InputTextEdit
            title="Телефон"
            textContentType="telephoneNumber"
            returnKeyType="next"
            keyboardType="phone-pad"
            // mask="[0000000000999]"
            value={phone}
            editable
            onChangeText={this.onChangePhone}
            onSubmitEditing={this.nextEmail}
          />
          <InputTextEdit
            ref={this.refEmail}
            textContentType="emailAddress"
            title="E-mail"
            keyboardType="email-address"
            value={email}
            editable
            onChangeText={this.onChangeEmail}
          />
        </Card>
        <Card title="Место жительства">
          <InputTextEdit
            title="Адрес"
            textContentType="fullStreetAddress"
            returnKeyType="next"
            autoCapitalize="sentences"
            value={address}
            editable
            onChangeText={this.onChangeAddress}
            onSubmitEditing={this.nextCountry}
          />
          <InputCountryEdit
            ref={this.refCountry}
            title="Страна"
            editable
            value={country}
            onChange={this.onChangeCountry}
          />

          <InputTextEdit
            ref={this.refCity}
            title="Город"
            textContentType="addressCity"
            autoCapitalize="sentences"
            value={city}
            editable
            onChangeText={this.onChangeCity}
          />
        </Card>
        <Card title="Информация обо мне">
          <InputTextEdit
            autoCapitalize="sentences"
            value={description}
            multiline
            editable
            onChangeText={this.onChangeDescription}
          />
        </Card>
        <Card title="Управление паролем">
          <InputTextEdit
            title="Старый пароль"
            textContentType="password"
            returnKeyType="next"
            value={oldPassword}
            secureTextEntry
            editable
            onChangeText={this.onChangeOldPassword}
            onSubmitEditing={this.nextNewPassword}
          />
          <InputTextEdit
            ref={this.refNewPassword}
            title="Новый пароль"
            textContentType="newPassword"
            returnKeyType="next"
            value={newPassword}
            secureTextEntry
            editable
            onChangeText={this.onChangeNewPassword}
            onSubmitEditing={this.nextConfNewPassword}
          />
          <InputTextEdit
            ref={this.refConfNewPassword}
            title="Повторите новый пароль"
            textContentType="newPassword"
            value={confNewPassword}
            secureTextEntry
            editable
            onChangeText={this.onChangeConfNewPassword}
          />
        </Card>
        <Footer style={base.footer} />
      </Wrap>
    );
  }
}
