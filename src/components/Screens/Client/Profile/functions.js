import moment from 'moment';
import {Alert, Keyboard} from 'react-native';
import * as validator from '../../../../helpers/validator';
import {URL} from '../../../../store/api';

export function constructorState(_this, props) {
  const {
    userName,
    photo,
    gender,
    lastName,
    name,
    patronymic,
    birthDate,
    phone,
    email,
    address,
    country,
    city,
    description,
  } = props.profile;

  _this.state = {
    userName: userName || '',
    gender: gender || 1,
    base64: '',
    photo: photo ? URL + photo : '',
    lastName: lastName || '',
    name: name || '',
    patronymic: patronymic || '',
    birthDate: birthDate ? moment(birthDate) : null,
    phone: phone || '',
    email: email || '',
    address: address || '',
    country: country || '',
    city: city || '',
    description: description || '',
    oldPassword: '',
    newPassword: '',
    confNewPassword: '',
    contentType: '',
  };
}

export function profileComponentDidUpdate(_this) {
  const {
    userName,
    photo,
    gender,
    lastName,
    name,
    patronymic,
    birthDate,
    phone,
    email,
    address,
    country,
    city,
    description,
  } = _this.props.profile;

  _this.setState({
    userName: userName || '',
    gender: gender || 1,
    photo: photo ? URL + photo : '',
    lastName: lastName || '',
    name: name || '',
    patronymic: patronymic || '',
    birthDate: birthDate ? moment(birthDate) : null,
    phone: phone || '+',
    email: email || '',
    address: address || '',
    country: country || '',
    city: city || '',
    description: description || '',
    oldPassword: '',
    newPassword: '',
    confNewPassword: '',
  });
}

export function onPressSave(_this) {
  Keyboard.dismiss();
  Alert.alert(
    'Сохранение',
    'Желаете сохранить информацию?',
    [
      {
        text: 'Да',
        onPress: () => {
          const {fetchPutClientData, setToast} = _this.props;

          const {user} = _this.props;
          const {
            gender,
            userName,
            base64,
            lastName,
            name,
            patronymic,
            birthDate,
            phone,
            email,
            address,
            country,
            city,
            description,
            oldPassword,
            newPassword,
            confNewPassword,
            contentType,
          } = _this.state;
          let data = {
            gender,
            userName,
            lastName,
            name,
            patronymic,
            phone,
            email,
            address,
            country,
            city,
            description,
          };

          if (moment(birthDate).isValid()) {
            data.birthDate = moment(birthDate).format();
          }
          if (name.length === 0) {
            setToast('Введите имя');
            return;
          }
          // if (lastName.length === 0) {
          //   setToast('Введите фамилию');
          //   return;
          // }
          // if (patronymic.length === 0) {
          //   setToast('Введите отчество');
          //   return;
          // }
          // if (city.length === 0) {
          //   setToast('Введите город');
          //   return;
          // }
          // if (country.length === 0) {
          //   setToast('Введите страну');
          //   return;
          // }
          if (!validator.phone(phone)) {
            setToast('Введите телефон');
            return;
          }
          if (!validator.email(email)) {
            setToast('Введите e-mail');
            return;
          }
          if (oldPassword.length > 0) {
            if (!validator.password(oldPassword)) {
              setToast('Старый пароль не прошел проверку');
              return;
            }
            if (!validator.password(newPassword)) {
              setToast('Новый пароль не прошел проверку');
              return;
            }
            if (newPassword !== confNewPassword) {
              setToast('Пароли не совпадают');
              return;
            }
            data.oldPassword = oldPassword;
            data.newPassword = newPassword;
          }
          fetchPutClientData({data, base64, contentType, user});
        },
      },
      {text: 'Нет'},
    ],
    {cancelable: false},
  );
}

export function onPressCancel(_this) {
  const {
    userName,
    photo,
    gender,
    lastName,
    name,
    patronymic,
    birthDate,
    phone,
    email,
    address,
    country,
    city,
    description,
  } = _this.props.profile;

  _this.setState({
    buttonIndex: 0,
    userName: userName || '',
    gender: gender || 1,
    photo: photo ? URL + photo : '',
    lastName: lastName || '',
    name: name || '',
    patronymic: patronymic || '',
    birthDate: birthDate ? moment(birthDate) : null,
    phone: phone || '+',
    email: email || '',
    address: address || '',
    country: country || '',
    city: city || '',
    description: description || '',
    oldPassword: '',
    newPassword: '',
    confNewPassword: '',
  });
}

export function onPressButton(_this) {
  const {buttonIndex} = _this.state;
  switch (buttonIndex) {
    case 0: {
      _this.setState({buttonIndex: 1});
      return;
    }
    case 1: {
      Alert.alert(
        'Сохранить изменения',
        'Желаете сохранить изменения?',
        [
          {text: 'Сохранить', onPress: _this.onPressSave},
          {text: 'Отмена', onPress: _this.onPressCancel},
        ],
        {cancelable: false},
      );
      _this.setState({buttonIndex: 0});
      return;
    }
  }
}
