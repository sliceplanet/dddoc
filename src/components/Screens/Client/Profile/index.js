import {connect} from 'react-redux';
import component from './component';

import {setToast} from '../../../../store/actions';
import {fetchPutClientData} from '../../../../store/actions/personal';

function mapStateToProps(state) {
  return {
    user: state.user,
    profile: state.profile,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setToast: (data) => dispatch(setToast(data)),
    fetchPutClientData: (data) => dispatch(fetchPutClientData(data)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(component);
