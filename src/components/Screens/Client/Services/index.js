import {connect} from 'react-redux';
import component from './component';

import {setToast} from '../../../../store/actions';
import {fetchPostVideosNew} from '../../../../store/actions/videos';

function mapStateToProps(state) {
  return {
    user: state.user,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setToast: (data) => dispatch(setToast(data)),
    fetchPostVideosNew: (data) => dispatch(fetchPostVideosNew(data)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(component);
