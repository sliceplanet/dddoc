import {connect} from 'react-redux';
import component from './component';

import {setToast} from '../../../../../store/actions';
import {reducerDidTheVideoCallTakePlace} from '../../../../../store/actions/alert';

function mapStateToProps(state) {
  return {
    user: state.user,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setToast: (data) => dispatch(setToast(data)),
    reducerDidTheVideoCallTakePlace: (id) =>
      dispatch(reducerDidTheVideoCallTakePlace(id)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(component);
