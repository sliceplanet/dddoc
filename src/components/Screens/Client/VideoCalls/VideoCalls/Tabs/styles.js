import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
  },
  wrap1: {
    width: wp(100),
    paddingHorizontal: 24,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'white',
    marginBottom: 12,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 1,
  },
  wrap2: {
    marginTop: 14,
    marginBottom: 16,
  },
  under1: {
    borderBottomColor: 'black',
    borderBottomWidth: 1,
  },
  under2: {
    borderBottomColor: '#FF9B85',
    borderBottomWidth: 1,
  },
  under3: {
    borderBottomColor: '#D962A5',
    borderBottomWidth: 1,
  },
  text1: {
    fontFamily: 'SF Pro Text',
    fontSize: 13,
    lineHeight: 22,
    color: 'black',
  },
  text2: {
    fontFamily: 'SF Pro Text',
    fontSize: 13,
    lineHeight: 22,
    color: '#FF9B85',
  },
  text3: {
    fontFamily: 'SF Pro Text',
    fontSize: 13,
    lineHeight: 22,
    color: '#D962A5',
  },
});

export default {base};
