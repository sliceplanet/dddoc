import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';

// Style
import {base} from './styles';

export default class Arrow extends React.Component {
  onPressTab1 = () => {
    const {value, onChange} = this.props;
    if (value !== 1) {
      onChange(1);
    }
  };

  onPressTab2 = () => {
    const {value, onChange} = this.props;
    if (value !== 2) {
      onChange(2);
    }
  };

  onPressTab3 = () => {
    const {value, onChange} = this.props;
    if (value !== 3) {
      onChange(3);
    }
  };

  render() {
    const {value, all, awaiting, scheduled} = this.props;

    return (
      <View style={base.wrap1}>
        <TouchableOpacity onPress={this.onPressTab1}>
          <View style={[base.wrap2, value === 1 && base.under1]}>
            <Text style={base.text1}>Все {all > 0 && `(${all})`}</Text>
          </View>
        </TouchableOpacity>
        <View style={base.flex} />
        <TouchableOpacity onPress={this.onPressTab2}>
          <View style={[base.wrap2, value === 2 && base.under2]}>
            <Text style={base.text2}>
              Ожидают {awaiting > 0 && `(${awaiting})`}
            </Text>
          </View>
        </TouchableOpacity>
        <View style={base.flex} />
        <TouchableOpacity onPress={this.onPressTab3}>
          <View style={[base.wrap2, value === 3 && base.under3]}>
            <Text style={base.text3}>
              Запланированы {scheduled > 0 && `(${scheduled})`}
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}
