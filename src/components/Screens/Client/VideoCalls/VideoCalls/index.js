import {connect} from 'react-redux';
import component from './component';

import {setToast} from '../../../../../store/actions';
import {fetchGetVideos} from '../../../../../store/actions/videos';

function mapStateToProps(state) {
  return {
    user: state.user,
    videos: state.videos,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    fetchGetVideos: (data) => dispatch(fetchGetVideos(data)),
    setToast: (data) => dispatch(setToast(data)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(component);
