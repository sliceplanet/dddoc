import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
  },
  backgroundColor: {
    backgroundColor: '#EFEFF4',
  },
  row: {
    flexDirection: 'row',
    alignSelf: 'flex-start',
    alignItems: 'center',
    paddingHorizontal: 16,
  },
  wrap1: {
    flex: 1,
  },
  text1: {
    flex: 1,
    fontFamily: 'SF Pro Text',
    fontSize: 17,
    lineHeight: 22,
    color: 'black',
  },
});

export default {base};
