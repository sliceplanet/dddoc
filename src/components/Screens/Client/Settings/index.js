import {connect} from 'react-redux';
import component from './component';

import {setToast} from '../../../../store/actions';
import {reducerPush} from '../../../../store/actions/settings';

function mapStateToProps(state) {
  return {
    user: state.user,
    settings: state.settings,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setToast: (data) => dispatch(setToast(data)),
    reducerPush: (data) => dispatch(reducerPush(data)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(component);
