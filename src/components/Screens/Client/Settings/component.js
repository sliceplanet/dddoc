import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import {Switch} from 'react-native-switch';

// Components
import Wrap from '../../../UI/Base/Wrap';
import Title from '../../../UI/Header/Title';
import Footer from '../../../UI/Footer';
import Card from '../../../UI/Card';

// Helpers

// Style
import {base} from './styles';

export default class Settings extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  onValueChange = () => {
    const {push} = this.props.settings;
    this.props.reducerPush(!push);
  };

  render() {
    const {push} = this.props.settings;

    return (
      <Wrap
        style={base.backgroundColor}
        titleView={<Title title="Настройки" />}>
        <View style={base.wrap1}>
          <Card title="PUSH уведомления">
            <TouchableOpacity style={base.row} onPress={this.onValueChange}>
              <Text style={base.text1}>{push ? 'Включены' : 'Выключены'}</Text>
              <Switch
                value={push}
                onValueChange={this.onValueChange}
                backgroundActive="#D962A5"
                circleActiveBorderColor="#D962A5"
                backgroundInactive="#ACACAC"
                circleInactiveBorderColor="#ACACAC"
                circleBorderWidth={2}
              />
            </TouchableOpacity>
          </Card>
        </View>
        <Footer />
      </Wrap>
    );
  }
}
