import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
  },
  backgroundColor: {
    backgroundColor: '#EFEFF4',
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 2,
  },
  alignItems: {
    alignItems: 'center',
  },
  tintColor: {
    tintColor: '#75C9C3',
  },
  tintColorImage: {
    tintColor: '#ACACAC',
  },
  line: {
    width: wp(100) - 32,
    height: 0.7,
    backgroundColor: 'black',
  },
  underline: {
    borderBottomColor: '#D962A5',
    borderBottomWidth: 0.7,
    marginTop: 8,
    marginBottom: 12,
  },
  wrap1: {
    flex: 1,
  },
  wrap2: {
    width: wp(100),
    flexDirection: 'row',
    alignItems: 'center',
    padding: 16,
    backgroundColor: 'white',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 1,
    marginBottom: 12,
  },
  wrap3: {
    flex: 1,
    marginHorizontal: 16,
  },
  wrap4: {
    width: wp(100),
    alignItems: 'center',
    paddingHorizontal: 16,
    backgroundColor: 'white',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 1,
    marginBottom: 12,
  },
  text1: {
    fontFamily: 'SF Pro Text',
    fontSize: 15,
    lineHeight: 18,
    fontWeight: 'bold',
    color: 'black',
  },
  text2: {
    fontFamily: 'SF Pro Text',
    fontSize: 15,
    lineHeight: 18,
    color: '#D962A5',
    paddingVertical: 4,
  },
  text3: {
    fontFamily: 'SF Pro Text',
    fontSize: 15,
    lineHeight: 18,
    color: 'black',
    marginRight: 8,
  },
  text4: {
    fontWeight: 'bold',
  },
  text5: {
    fontFamily: 'SF Pro Text',
    fontSize: 16,
    fontWeight: 'bold',
    lineHeight: 19,
    color: '#D962A5',
    marginHorizontal: 4,
  },
  text6: {
    fontFamily: 'SF Pro Text',
    fontSize: 10,
    fontWeight: 'bold',
    lineHeight: 12,
    color: '#75C9C3',
    marginHorizontal: 4,
    marginTop: 2,
  },
  text7: {
    fontFamily: 'SF Pro Text',
    fontSize: 13,
    lineHeight: 18,
    color: 'rgba(60, 60, 67, 0.6)',
    marginHorizontal: 16,
    marginBottom: 6,
    marginTop: 4,
  },
  text8: {
    fontFamily: 'SF Pro Text',
    fontSize: 13,
    lineHeight: 17,
    color: 'black',
    paddingVertical: 16,
    alignSelf: 'flex-start',
  },
  text9: {
    fontFamily: 'SF Pro Text',
    fontSize: 16,
    lineHeight: 22,
    color: '#D962A5',
  },
  text10: {
    fontFamily: 'SF Pro Text',
    fontSize: 10,
    fontWeight: 'bold',
    lineHeight: 12,
    color: '#ACACAC',
    marginHorizontal: 4,
    marginTop: 2,
  },
  text11: {
    fontFamily: 'SF Pro Text',
    fontSize: 10,
    fontWeight: 'bold',
    lineHeight: 12,
    color: '#ACACAC',
    marginHorizontal: 4,
    marginTop: 2,
  },
});

export default {base};
