import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import Image from 'react-native-scalable-image';
import moment from 'moment';

// Components
import Wrap from '../../../UI/Base/Wrap';
import Title from '../../../UI/Header/Title';
import Footer from '../../../UI/Footer';
import AvatarDoctorFinder from '../../../UI/Avatar/AvatarDoctorFinder';
import Rating from '../../../UI/Rating';
import InputTextEdit from '../../../UI/Input/InputTextEdit';
import InputSpecializations from '../../../UI/Input/InputSpecializations';
import InputCategoryEdit from '../../../UI/Input/InputCategoryEdit';

// Helpers
import * as functions from './functions';
import * as Images from '../../../../helpers/images';
import {navigate} from '../../../../helpers/navigation';
import {URL} from '../../../../store/api';

// Style
import {base} from './styles';

export default class Doctor extends React.Component {
  constructor(props) {
    super(props);
    functions.constructorState(this, props);
  }

  onPressFavorite = () => {
    const {isFavourite} = this.state;
    const {user, fetchPutFavoritesId, fetchDeleteFavoritesId} = this.props;
    const path = {
      id: this.state.id,
    };

    if (!isFavourite) {
      fetchPutFavoritesId({user, path});
      this.setState({
        isFavourite: true,
      });
    } else {
      fetchDeleteFavoritesId({user, path});
      this.setState({
        isFavourite: false,
      });
    }
  };

  onPressMessage = () => navigate('Message', {user: this.state});

  onPressVideoCall = () => {
    const {id} = this.state;
    navigate('Services', {id, type: 'fetchPostVideosNew'});
  };

  onPressServices = () => {
    const {id} = this.state;
    navigate('Services', {id, type: 'DateReservation'});
  };

  onPressReview = () => {
    const {id} = this.state;
    navigate('Review', {id});
  };

  renderPrice = () => {
    const {minPrice, maxPrice} = this.state;

    if (minPrice) {
      if (minPrice === maxPrice) {
        return `${minPrice} $`;
      }
      return `${minPrice}-${maxPrice} $`;
    }
    return '';
  };

  renderName = (isShort = false) => {
    const {name, lastName, patronymic} = this.state;
    let result = [];

    if (lastName) {
      result = [...result, lastName];
    }
    if (name) {
      result = [...result, name];
    }
    if (!isShort && patronymic) {
      result = [...result, patronymic];
    }

    return result.join(' ');
  };

  render() {
    const {
      specializations,
      education,
      rating,
      photo,
      online,
      from,
      to,
      workExperience,
      category,
      description,
      isFavourite,
    } = this.state;

    return (
      <Wrap
        style={base.backgroundColor}
        titleView={<Title title={this.renderName(true)} />}>
        <View style={base.wrap1}>
          <View style={base.wrap2}>
            <AvatarDoctorFinder
              source={photo && {uri: `${URL}${photo}`}}
              online={online}
            />
            <View style={base.wrap3}>
              <Text style={base.text1}>{this.renderName()}</Text>
              <Text style={base.text2}>{specializations.join(', ')}</Text>
              {from && to && (
                <Text style={base.text3}>
                  Часы приема (МСК):{' '}
                  <Text style={base.text4}>
                    {`${moment(from).format('HH:mm')} - ${moment(to).format(
                      'HH:mm',
                    )}`}
                  </Text>
                </Text>
              )}
            </View>
          </View>
          <View style={base.wrap2}>
            <Text style={base.text3}>Рейтинг:</Text>
            <Rating rating={rating} />
            <View style={base.flex} />
            <Text style={base.text3}>Стоимость:</Text>
            <Text style={base.text5}>{this.renderPrice()}</Text>
          </View>
          <View style={base.wrap2}>
            <TouchableOpacity
              style={base.alignItems}
              onPress={this.onPressFavorite}>
              <View style={base.flex} />
              <Image
                source={Images.favorite}
                height={20}
                style={!isFavourite && base.tintColorImage}
              />
              <View style={base.flex} />
              {isFavourite ? (
                <Text style={base.text6}>В избранном</Text>
              ) : (
                <Text style={base.text10}>В избранное</Text>
              )}
            </TouchableOpacity>
            <View style={base.flex} />
            <TouchableOpacity
              style={base.alignItems}
              onPress={this.onPressServices}>
              <View style={base.flex} />
              <Image source={Images.reservation} height={24} />
              <View style={base.flex} />
              <Text style={base.text6}>Забронировать</Text>
            </TouchableOpacity>
            <View style={base.flex} />
            <TouchableOpacity
              style={base.alignItems}
              onPress={this.onPressMessage}>
              <View style={base.flex} />
              <Image source={Images.messages} height={24} />
              <View style={base.flex} />
              <Text style={base.text6}>Сообщение</Text>
            </TouchableOpacity>
            <View style={base.flex} />
            <TouchableOpacity
              style={base.alignItems}
              disabled={!online}
              onPress={this.onPressVideoCall}>
              <View style={base.flex} />
              <Image
                source={Images.video}
                width={24}
                style={online && base.tintColor}
              />
              <View style={base.flex} />
              <Text style={online ? base.text6 : base.text11}>
                Видео-звонок
              </Text>
            </TouchableOpacity>
          </View>

          <Text style={base.text7}>КВАЛИФИКАЦИЯ</Text>
          <View style={base.wrap4}>
            <InputTextEdit
              title="ВУЗ"
              autoCapitalize="sentences"
              returnKeyType="next"
              value={education}
              editable={false}
            />
            <InputSpecializations
              title="Специализация"
              editable={false}
              value={specializations}
            />
            <InputTextEdit
              title="Стаж работы"
              value={workExperience}
              editable={false}
              suffix=" лет"
            />
            <InputCategoryEdit
              title="Категория"
              editable={false}
              value={category}
              noBorder
            />
          </View>

          <Text style={base.text7}>О СЕБЕ</Text>
          <View style={base.wrap4}>
            <Text style={base.text8}>{description}</Text>
            <View style={base.line} />
            <TouchableOpacity onPress={this.onPressReview}>
              <View style={base.underline}>
                <Text style={base.text9}>Отзывы</Text>
              </View>
            </TouchableOpacity>
            <View style={base.line} />
            <TouchableOpacity onPress={this.onPressServices}>
              <View style={base.underline}>
                <Text style={base.text9}>Услуги</Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
        <Footer />
      </Wrap>
    );
  }
}
