import {connect} from 'react-redux';
import component from './component';

import {setToast} from '../../../../store/actions';
import {fetchGetFavorites} from '../../../../store/actions/favorites';

function mapStateToProps(state) {
  return {
    user: state.user,
    favorites: state.favorites,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    fetchGetFavorites: (data) => dispatch(fetchGetFavorites(data)),
    setToast: (data) => dispatch(setToast(data)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(component);
