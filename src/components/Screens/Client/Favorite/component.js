import React from 'react';
import {View, FlatList} from 'react-native';

// Components
import Wrap from '../../../UI/Base/Wrap';
import Title from '../../../UI/Header/Title';
import Footer from '../../../UI/Footer';
import ItemFavorite from '../../../UI/Items/ItemFavorite';

// Helpers
import * as functions from './functions';

// Style
import {base} from './styles';

export default class Favorite extends React.Component {
  constructor(props) {
    super(props);
    functions.constructorState(this, props);
  }

  componentDidMount() {
    const {user, fetchGetFavorites} = this.props;
    const path = {
      page: 1,
      pageSize: 100,
    };
    fetchGetFavorites({user, path});
  }

  renderItem = ({item, index}) => <ItemFavorite key={index} {...item} />;

  renderSeparator = () => {
    return <View style={base.separator} />;
  };

  render() {
    const {favorites} = this.props;
    return (
      <Wrap
        style={base.backgroundColor}
        titleView={<Title title="Избранное" />}>
        <View style={base.wrap1}>
          <FlatList
            data={favorites}
            extraData={this.props}
            renderItem={this.renderItem}
            onRefresh={this.onRefresh}
            refreshing={false}
            ItemSeparatorComponent={this.renderSeparator}
            keyExtractor={(item, index) => index.toString()}
          />
        </View>
        <Footer />
      </Wrap>
    );
  }
}
