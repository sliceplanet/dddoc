import {connect} from 'react-redux';
import component from './component';

import {
  reducerDepositMethods,
  reducerOutputMethods,
} from '../../../../store/actions/alert';

import {
  fetchGetTransactions,
  fetchGetBalance,
} from '../../../../store/actions/balance';

function mapStateToProps(state) {
  return {
    payments: state.balance,
    user: state.user,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    fetchGetTransactions: (data) => dispatch(fetchGetTransactions(data)),
    fetchGetBalance: (data) => dispatch(fetchGetBalance(data)),
    reducerDepositMethods: () => dispatch(reducerDepositMethods()),
    reducerOutputMethods: () => dispatch(reducerOutputMethods()),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(component);
