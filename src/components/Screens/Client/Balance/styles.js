import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
  },
  backgroundColor: {
    backgroundColor: '#EFEFF4',
  },
  row: {
    width: wp(100) - 32,
    flexDirection: 'row',
    alignItems: 'center',
  },
  btn: {
    minWidth: 116,
  },
  separator: {
    width: wp(100),
    height: 0.7,
    backgroundColor: 'rgba(0, 0, 0, 0.3)',
  },
  wrap1: {
    flex: 1,
  },
  wrap2: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 1,
  },
  text1: {
    fontFamily: 'SF Pro Text',
    fontSize: 13,
    lineHeight: 16,
    color: 'rgba(0, 0, 0, 0.54)',
  },
  text2: {
    fontFamily: 'SF Pro Text',
    fontSize: 24,
    fontWeight: '600',
    color: '#D962A5',
    marginLeft: 8,
  },
  text3: {
    fontFamily: 'SF Pro Text',
    fontSize: 13,
    lineHeight: 16,
    color: 'red',
  },
});

export default {base};
