import React from 'react';
import {View, Text, FlatList} from 'react-native';
import Image from 'react-native-scalable-image';

// Components
import Wrap from '../../../UI/Base/Wrap';
import Title from '../../../UI/Header/Title';
import Footer from '../../../UI/Footer';
import Card from '../../../UI/Card';
import ButtonFullSmall from '../../../UI/Button/ButtonFullSmall';
import ButtonEmptySmall from '../../../UI/Button/ButtonEmptySmall';
import ItemHistory from '../../../UI/Items/ItemHistory';

// Helpers
import * as Images from '../../../../helpers/images';
import {getStatus, hasStatus, hasErrorStatus} from '../../../../helpers';

// Style
import {base} from './styles';

export default class Balance extends React.Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  componentDidMount() {
    const {user, fetchGetTransactions, fetchGetBalance} = this.props;
    const path = {
      page: 1,
      pageSize: 100,
    };
    fetchGetTransactions({user, path});
    fetchGetBalance({user});
  }

  onPressReplenish = () => {
    const {reducerDepositMethods} = this.props;
    reducerDepositMethods();
  };

  onPressWithdraw = () => {
    const {reducerOutputMethods} = this.props;
    reducerOutputMethods();
  };

  renderItem = ({item, index}) => <ItemHistory key={index} {...item} />;

  renderSeparator = () => {
    return <View style={base.separator} />;
  };

  renderStatus = (status) => {
    if (hasStatus(status) || hasErrorStatus(status)) {
      if (hasErrorStatus(status)) {
        return <Text style={base.text3}>{getStatus(status)}</Text>;
      }
      return <Text style={base.text1}>{getStatus(status)}</Text>;
    }
  };

  render() {
    const {balance, status, transactions} = this.props.payments;
    return (
      <Wrap style={base.backgroundColor} titleView={<Title title="Мой счет" />}>
        <View style={base.wrap1}>
          <Card title="Баланс">
            <View style={base.row}>
              <Text style={base.text1}>На вашем счету:</Text>
              <View style={base.flex} />
              <ButtonFullSmall
                style={base.btn}
                disabled={hasStatus(status)}
                title="Пополнить"
                onPress={this.onPressReplenish}
              />
            </View>
            <View style={base.row}>
              <Image source={Images.cash} width={24} />
              <Text style={base.text2}>{balance} $</Text>
              <View style={base.flex} />
              <ButtonEmptySmall
                style={base.btn}
                disabled={hasStatus(status)}
                title="Вывести"
                onPress={this.onPressWithdraw}
              />
            </View>
            {this.renderStatus(status)}
          </Card>

          <Card title="История">
            <FlatList
              data={transactions}
              extraData={this.props}
              renderItem={this.renderItem}
              ItemSeparatorComponent={this.renderSeparator}
              keyExtractor={(item, index) => index.toString()}
              contentContainerStyle={base.wrap2}
            />
          </Card>
        </View>
        <Footer />
      </Wrap>
    );
  }
}
