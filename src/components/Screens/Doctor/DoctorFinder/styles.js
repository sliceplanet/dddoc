import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
  },
  backgroundColor: {
    backgroundColor: '#EFEFF4',
  },
  separator: {
    width: wp(100),
    height: 0.7,
    backgroundColor: 'rgba(0, 0, 0, 0.3)',
  },
  wrap: {
    marginTop: 12,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 1,
    backgroundColor: '#EFEFF4',
  },
  isVerify: {
    flexDirection: 'row',
    alignItems: 'center',
    width: wp(100),
    paddingHorizontal: 16,
    backgroundColor: 'white',
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderTopColor: '#D962A5',
    borderBottomColor: '#D962A5',
  },
  text1: {
    fontFamily: 'SF Pro Text',
    fontSize: 15,
    lineHeight: 15,
    color: 'rgba(60, 60, 67, 0.6)',
    flexShrink: 1,
    paddingLeft: 4,
  },
  text2: {
    fontFamily: 'SF Pro Text',
    fontSize: 50,
    color: '#D962A5',
  },
});

export default {base};
