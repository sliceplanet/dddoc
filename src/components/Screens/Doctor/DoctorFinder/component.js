import React from 'react';
import {View, Text, FlatList} from 'react-native';

// Components
import Wrap from '../../../UI/Base/Wrap';
import Title from '../../../UI/Header/Title';
import FinderDoctor from '../../../UI/Finder/FinderDoctor';
import ItemDoctorFinder from '../../../UI/Items/ItemDoctorFinder';
import Footer from '../../../UI/Footer';

// Helpers
import * as functions from './functions';

// Style
import {base} from './styles';

export default class DoctorFinder extends React.Component {
  constructor(props) {
    super(props);

    functions.constructorState(this, props);
  }

  componentDidMount() {
    this.onFind({});
  }

  onFind = (data) => {
    const {user, fetchPostDoctorFind} = this.props;

    const path = {
      page: 1,
      pageSize: 100,
    };
    fetchPostDoctorFind({user, path, data});
  };

  onRefresh = () => {
    this.onFind({});
  };

  renderItem = ({item, index}) => <ItemDoctorFinder key={index} {...item} />;

  renderSeparator = () => {
    return <View style={base.separator} />;
  };

  render() {
    const {items} = this.props.doctorFind;
    const isVerify = this.props.profile.isVerify;

    return (
      <Wrap
        noScroll
        style={base.backgroundColor}
        titleView={<Title title="Поиск доктора" alert menu />}>
        <View style={base.flex}>
          <FinderDoctor onFind={this.onFind} />
          {isVerify === false && (
            <View style={base.isVerify}>
              <Text style={base.text2}>!</Text>
              <Text style={base.text1}>
                Для размещения вами услуг в качестве доктора необходимо в меню
                заполнить профиль, затем в настройках пройти верификацию
              </Text>
            </View>
          )}
          <FlatList
            data={items}
            contentContainerStyle={base.wrap}
            extraData={this.state}
            renderItem={this.renderItem}
            onRefresh={this.onRefresh}
            refreshing={false}
            ItemSeparatorComponent={this.renderSeparator}
            keyExtractor={(item, index) => index.toString()}
          />
        </View>
        <Footer />
      </Wrap>
    );
  }
}
