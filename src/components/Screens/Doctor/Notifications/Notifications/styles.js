import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
  },
  backgroundColor: {
    backgroundColor: '#EFEFF4',
  },
  wrap1: {
    flex: 1,
  },
  separator: {
    width: wp(100),
    height: 12,
  },
});

export default {base};
