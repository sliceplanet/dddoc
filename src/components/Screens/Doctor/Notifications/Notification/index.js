import {connect} from 'react-redux';
import component from './component';

import {setToast} from '../../../../../store/actions';
import {fetchDeleteNotifications} from '../../../../../store/actions/notifications';

function mapStateToProps(state) {
  return {
    user: state.user,
    notifications: state.notifications,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setToast: (data) => dispatch(setToast(data)),
    fetchDeleteNotifications: (data) =>
      dispatch(fetchDeleteNotifications(data)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(component);
