import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
  },
  backgroundColor: {
    backgroundColor: '#EFEFF4',
  },
  cross: {
    alignSelf: 'flex-start',
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  marginTop: {
    marginTop: 16,
  },
  wrap1: {
    flex: 1,
  },
  wrap2: {
    width: wp(100),
    paddingHorizontal: 16,
    paddingTop: 16,
    paddingBottom: 24,
    backgroundColor: 'white',
    marginTop: 12,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 1,
  },
  wrap3: {
    flexShrink: 1,
    paddingLeft: 20,
  },
  wrap4: {
    flex: 1,
  },
  text1: {
    fontFamily: 'SF Pro Text',
    fontSize: 15,
    lineHeight: 18,
    fontWeight: '600',
    color: 'black',
  },
  text2: {
    fontFamily: 'SF Pro Text',
    fontSize: 15,
    lineHeight: 18,
    color: '#D962A5',
  },
  text3: {
    fontFamily: 'SF Pro Text',
    fontSize: 15,
    lineHeight: 18,
    color: 'black',
    marginVertical: 3,
  },
  text4: {
    fontFamily: 'SF Pro Text',
    fontSize: 15,
    lineHeight: 18,
    fontWeight: '600',
    color: '#D962A5',
    marginVertical: 3,
  },
});

export default {base};
