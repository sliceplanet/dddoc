import React from 'react';
import {View, FlatList} from 'react-native';

// Components
import Wrap from '../../../../UI/Base/Wrap';
import Title from '../../../../UI/Header/Title';
import Footer from '../../../../UI/Footer';
import ItemVideoCallDoctor from '../../../../UI/Items/ItemVideoCallDoctor';
import ItemVideoCallClient from '../../../../UI/Items/ItemVideoCallClient';

import Tabs from './Tabs';

// Helpers
import * as functions from './functions';

// Style
import {base} from './styles';

export default class VideoCalls extends React.Component {
  constructor(props) {
    super(props);
    functions.constructorState(this, props);
  }

  componentDidMount() {
    const {fetchGetVideos, user} = this.props;
    const path = {
      page: 1,
      pageSize: 100,
    };
    fetchGetVideos({user, path});
  }

  onChangeTab = (tabIndex) => this.setState({tabIndex});

  renderItem = ({item, index}) => {
    if (item.clientId === this.props.user.user.id) {
      return <ItemVideoCallClient key={index} {...item} />;
    }
    return <ItemVideoCallDoctor key={index} {...item} />;
  };

  render() {
    const {tabIndex} = this.state;
    let {items} = this.props.videos;
    if (tabIndex === 2) {
      items = items.filter((e) => !e.status);
    } else if (tabIndex === 3) {
      items = items.filter((e) => e.status === 1);
    }

    return (
      <Wrap
        noScroll
        style={base.backgroundColor}
        titleView={<Title title="Видеозвонки" />}>
        <View style={base.wrap1}>
          <Tabs
            value={tabIndex}
            onChange={this.onChangeTab}
            all={this.props.videos.length}
            awaiting={this.props.videos.items.filter((e) => !e.status).length}
            scheduled={
              this.props.videos.items.filter((e) => e.status === 1).length
            }
          />
          <FlatList
            data={items}
            extraData={this.props}
            renderItem={this.renderItem}
            onRefresh={this.onRefresh}
            refreshing={false}
            keyExtractor={(item, index) => index.toString()}
          />
        </View>
        <Footer />
      </Wrap>
    );
  }
}
