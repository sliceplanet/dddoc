import React from 'react';
import {View, BackHandler, Alert} from 'react-native';

// Components
import Wrap from '../../../../UI/Base/Wrap';
import Title from '../../../../UI/Header/Title';
import Footer from '../../../../UI/Footer';
import Video from '../../../../UI/Video';

import {replace} from '../../../../../helpers/navigation';

import {
  apiPutVideosIdDone,
  apiPutVideosIdDecline,
} from '../../../../../store/api/videos';

// Style
import {base} from './styles';

export default class Call extends React.Component {
  constructor(props) {
    super(props);

    const channelName = props.route.params?.channelName.toString() ?? null;

    this.state = {
      appID: '1d32e36f526a4481ae4f3a57123424b8',
      channelName,
    };
  }

  componentDidMount() {
    this.backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      this.onPressBack,
    );

    if (!global.whoosh.isPlaying()) {
      global.whoosh.play((success) => {
        if (success) {
          console.log('successfully finished playing');
        } else {
          console.log('playback failed due to audio decoding errors');
        }
      });
    }
  }

  componentWillUnmount() {
    this.backHandler.remove();
    global.whoosh.pause();
  }

  onPressYes = () => {
    const {user} = this.props;
    const {channelName} = this.state;
    const path = {
      id: channelName,
    };
    if (global.whoosh.isPlaying()) {
      apiPutVideosIdDecline({user, path})
        .then((result) => {
          console.log(result);
          replace('DoctorFinder');
        })
        .catch((e) => console.log(e.response));
    } else {
      apiPutVideosIdDone({user, path})
        .then((result) => {
          console.log(result);
          replace('DoctorFinder');
        })
        .catch((e) => console.log(e.response));
    }
  };

  onPressBack = () => {
    Alert.alert(
      'Закрыть видео звонок',
      'Хотите закрыть видео звонок?',
      [{text: 'Да', onPress: this.onPressYes}, {text: 'Нет'}],
      {cancelable: false},
    );

    return true;
  };

  onUserJoined = () => {
    global.whoosh.pause();
  };

  render() {
    const {channelName} = this.state;

    return (
      <Wrap
        noScroll
        style={base.backgroundColor}
        titleView={
          <Title title="Звонок" back onPressBack={this.onPressBack} />
        }>
        <View style={base.wrap1}>
          {channelName && (
            <Video
              {...this.state}
              onChangePeer={this.onPressYes}
              onUserJoined={this.onUserJoined}
            />
          )}
        </View>
        <Footer />
      </Wrap>
    );
  }
}
