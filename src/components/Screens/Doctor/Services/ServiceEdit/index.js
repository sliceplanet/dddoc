import {connect} from 'react-redux';
import component from './component';

import {setToast} from '../../../../../store/actions';
import {fetchPostServicesId} from '../../../../../store/actions/services';

function mapStateToProps(state) {
  return {
    user: state.user,
    profile: state.profile,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setToast: (data) => dispatch(setToast(data)),
    fetchPostServicesId: (data) => dispatch(fetchPostServicesId(data)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(component);
