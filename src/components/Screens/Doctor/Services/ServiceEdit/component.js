import React from 'react';
import {View, Keyboard} from 'react-native';

// Components
import Wrap from '../../../../UI/Base/Wrap';
import Title from '../../../../UI/Header/Title';
import Card from '../../../../UI/Card';
import InputSpecializations from '../../../../UI/Input/InputSpecializations';
import InputDuration from '../../../../UI/Input/InputDuration';
import InputTextEdit from '../../../../UI/Input/InputTextEdit';
import Footer from '../../../../UI/Footer';

// Helpers
import * as functions from './functions';

// Style
import {base} from './styles';

export default class ServiceEdit extends React.Component {
  constructor(props) {
    super(props);
    functions.constructorState(this, props);
  }

  onChangeSpecializations = (specializations) =>
    this.setState({specializations});
  onChangeDuration = (duration) => this.setState({duration});
  onChangeCost = (price) => this.setState({price});
  onChangeDescription = (description) => this.setState({description});

  done = () => {
    Keyboard.dismiss();
    const {price, description, duration, specializations, id} = this.state;
    const {setToast, fetchPostServicesId, user} = this.props;

    if (!specializations) {
      setToast('Выберите специализацию');
      return;
    }
    if (!duration) {
      setToast('Выберите длительность');
      return;
    }
    if (!price) {
      setToast('Введите стоимость');
      return;
    }
    if (description.length === 0) {
      setToast('Введите описание');
      return;
    }
    const data = {
      price,
      description,
      duration: duration * 60,
      specializationId: specializations.id,
    };
    const path = {
      id,
    };
    fetchPostServicesId({user, data, path});
  };

  render() {
    const {specializations, duration, price, description} = this.state;

    return (
      <Wrap
        style={base.backgroundColor}
        titleView={
          <Title
            title="Изменить услугу"
            button="Сохранить"
            onPressButton={this.done}
          />
        }>
        <View style={base.wrap1}>
          <Card title="Информация об услуге">
            <InputSpecializations
              title="Специализация"
              value={specializations}
              editable
              one
              data={this.props.profile.specializations}
              onChange={this.onChangeSpecializations}
            />
            <InputDuration
              title="Длительность (мин.)"
              value={duration}
              editable
              onChange={this.onChangeDuration}
            />
            <InputTextEdit
              title="Стоимость ($)"
              keyboardType="numeric"
              value={`${price}`}
              editable
              onChangeText={this.onChangeCost}
            />
          </Card>
          <Card title="Описание">
            <InputTextEdit
              autoCapitalize="sentences"
              value={description}
              multiline
              editable
              onChangeText={this.onChangeDescription}
            />
          </Card>
        </View>
        <Footer />
      </Wrap>
    );
  }
}
