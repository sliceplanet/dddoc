export function constructorState(_this, props) {
  const _props = props.route.params?.props ?? [];
  _this.state = {
    ..._props,
    specializations: _props.specializations.filter(
      (e) => e.id === _props.specializationId,
    )[0],
    duration: _props.duration / 60,
  };
}
