import {connect} from 'react-redux';
import component from './component';

import {setToast} from '../../../../../store/actions';

function mapStateToProps(state) {
  return {
    user: state.user,
    services: state.services,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setToast: (data) => dispatch(setToast(data)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(component);
