import React from 'react';
import {View, FlatList} from 'react-native';

// Components
import Wrap from '../../../../UI/Base/Wrap';
import Title from '../../../../UI/Header/Title';
import ButtonFull from '../../../../UI/Button/ButtonFull';
import Footer from '../../../../UI/Footer';
import ItemService from '../../../../UI/Items/ItemService';

// Helpers
import {navigate} from '../../../../../helpers/navigation';
import * as functions from './functions';

// Style
import {base} from './styles';

export default class Services extends React.Component {
  constructor(props) {
    super(props);
    functions.constructorState(this);
  }

  onPress = () => navigate('ServiceAdd');

  renderItem = ({item, index}) => <ItemService key={index} {...item} />;

  renderSeparator = () => {
    return <View style={base.separator} />;
  };

  render() {
    const {services} = this.props;

    return (
      <Wrap
        style={base.backgroundColor}
        titleView={<Title title="Мои услуги" />}>
        <View style={base.wrap1}>
          <FlatList
            data={services}
            contentContainerStyle={base.wrap2}
            extraData={this.props}
            renderItem={this.renderItem}
            onRefresh={this.onRefresh}
            refreshing={false}
            ItemSeparatorComponent={this.renderSeparator}
            keyExtractor={(item, index) => index.toString()}
          />

          <ButtonFull title="Добавить услугу" onPress={this.onPress} />
        </View>
        <Footer />
      </Wrap>
    );
  }
}
