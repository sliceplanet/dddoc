export function constructorState(_this, props) {
  _this.state = {
    id: props.route.params?.id ?? '',
    type: props.route.params?.type ?? '',
    checkedId: -1,
  };
}
