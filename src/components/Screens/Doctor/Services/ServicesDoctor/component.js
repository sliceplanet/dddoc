import React from 'react';
import {View, FlatList, Keyboard} from 'react-native';
import moment from 'moment';

// Components
import Wrap from '../../../../UI/Base/Wrap';
import Title from '../../../../UI/Header/Title';
import Footer from '../../../../UI/Footer';
import ItemServiceSelect from '../../../../UI/Items/ItemServiceSelect';
import ButtonFull from '../../../../UI/Button/ButtonFull';

// Helpers
import * as functions from './functions';
import {navigate} from '../../../../../helpers/navigation';
import {apiGetServicesId} from '../../../../../store/api/services';

// Style
import {base} from './styles';

export default class ServicesDoctor extends React.Component {
  constructor(props) {
    super(props);
    functions.constructorState(this, props);
  }

  componentDidMount() {
    const {id} = this.state;
    const {user} = this.props;
    const path = {id};
    apiGetServicesId({path, user})
      .then((result) => this.setState({services: result.data}))
      .catch((e) => console.log(e));
  }

  renderItem = ({item, index}) => {
    const {checkedId} = this.state;
    return (
      <ItemServiceSelect
        key={index}
        checked={checkedId === index}
        onPress={() => this.setState({checkedId: index})}
        {...item}
      />
    );
  };

  renderSeparator = () => {
    return <View style={base.separator} />;
  };

  done = () => {
    Keyboard.dismiss();
    const {checkedId, id, type, services} = this.state;
    const {user, setToast, fetchPostVideosNew} = this.props;

    if (checkedId === -1) {
      setToast('Выберите услугу');
      return;
    }

    switch (type) {
      case 'fetchPostVideosNew': {
        const data = {
          date: moment().format(),
          userId: id,
          serviceId: services[checkedId].id,
          cito: true,
        };
        const goBack = 1;

        fetchPostVideosNew({user, data, goBack});
        break;
      }
      case 'DateReservation': {
        navigate('DateReservation', {
          serviceId: services[checkedId].id,
          userId: id,
        });
        break;
      }
    }
  };

  render() {
    const {services} = this.state;

    return (
      <Wrap
        style={base.backgroundColor}
        titleView={<Title title="Выбор услуги" />}>
        <View style={base.wrap1}>
          <FlatList
            data={services}
            extraData={this.state}
            renderItem={this.renderItem}
            onRefresh={this.onRefresh}
            refreshing={false}
            ItemSeparatorComponent={this.renderSeparator}
            keyExtractor={(item, index) => index.toString()}
          />
          <ButtonFull title="Выбрать услугу" onPress={this.done} />
        </View>
        <Footer />
      </Wrap>
    );
  }
}
