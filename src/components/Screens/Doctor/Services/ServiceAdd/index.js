import {connect} from 'react-redux';
import component from './component';

import {setToast} from '../../../../../store/actions';
import {fetchPostServicesNew} from '../../../../../store/actions/services';

function mapStateToProps(state) {
  return {
    user: state.user,
    profile: state.profile,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setToast: (data) => dispatch(setToast(data)),
    fetchPostServicesNew: (data) => dispatch(fetchPostServicesNew(data)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(component);
