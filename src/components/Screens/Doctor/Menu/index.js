import {connect} from 'react-redux';
import component from './component';

import {logoutUser} from '../../../../store/actions/auth';

function mapStateToProps(state) {
  return {
    user: state.user,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    logoutUser: () => dispatch(logoutUser()),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(component);
