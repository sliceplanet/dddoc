import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import Image from 'react-native-scalable-image';

// Components
import Wrap from '../../../UI/Base/Wrap';
import Title from '../../../UI/Header/Title';
import Footer from '../../../UI/Footer';

// Helpers
import * as functions from './functions';
import * as Images from '../../../../helpers/images';
import {navigate} from '../../../../helpers/navigation';

// Style
import {base} from './styles';

export default class Profile extends React.Component {
  constructor(props) {
    super(props);

    functions.constructorState(this, props);
  }

  onPressProfile = () => navigate('Profile');
  onPressMessages = () => navigate('Messages');
  onPressDoctorFinder = () => navigate('DoctorFinder');

  render() {
    const {logoutUser} = this.props;
    return (
      <Wrap style={base.backgroundColor}>
        <Title title="Меню" />
        <View style={base.flex}>
          <View style={base.wrap}>
            <TouchableOpacity style={base.row} onPress={this.onPressProfile}>
              <Image source={Images.menuProfile} width={29} />
              <Text style={base.text}>Мой профиль</Text>
              <Image source={Images.menuRight} width={8} />
            </TouchableOpacity>
            <View style={base.line} />
            <TouchableOpacity style={base.row}>
              <Image source={Images.menuVideo} width={29} />
              <Text style={base.text}>Видео-звонок доктору</Text>
              <Image source={Images.menuRight} width={8} />
            </TouchableOpacity>
            <View style={base.line} />
            <TouchableOpacity style={base.row} onPress={this.onPressMessages}>
              <Image source={Images.menuMessages} width={29} />
              <Text style={base.text}>Сообщения</Text>
              <Image source={Images.menuRight} width={8} />
            </TouchableOpacity>
            <View style={base.line} />
            <TouchableOpacity style={base.row}>
              <Image source={Images.menuFavorite} width={29} />
              <Text style={base.text}>Избранное</Text>
              <Image source={Images.menuRight} width={8} />
            </TouchableOpacity>
            <View style={base.line} />
            <TouchableOpacity style={base.row}>
              <Image source={Images.menuPay} width={29} />
              <Text style={base.text}>Мой счет</Text>
              <Image source={Images.menuRight} width={8} />
            </TouchableOpacity>
            <View style={base.line} />
            <TouchableOpacity style={base.row}>
              <Image source={Images.menuReviews} width={29} />
              <Text style={base.text}>Отзывы</Text>
              <Image source={Images.menuRight} width={8} />
            </TouchableOpacity>
            <View style={base.line} />
            <TouchableOpacity style={base.row}>
              <Image source={Images.menuServices} width={29} />
              <Text style={base.text}>Мои услуги</Text>
              <Image source={Images.menuRight} width={8} />
            </TouchableOpacity>
            <View style={base.line} />
            <TouchableOpacity style={base.row}>
              <Image source={Images.menuSettings} width={29} />
              <Text style={base.text}>Настройки</Text>
              <Image source={Images.menuRight} width={8} />
            </TouchableOpacity>
            <View style={base.line} />
            <TouchableOpacity
              style={base.row}
              onPress={this.onPressDoctorFinder}>
              <Image source={Images.menuFind} width={29} />
              <Text style={base.text}>Поиск доктора</Text>
              <Image source={Images.menuRight} width={8} />
            </TouchableOpacity>
            <View style={base.line} />
            <TouchableOpacity style={base.row} onPress={logoutUser}>
              <Image source={Images.menuLogout} width={29} />
              <Text style={base.text}>Выход</Text>
              <Image source={Images.menuRight} width={8} />
            </TouchableOpacity>
          </View>
        </View>
        <Footer />
      </Wrap>
    );
  }
}
