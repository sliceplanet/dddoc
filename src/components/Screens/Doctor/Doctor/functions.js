export function constructorState(_this, props) {
  _this.state = {
    ...(props.route.params?.props ?? []),
  };
}
