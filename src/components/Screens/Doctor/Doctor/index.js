import {connect} from 'react-redux';
import component from './component';

import {setToast} from '../../../../store/actions';
import {
  fetchPutFavoritesId,
  fetchDeleteFavoritesId,
} from '../../../../store/actions/favorites';

function mapStateToProps(state) {
  return {
    user: state.user,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setToast: (data) => dispatch(setToast(data)),
    fetchPutFavoritesId: (data) => dispatch(fetchPutFavoritesId(data)),
    fetchDeleteFavoritesId: (data) => dispatch(fetchDeleteFavoritesId(data)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(component);
