import React from 'react';
import {View, Text, Keyboard} from 'react-native';

// Components
import Wrap from '../../../UI/Base/Wrap';
import Title from '../../../UI/Header/Title';
import Footer from '../../../UI/Footer';
import Card from '../../../UI/Card';
import InputSpecializations from '../../../UI/Input/InputSpecializations';
import InputTextEdit from '../../../UI/Input/InputTextEdit';
import InputCategoryEdit from '../../../UI/Input/InputCategoryEdit';
import InputDocument from '../../../UI/Input/InputDocument';
import ButtonFull from '../../../UI/Button/ButtonFull';

// Style
import {base} from './styles';

export default class Verification extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      specializations: [],
      workExperience: '',
      category: '',
      passport: [],
      diploma: [],
      documentSpecialization: [],
      documentCategory: [],
    };
  }

  onChangeSpecializations = (specializations) =>
    this.setState({specializations});
  onChangeWorkExperience = (workExperience) => this.setState({workExperience});
  onChangeCategory = (category) => this.setState({category});
  onChangePassport = (passport) => this.setState({passport});
  onChangeDiploma = (diploma) => this.setState({diploma});
  onChangeDocumentSpecialization = (documentSpecialization) =>
    this.setState({documentSpecialization});
  onChangeDocumentCategory = (documentCategory) =>
    this.setState({documentCategory});

  isDisabled = () => {
    const {
      specializations,
      workExperience,
      category,
      passport,
      diploma,
      documentSpecialization,
      documentCategory,
    } = this.state;

    if (specializations.length === 0) {
      return true;
    }
    if (workExperience.length === 0) {
      return true;
    }
    if (category.length === 0) {
      return true;
    }
    if (passport.length === 0) {
      return true;
    }
    if (diploma.length === 0) {
      return true;
    }
    if (documentSpecialization.length === 0) {
      return true;
    }
    if (documentCategory.length === 0) {
      return true;
    }
    return false;
  };

  done = () => {
    Keyboard.dismiss();
    const {user, fetchPostDoctorVerify} = this.props;
    const {
      specializations,
      workExperience,
      category,
      passport,
      diploma,
      documentSpecialization,
      documentCategory,
    } = this.state;

    const data = {
      specializations,
      workExperience,
      category,
    };

    const files = {
      passport,
      diploma,
      documentSpecialization,
      documentCategory,
    };

    fetchPostDoctorVerify({data, files, user});
  };

  render() {
    const {
      specializations,
      workExperience,
      category,
      passport,
      diploma,
      documentSpecialization,
      documentCategory,
    } = this.state;
    return (
      <Wrap
        style={base.backgroundColor}
        titleView={<Title title="Верификация" />}>
        <View style={base.wrap1}>
          <Card title="Квалификация">
            <InputSpecializations
              title="Специализация"
              value={specializations}
              editable
              data={this.props.profile.specializations}
              onChange={this.onChangeSpecializations}
            />
            <InputTextEdit
              ref={this.refWorkExperience}
              title="Стаж (лет)"
              keyboardType="numeric"
              returnKeyType="next"
              editable
              value={workExperience}
              onChangeText={this.onChangeWorkExperience}
              onSubmitEditing={this.nextCategory}
            />
            <InputCategoryEdit
              ref={this.refCategory}
              title="Категория"
              value={category}
              editable
              onChange={this.onChangeCategory}
            />
          </Card>
          <Text style={base.text1}>ДОКУМЕНТЫ</Text>
          <InputDocument
            title="Паспорт"
            subTitle="Страница с фото"
            value={passport}
            onChange={this.onChangePassport}
          />
          <InputDocument
            title="Диплом врача"
            value={diploma}
            onChange={this.onChangeDiploma}
          />
          <InputDocument
            title="Документ подтверждающий специализацию"
            subTitleGrey="(сертификат, интернатура)"
            value={documentSpecialization}
            onChange={this.onChangeDocumentSpecialization}
          />
          <InputDocument
            title="Документ подтверждающий категорию или звание"
            value={documentCategory}
            onChange={this.onChangeDocumentCategory}
          />
          <View style={base.flex} />
          <ButtonFull
            title="Подать заявку"
            disabled={this.isDisabled()}
            onPress={this.done}
          />
        </View>
        <Footer />
      </Wrap>
    );
  }
}
