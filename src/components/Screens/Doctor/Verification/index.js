import {connect} from 'react-redux';
import component from './component';

import {setToast} from '../../../../store/actions';
import {fetchPostDoctorVerify} from '../../../../store/actions/doctor';

function mapStateToProps(state) {
  return {
    user: state.user,
    profile: state.profile,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setToast: (data) => dispatch(setToast(data)),
    fetchPostDoctorVerify: (data) => dispatch(fetchPostDoctorVerify(data)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(component);
