import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
  },
  backgroundColor: {
    backgroundColor: '#EFEFF4',
  },
  wrap1: {
    flex: 1,
    alignItems: 'center',
  },
  text1: {
    width: wp(100),
    fontFamily: 'SF Pro Text',
    fontSize: 13,
    lineHeight: 18,
    color: 'rgba(60, 60, 67, 0.6)',
    paddingHorizontal: 16,
    paddingVertical: 6,
  },
});

export default {base};
