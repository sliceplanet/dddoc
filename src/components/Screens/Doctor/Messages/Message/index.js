import {connect} from 'react-redux';
import component from './component';

import {setToast} from '../../../../../store/actions';
import {
  fetchPostChatsId,
  reducerAddMessage,
  reducerReadedMessage,
} from '../../../../../store/actions/chats';

function mapStateToProps(state) {
  return {
    user: state.user,
    profile: state.profile,
    chats: state.chats,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setToast: (data) => dispatch(setToast(data)),
    fetchPostChatsId: (data) => dispatch(fetchPostChatsId(data)),
    reducerAddMessage: (messages, chatId, user) =>
      dispatch(reducerAddMessage(messages, chatId, user)),
    reducerReadedMessage: (messageId, chatId) =>
      dispatch(reducerReadedMessage(messageId, chatId)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(component);
