import moment from 'moment';

export function constructorState(_this, props) {
  _this.state = {
    date: props.route.params?.day ?? '',
    day: moment(props.route.params?.day ?? '')
      .format('D MMMM YYYY')
      .toUpperCase(),
    serviceId: props.route.params?.serviceId ?? -1,
    userId: props.route.params?.userId ?? 0,
    index: -1,
  };
}
