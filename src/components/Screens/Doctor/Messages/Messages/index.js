import {connect} from 'react-redux';
import component from './component';

import {setToast} from '../../../../../store/actions';
import {fetchGetChats} from '../../../../../store/actions/chats';

function mapStateToProps(state) {
  return {
    user: state.user,
    profile: state.profile,
    chats: state.chats,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setToast: (data) => dispatch(setToast(data)),
    fetchGetChats: (data) => dispatch(fetchGetChats(data)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(component);
