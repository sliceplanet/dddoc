import React from 'react';
import {View, ScrollView, Text, TouchableOpacity, Alert} from 'react-native';

// Components
import Wrap from '../../../UI/Base/Wrap';
import Title from '../../../UI/Header/Title';
import Footer from '../../../UI/Footer';
import CheckBox from '../../../UI/CheckBox';
import ButtonFull from '../../../UI/Button/ButtonFull';

// Helpers
import * as functions from './functions';
import {goBack, replace} from '../../../../helpers/navigation';

// Style
import {base} from './styles';

export default class Specializations extends React.Component {
  constructor(props) {
    super(props);

    functions.constructorState(this, props);
  }

  componentDidMount() {
    const {value} = this.state;
    if (value.length === 0) {
      setTimeout(() => {
        Alert.alert(
          'Специализации',
          'Ваш список специализаций пуст, нужно их добавить в профиле',
          [{text: 'Ок', onPress: () => replace('Profile')}, {text: 'Отмена'}],
          {cancelable: false},
        );
      }, 1000);
    }
  }

  onChangeOne = (e) => {
    this.setState({
      id: e.id,
    });
    this.state.onChange(e);
  };

  onChange = (e) => {
    let {value} = this.state;
    let index = 0;
    for (let i = 0; i < value.length; i++) {
      if (value[i].id === e.id) {
        index = i;
        break;
      }
    }
    value[index].checked = !value[index].checked;
    this.setState({value: [...value]});
    const result = value.filter((n) => n.checked).map((n) => n.id);
    this.state.onChange(result);
  };

  render() {
    const {value, one, id} = this.state;
    if (one) {
      return (
        <Wrap
          style={base.backgroundColor}
          titleView={<Title title="Специализация" />}>
          <View style={base.flex}>
            <View style={base.flex}>
              <ScrollView>
                <View style={base.wrap}>
                  {value.map((e, i) => {
                    return (
                      <TouchableOpacity
                        key={i}
                        style={base.row}
                        onPress={() => this.onChangeOne(e)}>
                        <CheckBox
                          value={e.id === id}
                          onChange={() => this.onChangeOne(e)}
                        />
                        <Text style={base.text}>{e.name}</Text>
                      </TouchableOpacity>
                    );
                  })}
                </View>
              </ScrollView>
            </View>
            <ButtonFull style={base.btn} title="Выбрать" onPress={goBack} />
          </View>
          <Footer />
        </Wrap>
      );
    }

    return (
      <Wrap
        noScroll
        style={base.backgroundColor}
        titleView={<Title title="Специализация" />}>
        <View style={base.flex}>
          <View style={base.flex}>
            <ScrollView>
              <View style={base.wrap}>
                {value.map((e, i) => {
                  return (
                    <TouchableOpacity
                      key={i}
                      style={base.row}
                      onPress={() => this.onChange(e)}>
                      <CheckBox
                        value={e.checked}
                        onChange={() => this.onChange(e)}
                      />
                      <Text style={base.text}>{e.name}</Text>
                    </TouchableOpacity>
                  );
                })}
              </View>
            </ScrollView>
          </View>
          <ButtonFull style={base.btn} title="Выбрать" onPress={goBack} />
        </View>
        <Footer />
      </Wrap>
    );
  }
}
