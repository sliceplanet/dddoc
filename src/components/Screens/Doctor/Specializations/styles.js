import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const base = StyleSheet.create({
  flex: {
    flex: 1,
  },
  backgroundColor: {
    backgroundColor: '#EFEFF4',
  },
  wrap: {
    backgroundColor: 'white',
    paddingTop: 10,
  },
  btn: {
    alignSelf: 'center',
  },
  row: {
    width: wp(100),
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'white',
    paddingTop: 4,
    paddingBottom: 8,
    paddingHorizontal: 16,
  },
  text: {
    fontFamily: 'SF Pro Text',
    fontSize: 16,
    lineHeight: 22,
    color: 'black',
    marginLeft: 8,
  },
});

export default {base};
