export function constructorState(_this, props) {
  const {specializations} = props;
  const value = props.route.params?.value ?? null;
  const one = props.route.params?.one ?? false;
  const data = props.route.params?.data ?? null;
  const onChange = props.route.params?.onChange ?? null;

  _this.state = {
    value: data
      ? data.map((e) => {
          const s = specializations.filter((n) => n.id === e);
          if (s.length > 0) {
            return {
              ...s[0],
              checked: one
                ? s[0].id === value
                : value.filter((n) => n === s[0].id).length > 0,
            };
          }
        })
      : specializations.map((e) => {
          return {
            ...e,
            checked: one
              ? e.id === value
              : value.filter((n) => n === e.id).length > 0,
          };
        }),
    one,
    onChange,
    id: -1,
  };
}
