import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import {Switch} from 'react-native-switch';
import Image from 'react-native-scalable-image';

// Components
import Wrap from '../../../UI/Base/Wrap';
import Title from '../../../UI/Header/Title';
import Footer from '../../../UI/Footer';
import Card from '../../../UI/Card';
import ModalTimezone from '../../../UI/Modal/ModalTimezone';

// Helpers
import * as Images from '../../../../helpers/images';
import {navigate} from '../../../../helpers/navigation';

// Constant
import {timezones} from '../../../../helpers/constant/timezones';

// Style
import {base} from './styles';

export default class Settings extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isVisible: false,
    };
  }

  onValueChange = () => {
    const {push} = this.props.settings;
    this.props.reducerPush(!push);
  };

  onPressTimezone = () => {
    this.setState({
      isVisible: true,
    });
  };
  onSelect = (timezone) => {
    this.props.reducerTimezone(timezone);
    this.onClose();
  };
  onClose = () => this.setState({isVisible: false});

  onPressVerification = () => navigate('Verification');

  render() {
    const {push, timezone} = this.props.settings;
    const {isVisible} = this.state;

    return (
      <Wrap
        style={base.backgroundColor}
        titleView={<Title title="Настройки" />}>
        <View style={base.wrap1}>
          <Card title="PUSH уведомления">
            <TouchableOpacity style={base.row} onPress={this.onValueChange}>
              <Text style={base.text1}>{push ? 'Включены' : 'Выключены'}</Text>
              <Switch
                value={push}
                onValueChange={this.onValueChange}
                backgroundActive="#D962A5"
                circleActiveBorderColor="#D962A5"
                backgroundInactive="#ACACAC"
                circleInactiveBorderColor="#ACACAC"
                circleBorderWidth={2}
              />
            </TouchableOpacity>
          </Card>
          <Card title="Часовой пояс">
            <TouchableOpacity style={base.row} onPress={this.onPressTimezone}>
              <Text style={base.text1} numberOfLines={1}>
                {timezones.filter((e) => e.timezone === timezone)[0].countries}
              </Text>
              <Image source={Images.right} height={12} style={base.tintColor} />
            </TouchableOpacity>
          </Card>
          <Card title="Верификация">
            <TouchableOpacity
              style={base.row}
              onPress={this.onPressVerification}>
              <Text style={base.text2}>Пройти верификацию</Text>
              <Image source={Images.right} height={12} style={base.tintColor} />
            </TouchableOpacity>
          </Card>
        </View>
        <Footer />
        <ModalTimezone
          isVisible={isVisible}
          onSelect={this.onSelect}
          onPressClose={this.onClose}
        />
      </Wrap>
    );
  }
}
