import React from 'react';
import {View, Keyboard} from 'react-native';

// Components
import Wrap from '../../../UI/Base/Wrap';
import Title from '../../../UI/Header/Title';
import Footer from '../../../UI/Footer';
import Card from '../../../UI/Card';
import InputTextEdit from '../../../UI/Input/InputTextEdit';
import ButtonFull from '../../../UI/Button/ButtonFull';

// Helpers
import * as functions from './functions';
import * as validator from '../../../../helpers/validator';

// Style
import {base} from './styles';

export default class Support extends React.Component {
  constructor(props) {
    super(props);
    functions.constructorState(this, props);
  }

  onChangeTheme = (theme) => this.setState({theme});
  onChangeEmail = (email) => this.setState({email});
  onChangeText = (text) => this.setState({text});

  refEmail = (ref) => (this.email = ref);
  refText = (ref) => (this.text = ref);

  nextEmail = () => this.email.focus();
  nextText = () => this.text.focus();

  done = () => {
    Keyboard.dismiss();
    const {theme, email, text} = this.state;
    const {user, setToast, fetchPostSupport} = this.props;

    if (theme.length === 0) {
      setToast('Введите тему сообщения.');
      return;
    }

    if (email.length === 0) {
      setToast('Введите свой e-mail.');
      return;
    }
    if (!validator.email(email)) {
      setToast('E-mail  введен не верно.');
      return;
    }

    if (text.length === 0) {
      setToast('Введите текст сообщения.');
      return;
    }

    const data = {
      theme,
      email,
      text,
    };
    fetchPostSupport({user, data});
  };

  render() {
    const {theme, email, text} = this.state;

    return (
      <Wrap
        style={base.backgroundColor}
        titleView={<Title title="Служба поддержки" />}>
        <View style={base.wrap1}>
          <Card title="Тема">
            <InputTextEdit
              returnKeyType="next"
              autoCapitalize="sentences"
              value={theme}
              editable
              onChangeText={this.onChangeTheme}
              onSubmitEditing={this.nextEmail}
            />
          </Card>

          <Card title="E-mail">
            <InputTextEdit
              ref={this.refEmail}
              textContentType="emailAddress"
              returnKeyType="next"
              value={email}
              editable
              onChangeText={this.onChangeEmail}
              onSubmitEditing={this.nextText}
            />
          </Card>

          <Card title="Текст сообщения">
            <InputTextEdit
              ref={this.refText}
              // inputStyle={base.input}
              returnKeyType="done"
              value={text}
              editable
              multiline
              onChangeText={this.onChangeText}
            />
          </Card>

          <View style={base.flex} />

          <ButtonFull title="Отправить" onPress={this.done} />
        </View>
        <Footer />
      </Wrap>
    );
  }
}
