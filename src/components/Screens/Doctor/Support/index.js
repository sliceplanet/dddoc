import {connect} from 'react-redux';
import component from './component';

import {setToast} from '../../../../store/actions';
import {fetchPostSupport} from '../../../../store/actions/support';

function mapStateToProps(state) {
  return {
    user: state.user,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setToast: (data) => dispatch(setToast(data)),
    fetchPostSupport: (data) => dispatch(fetchPostSupport(data)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(component);
