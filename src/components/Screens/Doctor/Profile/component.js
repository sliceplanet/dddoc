import React from 'react';
import moment from 'moment';

// Components
import Wrap from '../../../UI/Base/Wrap';
import Title from '../../../UI/Header/Title';
import PickerGender from '../../../UI/Picker/PickerGender';
import Card from '../../../UI/Card';
import Footer from '../../../UI/Footer';
import Avatar from '../../../UI/Avatar';
import InputTextEdit from '../../../UI/Input/InputTextEdit';
import InputDate from '../../../UI/Input/InputDate';
import InputTime from '../../../UI/Input/InputTime';
import InputCountryEdit from '../../../UI/Input/InputCountryEdit';
import InputSpecializations from '../../../UI/Input/InputSpecializations';
import InputCategoryEdit from '../../../UI/Input/InputCategoryEdit';

// Helpers
import * as functions from './functions';

// Style
import {base} from './styles';

export default class Profile extends React.Component {
  constructor(props) {
    super(props);

    functions.constructorState(this, props);
  }

  componentDidUpdate(prevProps) {
    if (prevProps.profile !== this.props.profile) {
      functions.profileComponentDidUpdate(this);
    }
  }

  onPressAvatar = (base64, photo, contentType) =>
    this.setState({base64, photo, contentType});
  onPressCancel = () => functions.onPressCancel(this);
  onPressButton = () => functions.onPressButton(this);

  onChangePickerGender = (gender) => this.setState({gender});

  onChangeLastName = (lastName) => this.setState({lastName});
  onChangeName = (name) => this.setState({name});
  onChangePatronymic = (patronymic) => this.setState({patronymic});
  onChangeBirthDate = (birthDate) => this.setState({birthDate});
  onChangeTimeFrom = (from) => {
    const {schedule} = this.state;
    this.setState({
      schedule: {
        ...schedule,
        from: moment(from).format(),
      },
    });
  };
  onChangeTimeTo = (to) => {
    const {schedule} = this.state;
    this.setState({
      schedule: {
        ...schedule,
        to: moment(to).format(),
      },
    });
  };
  onChangePhone = (phone) => this.setState({phone});
  onChangeEmail = (email) => this.setState({email});
  onChangeAddress = (address) => this.setState({address});
  onChangeCountry = (country) => this.setState({country});
  onChangeCity = (city) => this.setState({city});
  onChangeSpecializations = (specializations) =>
    this.setState({specializations});
  onChangeEducation = (education) => this.setState({education});
  onChangeWorkExperience = (workExperience) => this.setState({workExperience});
  onChangeCategory = (category) => this.setState({category});
  onChangeDescription = (description) => this.setState({description});
  onChangeOldPassword = (oldPassword) => this.setState({oldPassword});
  onChangeNewPassword = (newPassword) => this.setState({newPassword});
  onChangeConfNewPassword = (confNewPassword) =>
    this.setState({confNewPassword});
  onChangeUserName = (userName) => this.setState({userName});

  onBlur = () => {
    functions.onPressSave(this);
  };

  refName = (ref) => (this.name = ref);
  refPatronymic = (ref) => (this.patronymic = ref);
  refBirthDate = (ref) => (this.birthDate = ref);
  refEmail = (ref) => (this.email = ref);
  refCountry = (ref) => (this.country = ref);
  refCity = (ref) => (this.city = ref);
  refEducation = (ref) => (this.education = ref);
  refWorkExperience = (ref) => (this.workExperience = ref);
  refCategory = (ref) => (this.category = ref);
  refNewPassword = (ref) => (this.newPassword = ref);
  refConfNewPassword = (ref) => (this.confNewPassword = ref);

  nextName = () => this.name.focus();
  nextPatronymic = () => this.patronymic.focus();
  nextEmail = () => this.email.focus();
  nextCountry = () => this.country.focus();
  nextCity = () => this.city.focus();
  nextEducation = () => this.education.focus();
  nextWorkExperience = () => this.workExperience.focus();
  nextCategory = () => this.category.focus();
  nextNewPassword = () => this.newPassword.focus();
  nextConfNewPassword = () => this.confNewPassword.focus();

  render() {
    const {
      userName,
      gender,
      photo,
      lastName,
      name,
      patronymic,
      birthDate,
      phone,
      email,
      address,
      country,
      city,
      education,
      specializations,
      workExperience,
      category,
      schedule,
      description,
      oldPassword,
      newPassword,
      confNewPassword,
    } = this.state;
    const {isVerify} = this.props.profile;

    return (
      <Wrap
        style={base.backgroundColor}
        titleView={
          <Title
            title="Профиль"
            button="Сохранить"
            onPressButton={this.onBlur}
          />
        }>
        <Card title="Пол" verify={isVerify}>
          <PickerGender
            index={gender}
            onChange={this.onChangePickerGender}
            editable
          />
        </Card>
        <Card title="Пользователь">
          <Avatar
            source={photo}
            nickname={userName}
            onPress={this.onPressAvatar}
            onChangeText={this.onChangeUserName}
            editable
          />
          <InputTextEdit
            title="*Фамилия"
            textContentType="namePrefix"
            returnKeyType="next"
            autoCapitalize="sentences"
            value={lastName}
            editable
            onChangeText={this.onChangeLastName}
            onSubmitEditing={this.nextName}
          />
          <InputTextEdit
            ref={this.refName}
            title="*Имя"
            textContentType="name"
            returnKeyType="next"
            autoCapitalize="sentences"
            value={name}
            editable
            onChangeText={this.onChangeName}
            onSubmitEditing={this.nextPatronymic}
          />
          <InputTextEdit
            ref={this.refPatronymic}
            title="Отчество"
            textContentType="nameSuffix"
            returnKeyType="next"
            autoCapitalize="sentences"
            value={patronymic}
            editable
            onChangeText={this.onChangePatronymic}
            onSubmitEditing={this.nextEducation}
          />
          <InputDate
            ref={this.refBirthDate}
            title="Дата рождения"
            value={birthDate}
            editable
            onChange={this.onChangeBirthDate}
          />
        </Card>
        <Card title="Квалификация">
          <InputTextEdit
            ref={this.refEducation}
            title="ВУЗ"
            autoCapitalize="sentences"
            returnKeyType="next"
            value={education}
            editable
            onChangeText={this.onChangeEducation}
            onSubmitEditing={this.nextWorkExperience}
          />
          <InputSpecializations
            title="*Специализация"
            value={specializations}
            editable
            onChange={this.onChangeSpecializations}
          />
          <InputTextEdit
            ref={this.refWorkExperience}
            title="Стаж (лет)"
            keyboardType="numeric"
            returnKeyType="next"
            editable
            value={workExperience}
            onChangeText={this.onChangeWorkExperience}
            onSubmitEditing={this.nextCategory}
          />
          <InputCategoryEdit
            ref={this.refCategory}
            title="*Категория"
            value={category}
            editable
            onChange={this.onChangeCategory}
          />
        </Card>
        <Card title="Час приема">
          <InputTime
            title="Начало приема"
            value={schedule.from}
            editable
            onChange={this.onChangeTimeFrom}
          />
          <InputTime
            title="Конец приема"
            value={schedule.to}
            editable
            onChange={this.onChangeTimeTo}
          />
        </Card>
        <Card title="Контакты">
          <InputTextEdit
            title="*Телефон"
            textContentType="telephoneNumber"
            returnKeyType="next"
            keyboardType="phone-pad"
            // mask="[0000000000999]"
            value={phone}
            editable
            onChangeText={this.onChangePhone}
            onSubmitEditing={this.nextEmail}
          />
          <InputTextEdit
            ref={this.refEmail}
            textContentType="emailAddress"
            title="*E-mail"
            keyboardType="email-address"
            value={email}
            editable
            onChangeText={this.onChangeEmail}
          />
        </Card>
        <Card title="Место жительства">
          <InputTextEdit
            title="Адрес"
            textContentType="fullStreetAddress"
            returnKeyType="next"
            autoCapitalize="sentences"
            value={address}
            editable
            onChangeText={this.onChangeAddress}
            onSubmitEditing={this.nextCountry}
          />
          <InputCountryEdit
            ref={this.refCountry}
            title="*Страна"
            returnKeyType="next"
            value={country}
            editable
            onChange={this.onChangeCountry}
            onSubmitEditing={this.nextCity}
          />

          <InputTextEdit
            ref={this.refCity}
            title="Город"
            returnKeyType="next"
            textContentType="addressCity"
            autoCapitalize="sentences"
            value={city}
            editable
            onChangeText={this.onChangeCity}
            onSubmitEditing={this.nextCity}
          />
        </Card>
        <Card title="Информация обо мне">
          <InputTextEdit
            autoCapitalize="sentences"
            value={description}
            maxLength={2000}
            multiline
            editable
            onChangeText={this.onChangeDescription}
          />
        </Card>
        <Card title="Управление паролем">
          <InputTextEdit
            title="Старый пароль"
            textContentType="password"
            returnKeyType="next"
            value={oldPassword}
            secureTextEntry
            editable
            onChangeText={this.onChangeOldPassword}
            onSubmitEditing={this.nextNewPassword}
          />
          <InputTextEdit
            ref={this.refNewPassword}
            title="Новый пароль"
            textContentType="newPassword"
            returnKeyType="next"
            value={newPassword}
            secureTextEntry
            editable
            onChangeText={this.onChangeNewPassword}
            onSubmitEditing={this.nextConfNewPassword}
          />
          <InputTextEdit
            ref={this.refConfNewPassword}
            title="Повторите новый пароль"
            textContentType="newPassword"
            value={confNewPassword}
            secureTextEntry
            editable
            onChangeText={this.onChangeConfNewPassword}
          />
        </Card>
        <Footer style={base.footer} />
      </Wrap>
    );
  }
}
