import {connect} from 'react-redux';
import component from './component';

import {setToast, setNetworkIndicator} from '../../../../../store/actions';

function mapStateToProps(state) {
  return {
    user: state.user,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setToast: (data) => dispatch(setToast(data)),
    setNetworkIndicator: (data) => dispatch(setNetworkIndicator(data)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(component);
