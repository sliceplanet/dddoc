export function constructorState(_this, props) {
  const id = props.route.params?.id ?? '';
  _this.state = {
    items: [],
    id,
  };
}
