import React from 'react';
import {View, FlatList} from 'react-native';

// Components
import Wrap from '../../../../UI/Base/Wrap';
import Title from '../../../../UI/Header/Title';
import Footer from '../../../../UI/Footer';
import ItemReviewsMy from '../../../../UI/Items/ItemReviews/ItemReviewsMy';

// Helpers
import * as functions from './functions';

// Api
import {apiGetReviewsId} from '../../../../../store/api/reviews';

// Style
import {base} from './styles';

export default class Review extends React.Component {
  constructor(props) {
    super(props);
    functions.constructorState(this, props);
  }

  componentDidMount() {
    const {user, setNetworkIndicator} = this.props;
    const {id} = this.state;
    const path = {
      id,
    };
    const data = {
      page: 1,
      pageSize: 100,
    };

    setNetworkIndicator(true);
    apiGetReviewsId({user, path, data})
      .then((result) => {
        const {items} = result.data;
        this.setState({
          items,
        });
        setNetworkIndicator(false);
      })
      .catch((e) => {
        console.log(e);
        setNetworkIndicator(false);
      });
  }

  onRefresh = () => {};

  renderItem = ({item, index}) => <ItemReviewsMy key={index} {...item} />;

  render() {
    const {items} = this.state;
    return (
      <Wrap style={base.backgroundColor} titleView={<Title title="Отзывы" />}>
        <View style={base.wrap1}>
          <FlatList
            data={items}
            extraData={this.props}
            renderItem={this.renderItem}
            onRefresh={this.onRefresh}
            refreshing={false}
            keyExtractor={(item, index) => index.toString()}
          />
        </View>
        <Footer />
      </Wrap>
    );
  }
}
