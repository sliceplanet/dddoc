export const timezones = [
  {
    countries: '(UTC -12) Baker Island, Howland Island',
    timezone: -12,
  },
  {
    countries:
      '(UTC -11) Jarvis Island, Kingman Reef, Midway Atoll, Palmyra Atoll, Niue',
    timezone: -11,
  },
  {
    countries:
      '(UTC -10) Honolulu, French Polynesia, Cook Islands, Alaska, Hawaii, Johnston Atoll',
    timezone: -10,
  },
  {
    countries: '(UTC -9) Gambier Islands, Alaska',
    timezone: -9,
  },
  {
    countries: '(UTC -8) Los Angeles, Vancouver, Tijuana',
    timezone: -8,
  },
  {
    countries: '(UTC -7) Phoenix, Calgary, Ciudad Juárez',
    timezone: -7,
  },
  {
    countries:
      '(UTC -6) Mexico City, Chicago, Guatemala City, Tegucigalpa, Winnipeg, San José, San Salvador',
    timezone: -6,
  },
  {
    countries: '(UTC -5) New York, Toronto, Havana, Lima, Bogotá, Kingston',
    timezone: -5,
  },
  {
    countries:
      '(UTC -4) Santiago, Santo Domingo, Manaus, Caracas, La Paz, Halifax',
    timezone: -4,
  },
  {
    countries: '(UTC -3) São Paulo, Buenos Aires, Montevideo',
    timezone: -3,
  },
  {
    countries:
      '(UTC -2) Fernando de Noronha, South Georgia and the South Sandwich Islands',
    timezone: -2,
  },
  {
    countries: '(UTC -1) Cape Verde, Denmark, Portugal',
    timezone: -1,
  },
  {
    countries: '(UTC ±0) London, Dublin, Lisbon, Abidjan, Accra, Dakar',
    timezone: 0,
  },
  {
    countries:
      '(UTC +1) Berlin, Rome, Paris, Madrid, Warsaw, Lagos, Kinshasa, Algiers, Casablanca',
    timezone: 1,
  },
  {
    countries:
      '(UTC +2) Cairo, Johannesburg, Khartoum, Kiev, Bucharest, Athens, Jerusalem, Sofia',
    timezone: 2,
  },
  {
    countries: '(UTC +3) Moscow, Istanbul, Riyadh, Baghdad, Addis Ababa, Doha',
    timezone: 3,
  },
  {
    countries: '(UTC +4) Dubai, Baku, Tbilisi, Yerevan, Samara',
    timezone: 4,
  },
  {
    countries: '(UTC +5) Karachi, Tashkent, Yekaterinburg',
    timezone: 5,
  },
  {
    countries: '(UTC +6) Dhaka, Almaty, Omsk',
    timezone: 6,
  },
  {
    countries: '(UTC +7) Jakarta, Ho Chi Minh City, Bangkok, Krasnoyarsk',
    timezone: 7,
  },
  {
    countries:
      '(UTC +8) Shanghai, Taipei, Kuala Lumpur, Singapore, Perth, Manila, Makassar, Irkutsk',
    timezone: 8,
  },
  {
    countries: '(UTC +9) Tokyo, Seoul, Pyongyang, Ambon, Yakutsk',
    timezone: 9,
  },
  {
    countries: '(UTC +10) Sydney, Port Moresby, Vladivostok',
    timezone: 10,
  },
  {
    countries:
      '(UTC +11) Australia, Papua New Guinea, Solomon Islands, Vanuatu',
    timezone: 11,
  },
  {
    countries: '(UTC +12) Auckland, Suva, Petropavlovsk-Kamchatsky',
    timezone: 12,
  },
];
