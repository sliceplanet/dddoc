export function equals(props, state) {
  if (props) {
    return state || props;
  }
  return props || state;
}

export function getDate(duration) {
  let seconds = 0;
  let minutes = 0;
  let hours = 0;

  if (duration < 60) {
    seconds = duration;
    return `${seconds} сек.`;
  } else {
    seconds = duration % 60;
    duration = Math.ceil(duration / 60);
  }
  if (duration < 60) {
    minutes = duration;
    return `${minutes} мин. ${seconds} сек.`;
  } else {
    minutes = duration % 60;
    duration = Math.ceil(duration / 60);
  }
  if (duration < 60) {
    hours = duration;
  } else {
    hours = duration % 60;
    duration = Math.ceil(duration / 60);
  }
  return `${hours} час. ${minutes} мин. ${seconds} сек.`;
}

export function getStatus(status) {
  if (status.withdrawal) {
    switch (status.withdrawal) {
      case 'success': {
        return 'Выполнен';
      }
      case 'processing': {
        return 'Выполняется вывод';
      }
      case 'canceled': {
        return 'Отменен';
      }
      case 'waiting': {
        return 'В ожидании';
      }
      case 'pending': {
        return 'Приостановлен';
      }
      case 'error': {
        return 'Произошла ошибка, повторите пожалуйста операцию';
      }
    }
  }
  if (status.refill) {
    switch (status.refill) {
      case 'success': {
        return 'Выполнен';
      }
      case 'processing': {
        return 'Выполняется пополнение';
      }
      case 'canceled': {
        return 'Отменен';
      }
      case 'waiting': {
        return 'В ожидании';
      }
      case 'pending': {
        return 'Приостановлен';
      }
      case 'error': {
        return 'Произошла ошибка, повторите пожалуйста операцию';
      }
    }
  }
}

export function hasStatus(status) {
  if (status.withdrawal) {
    switch (status.withdrawal) {
      case 'waiting':
      case 'processing': {
        return true;
      }
      default: {
        return false;
      }
    }
  }
  if (status.refill) {
    switch (status.refill) {
      case 'waiting':
      case 'processing': {
        return true;
      }
      default: {
        return false;
      }
    }
  }
  return false;
}

export function hasErrorStatus(status) {
  if (status.withdrawal) {
    switch (status.withdrawal) {
      case 'error': {
        return true;
      }
      default: {
        return false;
      }
    }
  }
  if (status.refill) {
    switch (status.refill) {
      case 'error': {
        return true;
      }
      default: {
        return false;
      }
    }
  }
  return false;
}
