export function phone(value) {
  return /^\+?[0-9]+$/.test(value);
}
export function email(value) {
  return /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(value);
}
export function password(value) {
  return /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,20}$/.test(value);
}
export function card(value) {
  return /^\d{16}$/.test(value);
}
export function cvv(value) {
  return /^[0-9]{3,4}$/.test(value);
}
