export const logo = require('./direction/logo.png');
export const back = require('./direction/back.png');
export const check = require('./direction/check.png');
export const photo = require('./direction/photo.png');
export const selector = require('./direction/selector.png');
export const right = require('./direction/right.png');
export const actions = require('./direction/actions.png');
export const send = require('./direction/send.png');
export const arrowLeft = require('./direction/arrow_left.png');
export const arrowRight = require('./direction/arrow_right.png');
export const menu = require('./direction/menu.png');
export const alert = require('./direction/alert.png');
export const filter = require('./direction/filter.png');
export const search = require('./direction/search.png');
export const cross = require('./direction/cross.png');
export const star = require('./direction/star.png');
export const video = require('./direction/video.png');
export const messages = require('./direction/messages.png');
export const favorite = require('./direction/favorite.png');
export const reservation = require('./direction/reservation.png');
export const edit = require('./direction/edit.png');
export const trash = require('./direction/trash.png');
export const select = require('./direction/select.png');
export const unselect = require('./direction/unselect.png');
export const document = require('./direction/document.png');
export const cash = require('./direction/cash.png');
export const down = require('./direction/down.png');
export const up = require('./direction/up.png');
export const videoCall = require('./direction/video_call.png');

export const visa = require('./direction/visa.png');
export const mastercard = require('./direction/mastercard.png');
export const mir = require('./direction/mir.png');
export const yandexPay = require('./direction/yandex_pay.png');
export const qiwi = require('./direction/qiwi.png');
export const payeer = require('./direction/payeer.png');

export const menuFavorite = require('./direction/menu_favorite.png');
export const menuFind = require('./direction/menu_find.png');
export const menuLogout = require('./direction/menu_logout.png');
export const menuMessages = require('./direction/menu_messages.png');
export const menuPay = require('./direction/menu_pay.png');
export const menuProfile = require('./direction/menu_profile.png');
export const menuReviews = require('./direction/menu_reviews.png');
export const menuServices = require('./direction/menu_services.png');
export const menuSettings = require('./direction/menu_settings.png');
export const menuVideo = require('./direction/menu_video.png');
export const menuRight = require('./direction/menu_right.png');
