const initialState = [];

export default function initReducer(state = initialState, action) {
  switch (action.type) {
    case 'logoutUser': {
      return initialState;
    }
    case 'specializations': {
      return action.data;
    }
    default:
      return state;
  }
}
