const initialState = {
  items: [],
};

export default function initReducer(state = initialState, action) {
  switch (action.type) {
    case 'logoutUser': {
      return initialState;
    }
    case 'videos': {
      return action.data;
    }
    case 'reducerVideoCount': {
      const {id} = action;
      let s = state.items;
      const index = s.map((e) => e.id).indexOf(id);
      s[index] = {
        ...s[index],
        videoCount: s[index].videoCount + 1,
      };
      return {
        ...state,
        items: s,
      };
    }
    case 'reducerVideoTime': {
      const {id, duration} = action.notify.extra;
      let s = state.items;
      const index = s.map((e) => e.id).indexOf(id);
      s[index] = {
        ...s[index],
        duration:
          s[index].videoCount > 0 ? s[index].duration + duration : duration,
      };
      return {
        ...state,
        items: s,
      };
    }
    case 'reducerVideosCancel': {
      const {id} = action.notify.extra;
      let s = state.items;
      const index = s.map((e) => e.id).indexOf(id);
      s[index] = {
        ...s[index],
        status: 3,
      };
      return {
        ...state,
        items: s,
      };
    }
    case 'reducerVideosAccept': {
      const {id} = action.notify.extra;
      let s = state.items;
      const index = s.map((e) => e.id).indexOf(id);
      s[index] = {
        ...s[index],
        status: 2,
      };
      return {
        ...state,
        items: s,
      };
    }
    case 'reducerVideosConfirm': {
      const {id} = action.notify.extra;
      let s = state.items;
      const index = s.map((e) => e.id).indexOf(id);
      s[index] = {
        ...s[index],
        status: 1,
      };
      return {
        ...state,
        items: s,
      };
    }
    case 'videosCancel': {
      const {id} = action.data;
      let s = state.items;
      const index = s.map((e) => e.id).indexOf(id);
      s[index] = {
        ...s[index],
        status: 3,
      };
      return {
        ...state,
        items: s,
      };
    }
    case 'videosConfirm': {
      const {id} = action.data;
      let s = state.items;
      const index = s.map((e) => e.id).indexOf(id);
      s[index] = {
        ...s[index],
        status: 1,
      };
      return {
        ...state,
        items: s,
      };
    }
    case 'reducerVideos': {
      return {
        ...state,
        items: [
          {
            ...action.notify.extra,
            user:
              action.user.user.id === action.notify.to._id
                ? action.notify.from
                : action.notify.to,
            clientId: action.notify.clientId,
            doctorId: action.notify.doctorId,
          },
          ...state.items,
        ],
      };
    }
    case 'reducerVideoPay': {
      const {id} = action.notify.extra;
      let s = state.items;
      const index = s.map((e) => e.id).indexOf(id);
      s[index] = {
        ...s[index],
        isPaid: true,
      };
      return {
        ...state,
        items: s,
      };
    }
    case 'reducerAnswerClient': {
      const {id} = action;
      let s = state.items;
      const index = s.map((e) => e.id).indexOf(id);
      s[index] = {
        ...s[index],
        clientAnswer: true,
      };
      return {
        ...state,
        items: s,
      };
    }
    case 'reducerAnswerDoctor': {
      const {id} = action;
      let s = state.items;
      const index = s.map((e) => e.id).indexOf(id);
      s[index] = {
        ...s[index],
        doctorAnswer: true,
      };
      return {
        ...state,
        items: s,
      };
    }
    default:
      return state;
  }
}
