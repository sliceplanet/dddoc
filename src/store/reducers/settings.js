const initialState = {
  push: true,
  timezone: 0,
};

export default function initReducer(state = initialState, action) {
  switch (action.type) {
    case 'logoutUser': {
      return initialState;
    }
    case 'reducerPush': {
      const {push} = action;
      return {
        ...state,
        push,
      };
    }
    case 'reducerTimezone': {
      const {timezone} = action;
      return {
        ...state,
        timezone,
      };
    }
    default:
      return state;
  }
}
