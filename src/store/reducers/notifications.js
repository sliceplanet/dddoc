const initialState = [];

export default function initReducer(state = initialState, action) {
  switch (action.type) {
    case 'logoutUser': {
      return initialState;
    }
    case 'notifications': {
      return action.data.items.filter((e) => e.from);
    }
    case 'reducerNotify': {
      return [action.notify, ...state];
    }
    case 'notifyReaded': {
      const {id} = action.data;
      let s = state;
      const index = s.map((e) => e.id).indexOf(id);
      s[index] = {
        ...s[index],
        isRead: true,
      };
      return [...s];
    }
    case 'notifyDelete': {
      const {id} = action.data;
      return state.filter((e) => e.id !== id);
    }
    default:
      return state;
  }
}
