const initialState = {
  accessToken: '',
};

export default function initReducer(state = initialState, action) {
  switch (action.type) {
    case 'logoutUser': {
      return initialState;
    }
    case 'clientRegister':
    case 'doctorRegister':
    case 'userLogin': {
      return action.data;
    }
    default:
      return state;
  }
}
