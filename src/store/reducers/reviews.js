const initialState = {
  my: {
    items: [],
    page: 1,
    total: -1,
  },
  from: {
    items: [],
    page: 1,
    total: -1,
  },
};

export default function initReducer(state = initialState, action) {
  switch (action.type) {
    case 'logoutUser': {
      return initialState;
    }
    case 'reviewsMy': {
      return {
        ...state,
        my: {
          ...action.data,
          page: 1,
        },
      };
    }
    case 'reviewsMyAdd': {
      const {page, data} = action;
      const {items, total} = data;

      return {
        ...state,
        my: {
          items: [...state.my.items, ...items],
          page,
          total,
        },
      };
    }
    case 'reviewsFrom': {
      return {
        ...state,
        from: {
          ...action.data,
          page: 1,
        },
      };
    }
    case 'reviewsFromAdd': {
      const {page, data} = action;
      const {items, total} = data;

      return {
        ...state,
        from: {
          items: [...state.from.items, ...items],
          page,
          total,
        },
      };
    }
    case 'reducerFromAdd': {
      const {data} = action;

      return {
        ...state,
        from: {
          ...state.from,
          items: [...state.from.items, data],
        },
      };
    }
    case 'reducerMyAdd': {
      const {data} = action;

      return {
        ...state,
        my: {
          ...state.my,
          items: [...state.my.items, data],
        },
      };
    }
    case 'reducerReview': {
      const {extra} = action.notify;

      return {
        ...state,
        my: {
          ...state.my,
          items: [...state.my.items, extra],
        },
      };
    }
    default:
      return state;
  }
}
