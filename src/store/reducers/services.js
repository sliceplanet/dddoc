const initialState = [];

export default function initReducer(state = initialState, action) {
  switch (action.type) {
    case 'logoutUser': {
      return initialState;
    }
    case 'services': {
      return action.data;
    }
    case 'serviceEdit': {
      const {id} = action.data.path;
      let s = state;
      const index = s.map((e) => e.id).indexOf(id);
      s[index] = {
        ...s[index],
        ...action.data.data,
      };
      return [...s];
    }
    case 'serviceNew': {
      const {id} = action.data;
      const {
        price,
        description,
        duration,
        specializationId,
      } = action.actionData;
      return [
        ...state,
        {id, price, description, duration, specializationId, canRemove: true},
      ];
    }
    case 'serviceRemove': {
      const {id} = action.data;
      return state.filter((e) => e.id !== id);
    }
    default:
      return state;
  }
}
