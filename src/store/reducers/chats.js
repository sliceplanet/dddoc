const initialState = [];

export default function initReducer(state = initialState, action) {
  switch (action.type) {
    case 'logoutUser': {
      return initialState;
    }
    case 'chats': {
      const chats = [];
      const map = new Map();
      for (const item of [...action.data.chats, ...state]) {
        if (!map.has(item.id)) {
          map.set(item.id, true);
          chats.push({
            messages: [],
            ...item,
          });
        }
      }

      return chats.sort(function (a, b) {
        return new Date(b.lastUpdate) - new Date(a.lastUpdate);
      });
    }
    case 'chatsId': {
      const {chatId} = action.path;
      const chat = state.filter((e) => e.id === chatId)[0];
      const chats = state.filter((e) => e.id !== chatId);

      const m = [
        ...chat.messages,
        ...action.data.map((e) => {
          return {
            _id: e._id,
            text: e.text,
            createdAt: e.date,
            isReaded: e.isReaded || false,
            frontId: e.frontId ? e.frontId : e._id,
            attachments: e.attachments || null,
            type: e.type || 0,
            start: e.start || null,
            end: e.end || null,
            user: {
              _id: e.userId,
              avatar: null,
            },
          };
        }),
      ];

      let resMessages = [];
      const mapMessage = new Map();
      for (const item of m) {
        if (item.frontId && !mapMessage.has(item.frontId)) {
          mapMessage.set(item.frontId, true);
          resMessages.push(item);
        }
      }
      const messages = resMessages.sort(function (a, b) {
        return new Date(b.createdAt) - new Date(a.createdAt);
      });

      const c = [
        ...chats,
        {
          ...chat,
          lastUpdate: messages[0].createdAt,
          messages,
        },
      ];

      const resChats = [];
      const mapChats = new Map();
      for (const item of c) {
        if (!mapChats.has(item.id)) {
          mapChats.set(item.id, true);
          resChats.push(item);
        }
      }
      return resChats.sort(function (a, b) {
        return new Date(b.lastUpdate) - new Date(a.lastUpdate);
      });
    }
    // reducers
    case 'reducerAddMessage': {
      const {chatId, messages, user} = action;
      const ch = state.filter((e) => e.id === chatId);
      let chat = {};
      if (ch.length > 0) {
        chat = ch[0];
      } else {
        chat = {
          id: chatId,
          messages,
          lastUpdate: messages[0].createdAt,
          user,
        };
      }
      const chats = state.filter((e) => e.id !== chatId);
      let resMessages = [];

      const mapMessage = new Map();
      for (const item of [...chat.messages, ...messages].map((e) => {
        return {...e, frontId: e.frontId || e._id};
      })) {
        if (!mapMessage.has(item.frontId)) {
          mapMessage.set(item.frontId, true);
          resMessages.push(item);
        }
      }

      const m = resMessages.sort(function (a, b) {
        return new Date(b.createdAt) - new Date(a.createdAt);
      });

      return [
        ...chats,
        {
          ...chat,
          lastUpdate: m[0].createdAt,
          messages: m,
        },
      ].sort(function (a, b) {
        return new Date(b.lastUpdate) - new Date(a.lastUpdate);
      });
    }
    case 'reducerReadedMessage': {
      const {chatId, messageId} = action;

      const chats = state.filter((e) => e.id !== chatId);
      const chat = state.filter((e) => e.id === chatId)[0];
      const messages = chat.messages.filter((e) => e._id !== messageId);
      const message = chat.messages.filter((e) => e._id === messageId)[0];

      return [
        ...chats,
        {
          ...chat,
          unread: chat.unread > 0 ? chat.unread - 1 : 0,
          messages: [
            ...messages,
            {
              ...message,
              isReaded: true,
            },
          ].sort(function (a, b) {
            return new Date(b.createdAt) - new Date(a.createdAt);
          }),
        },
      ].sort(function (a, b) {
        return new Date(b.lastUpdate) - new Date(a.lastUpdate);
      });
    }
    case 'reducerReceive': {
      const {
        id,
        chatId,
        messageId,
        text,
        createdAt,
        attachments,
        type,
        from,
        to,
      } = action.receive;
      const chats = state.filter((e) => e.id !== chatId);
      const chat = state.filter((e) => e.id === chatId);

      let user = {};
      if (from._id === action.user.user.id) {
        user = from;
      } else {
        user = to;
      }
      return [
        ...chats,
        chat.length > 0
          ? {
              ...chat[0],
              lastUpdate: createdAt,
              unread: chat[0].unread ? chat[0].unread + 1 : 1,
              messages: [
                ...chat[0].messages,
                {
                  _id: messageId,
                  text,
                  createdAt,
                  isReaded: false,
                  frontId: id,
                  attachments: attachments || null,
                  type: type || 0,
                  user: {
                    _id: user._id,
                    avatar: null,
                  },
                },
              ].sort(function (a, b) {
                return new Date(b.createdAt) - new Date(a.createdAt);
              }),
            }
          : {
              id: chatId,
              user,
              lastUpdate: createdAt,
              messages: [
                {
                  _id: messageId,
                  text,
                  createdAt,
                  frontId: id,
                  isReaded: false,
                  type: type || 0,
                  user,
                },
              ],
            },
      ].sort(function (a, b) {
        return new Date(b.lastUpdate) - new Date(a.lastUpdate);
      });
    }
    case 'reducerCreated': {
      const {
        id,
        chatId,
        messageId,
        text,
        attachments,
        createdAt,
        type,
        isSupport,
        isAdmin,
        from,
        to,
      } = action.created;

      let user = {};
      if (from._id === action.user.user.id) {
        user = to;
      } else {
        user = from;
      }

      return [
        ...state,
        {
          id: chatId,
          user,
          isSupport,
          isAdmin,
          messages: [
            {
              _id: messageId,
              text,
              createdAt,
              isReaded: false,
              frontId: id,
              attachments: attachments || null,
              type: type || 0,
              user: {
                _id: user._id,
                avatar: null,
              },
            },
          ],
          lastUpdate: createdAt,
        },
      ];
    }
    default:
      return state;
  }
}
