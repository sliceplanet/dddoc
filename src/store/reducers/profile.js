const initialState = {};

export default function initReducer(state = initialState, action) {
  switch (action.type) {
    case 'logoutUser': {
      return initialState;
    }
    case 'doctorData':
    case 'clientData': {
      return action.data;
    }
    case 'scheduleData': {
      return {
        ...state,
        schedule: {
          ...action.data,
        },
      };
    }
    case 'reducerVerify': {
      return {
        ...state,
        isVerify: true,
      };
    }
    default:
      return state;
  }
}
