import {combineReducers} from 'redux';

import networkIndicator from './networkIndicator';
import toast from './toast';
import user from './user';
import profile from './profile';
import specializations from './specializations';
import chats from './chats';
import favorites from './favorites';
import videos from './videos';
import services from './services';
import notifications from './notifications';
import settings from './settings';
import doctorFind from './doctorFind';
import alert from './alert';
import reviews from './reviews';
import balance from './balance';
import playerId from './playerId';

export default combineReducers({
  networkIndicator,
  toast,

  user,
  profile,
  specializations,
  chats,
  favorites,
  videos,
  services,
  notifications,
  settings,
  doctorFind,
  alert,
  reviews,
  balance,
  playerId,
});
