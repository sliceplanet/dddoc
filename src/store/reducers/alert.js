const initialState = {
  type: '',
  extra: {},
  notify: {},
};

export default function initReducer(state = initialState, action) {
  switch (action.type) {
    case 'logoutUser':
    case 'reduceOnPressClose': {
      return initialState;
    }
    case 'reducerYourRequestHasBeenSent': {
      return {
        ...state,
        type: 'YourRequestHasBeenSent',
      };
    }
    case 'reducerDoctorPay': {
      return {
        ...state,
        type: 'DoctorPay',
      };
    }
    case 'reducerClientPay': {
      return {
        ...state,
        type: 'ClientPay',
      };
    }
    case 'reducerGiveFeedback': {
      return {
        ...state,
        type: 'GiveFeedback',
      };
    }
    case 'reducerFeedback': {
      return {
        ...state,
        type: 'Feedback',
      };
    }
    case 'reducerDepositMethods': {
      return {
        ...state,
        type: 'DepositMethods',
      };
    }
    case 'reducerOutputMethods': {
      return {
        ...state,
        type: 'OutputMethods',
      };
    }
    case 'reducerMethodCard': {
      return {
        ...state,
        type: 'MethodCard',
      };
    }
    case 'reducerMethodPayeer': {
      return {
        ...state,
        type: 'MethodPayeer',
      };
    }
    case 'reducerAlertMessage': {
      return {
        ...state,
        type: 'AlertMessage',
      };
    }
    case 'reducerBookingConfirmed': {
      const {id} = action;
      return {
        ...state,
        type: 'BookingConfirmed',
        extra: {
          id,
        },
      };
    }
    case 'reducerCallConfirmed': {
      const {id} = action;
      return {
        ...state,
        type: 'CallConfirmed',
        extra: {
          id,
        },
      };
    }
    case 'reducerMethodPayment': {
      const {id} = action;
      return {
        type: 'MethodPayment',
        extra: {
          id,
        },
      };
    }
    case 'reducerAlertVideoCallRequest': {
      const {id} = action;
      return {
        type: 'AlertVideoCallRequest',
        extra: {
          id,
        },
      };
    }
    case 'reducerDidTheVideoCallTakePlace': {
      const {id} = action;
      return {
        type: 'DidTheVideoCallTakePlace',
        extra: {
          id,
        },
      };
    }
    case 'reducerAlertSound': {
      const {notify} = action;
      return {
        ...state,
        type: 'AlertSound',
        notify,
      };
    }
    case 'reducerDoctorBusyNow': {
      const {_id, avatar, lastName, name, patronymic} = action.notify.from;
      return {
        ...state,
        type: 'DoctorBusyNow',
        extra: {
          _id,
          avatar,
          lastName,
          name,
          patronymic,
        },
      };
    }
    case 'reducerBookingConfirmation': {
      return {
        ...state,
        type: 'BookingConfirmation',
        extra: {...action.notify.extra, ...action.notify.from},
        notify: action.notify,
      };
    }
    default:
      return state;
  }
}
