const initialState = {
  items: [],
  page: 1,
  total: -1,
};

export default function initReducer(state = initialState, action) {
  switch (action.type) {
    case 'logoutUser': {
      return initialState;
    }
    case 'doctorFind': {
      return {...action.data, page: 1};
    }
    case 'doctorFindAdd': {
      const {page, data} = action;
      const {items, total} = data;

      return {items: [...state.items, ...items], page, total};
    }
    case 'updateFavorite': {
      let {items} = state;
      const {id} = action.data.path;
      const index = items.map((e) => e.id).indexOf(id);
      items[index] = {...items[index], isFavourite: action.favourite};
      return {
        ...state,
        items: [...items],
      };
    }
    case 'reducerStatus': {
      let {items} = state;
      const {id, online} = action.status;
      const index = items.map((e) => e.id).indexOf(id);

      if (index === -1) {
        return state;
      }
      items[index] = {...items[index], online};
      return {
        ...state,
        items: [...items],
      };
    }
    default:
      return state;
  }
}
