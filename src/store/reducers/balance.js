const initialState = {
  balance: 0,
  transactions: [],
  page: 1,
  total: 0,
};

export default function initReducer(state = initialState, action) {
  switch (action.type) {
    case 'logoutUser': {
      return initialState;
    }
    case 'balance': {
      return {
        ...state,
        balance: action.data.balance,
        status: action.data.status,
      };
    }
    case 'transactions': {
      const {balance, status, transactions} = state;
      const {items, total} = action.data;
      const {page} = action.path;

      return {
        balance,
        status,
        transactions: page > 1 ? [...transactions, ...items] : items,
        page,
        total: total ? total : 0,
      };
    }
    default:
      return state;
  }
}
