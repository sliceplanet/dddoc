const initialState = [];

export default function initReducer(state = initialState, action) {
  switch (action.type) {
    case 'logoutUser': {
      return initialState;
    }
    case 'favorites': {
      return action.data.items;
    }
    case 'addFavorite': {
      return [...state, action.data];
    }
    case 'removeFavorite': {
      return state.filter((e) => e.id !== action.data.id);
    }
    default:
      return state;
  }
}
