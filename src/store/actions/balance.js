export function fetchGetBalance(data) {
  return {
    type: 'fetchGetBalance',
    data,
  };
}

export function fetchGetTransactions(data) {
  return {
    type: 'fetchGetTransactions',
    data,
  };
}

export function fetchPostRefill(data) {
  return {
    type: 'fetchPostRefill',
    data,
  };
}

export function fetchPutTransfer(data) {
  return {
    type: 'fetchPutTransfer',
    data,
  };
}

export function fetchPutVideoIdPayment(data) {
  return {
    type: 'fetchPutVideoIdPayment',
    data,
  };
}

export function fetchPostTransfer(data) {
  return {
    type: 'fetchPostTransfer',
    data,
  };
}

export function fetchPostPayout(data) {
  return {
    type: 'fetchPostPayout',
    data,
  };
}
