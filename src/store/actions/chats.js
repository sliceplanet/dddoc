export function fetchGetChats(data) {
  return {
    type: 'fetchGetChats',
    data,
  };
}

export function fetchPostChatsId(data) {
  return {
    type: 'fetchPostChatsId',
    data,
  };
}

export function reducerAddMessage(messages, chatId, user) {
  return {
    type: 'reducerAddMessage',
    messages,
    chatId,
    user,
  };
}

export function reducerReadedMessage(messageId, chatId) {
  return {
    type: 'reducerReadedMessage',
    messageId,
    chatId,
  };
}

export function reducerReceive(receive, user) {
  return {
    type: 'reducerReceive',
    receive,
    user,
  };
}

export function reducerCreated(created, user) {
  return {
    type: 'reducerCreated',
    created,
    user,
  };
}
