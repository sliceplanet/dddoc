export function fetchGetReviewsMy(data) {
  return {
    type: 'fetchGetReviewsMy',
    data,
  };
}

export function fetchGetReviewsFrom(data) {
  return {
    type: 'fetchGetReviewsFrom',
    data,
  };
}

export function fetchPostReviewsId(data) {
  return {
    type: 'fetchPostReviewsId',
    data,
  };
}

export function reducerReview(notify) {
  return {
    type: 'reducerReview',
    notify,
  };
}
