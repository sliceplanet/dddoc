export function fetchPostDoctorFind(data) {
  return {
    type: 'fetchPostDoctorFind',
    data,
  };
}

export function fetchPostDoctorVerify(data) {
  return {
    type: 'fetchPostDoctorVerify',
    data,
  };
}
