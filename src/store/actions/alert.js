export function reduceOnPressClose() {
  return {
    type: 'reduceOnPressClose',
  };
}

export function reducerYourRequestHasBeenSent() {
  return {
    type: 'reducerYourRequestHasBeenSent',
  };
}

export function reducerGiveFeedback() {
  return {
    type: 'reducerGiveFeedback',
  };
}

export function reducerDepositMethods() {
  return {
    type: 'reducerDepositMethods',
  };
}

export function reducerOutputMethods() {
  return {
    type: 'reducerOutputMethods',
  };
}

export function reducerMethodCard() {
  return {
    type: 'reducerMethodCard',
  };
}

export function reducerMethodPayeer() {
  return {
    type: 'reducerMethodPayeer',
  };
}

export function reducerMethodPayment(id) {
  return {
    type: 'reducerMethodPayment',
    id,
  };
}

export function reducerAlertMessage() {
  return {
    type: 'reducerAlertMessage',
  };
}

export function reducerBookingConfirmed(id) {
  return {
    type: 'reducerBookingConfirmed',
    id,
  };
}

export function reducerCallConfirmed(id) {
  return {
    type: 'reducerCallConfirmed',
    id,
  };
}

export function reducerAlertVideoCallRequest(id) {
  return {
    type: 'reducerAlertVideoCallRequest',
    id,
  };
}

export function reducerBookingConfirmation(notify) {
  return {
    type: 'reducerBookingConfirmation',
    notify,
  };
}

export function reducerDidTheVideoCallTakePlace(id) {
  return {
    type: 'reducerDidTheVideoCallTakePlace',
    id,
  };
}

export function reducerDoctorBusyNow(notify) {
  return {
    type: 'reducerDoctorBusyNow',
    notify,
  };
}

export function reducerAlertSound(notify) {
  return {
    type: 'reducerAlertSound',
    notify,
  };
}

export function reducerFeedback(notify) {
  return {
    type: 'reducerFeedback',
    notify,
  };
}

export function reducerDoctorPay() {
  return {
    type: 'reducerDoctorPay',
  };
}

export function reducerClientPay() {
  return {
    type: 'reducerClientPay',
  };
}
