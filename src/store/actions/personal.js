export function fetchPutClientData(data) {
  return {
    type: 'fetchPutClientData',
    data,
  };
}

export function fetchPutDoctorData(data) {
  return {
    type: 'fetchPutDoctorData',
    data,
  };
}

export function fetchGetSpecializations(data) {
  return {
    type: 'fetchGetSpecializations',
    data,
  };
}

export function reducerVerify(notify) {
  return {
    type: 'reducerVerify',
    notify,
  };
}
