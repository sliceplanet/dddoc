export function fetchGetVideos(data) {
  return {
    type: 'fetchGetVideos',
    data,
  };
}

export function fetchPutVideosIdStart(data) {
  return {
    type: 'fetchPutVideosIdStart',
    data,
  };
}

export function fetchPostVideosNew(data) {
  return {
    type: 'fetchPostVideosNew',
    data,
  };
}

export function fetchPutVideosIdCancel(data) {
  return {
    type: 'fetchPutVideosIdCancel',
    data,
  };
}

export function fetchPutVideosIdConfirm(data) {
  return {
    type: 'fetchPutVideosIdConfirm',
    data,
  };
}

export function fetchPutVideosIdDone(data) {
  return {
    type: 'fetchPutVideosIdDone',
    data,
  };
}

export function reducerVideos(notify, user) {
  return {
    type: 'reducerVideos',
    notify,
    user,
  };
}

export function reducerVideosCancel(notify) {
  return {
    type: 'reducerVideosCancel',
    notify,
  };
}

export function reducerVideosAccept(notify) {
  return {
    type: 'reducerVideosAccept',
    notify,
  };
}

export function reducerVideosConfirm(notify) {
  return {
    type: 'reducerVideosConfirm',
    notify,
  };
}

export function reducerVideoPay(notify) {
  return {
    type: 'reducerVideoPay',
    notify,
  };
}

export function reducerVideoCount(id) {
  return {
    type: 'reducerVideoCount',
    id,
  };
}

export function reducerVideoTime(notify) {
  return {
    type: 'reducerVideoTime',
    notify,
  };
}

export function reducerAnswerClient(id) {
  return {
    type: 'reducerAnswerClient',
    id,
  };
}

export function reducerAnswerDoctor(id) {
  return {
    type: 'reducerAnswerDoctor',
    id,
  };
}
