export function fetchPostUserLogin(data) {
  return {
    type: 'fetchPostUserLogin',
    data,
  };
}

export function fetchPostUserForgotPassword(data) {
  return {
    type: 'fetchPostUserForgotPassword',
    data,
  };
}

export function fetchPostUserLogout(data) {
  return {
    type: 'fetchPostUserLogout',
    data,
  };
}

export function logoutUser(data) {
  return {
    type: 'logoutUser',
    data,
  };
}
