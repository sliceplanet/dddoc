export function fetchGetNotifications(data) {
  return {
    type: 'fetchGetNotifications',
    data,
  };
}

export function fetchPutNotifications(data) {
  return {
    type: 'fetchPutNotifications',
    data,
  };
}

export function fetchDeleteNotifications(data) {
  return {
    type: 'fetchDeleteNotifications',
    data,
  };
}

export function fetchNotify(notify) {
  return {
    type: 'fetchNotify',
    notify,
  };
}

export function reducerNotify(notify) {
  return {
    type: 'reducerNotify',
    notify,
  };
}

export function reducerStatus(status) {
  return {
    type: 'reducerStatus',
    status,
  };
}
