export function fetchPostClientRegister(data) {
  return {
    type: 'fetchPostClientRegister',
    data,
  };
}
export function fetchPostDoctorRegister(data) {
  return {
    type: 'fetchPostDoctorRegister',
    data,
  };
}
