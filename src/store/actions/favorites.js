export function fetchGetFavorites(data) {
  return {
    type: 'fetchGetFavorites',
    data,
  };
}

export function fetchPutFavoritesId(data) {
  return {
    type: 'fetchPutFavoritesId',
    data,
  };
}

export function fetchDeleteFavoritesId(data) {
  return {
    type: 'fetchDeleteFavoritesId',
    data,
  };
}
