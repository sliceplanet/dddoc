export function reducerPush(push) {
  return {
    type: 'reducerPush',
    push,
  };
}

export function reducerTimezone(timezone) {
  return {
    type: 'reducerTimezone',
    timezone,
  };
}
