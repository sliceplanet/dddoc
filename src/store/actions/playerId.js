export function reducePlayerId(data) {
  return {
    type: 'reducePlayerId',
    data,
  };
}
