export function setNetworkIndicator(data) {
  return {
    type: 'networkIndicator',
    data,
  };
}

export function setToast(data) {
  return {
    type: 'toast',
    data,
  };
}
