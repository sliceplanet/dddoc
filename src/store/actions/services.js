export function fetchGetServices(data) {
  return {
    type: 'fetchGetServices',
    data,
  };
}

export function fetchPostServicesId(data) {
  return {
    type: 'fetchPostServicesId',
    data,
  };
}

export function fetchDeleteServicesId(data) {
  return {
    type: 'fetchDeleteServicesId',
    data,
  };
}

export function fetchPostServicesNew(data) {
  return {
    type: 'fetchPostServicesNew',
    data,
  };
}
