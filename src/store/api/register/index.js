import axios from 'axios';

import {axiosConfig, URL} from '../index';

export function apiPostClientRegister(body) {
  return axios.post(`${URL}/api/client/register`, body, axiosConfig);
}

export function apiPostDoctorRegister(body) {
  return axios.post(`${URL}/api/doctor/register`, body, axiosConfig);
}
