import axios from 'axios';

import {axiosConfig, URL} from '../index';

export function apiGetTerms() {
  return axios.get(`${URL}/api/admin/settings/terms`, axiosConfig);
}
