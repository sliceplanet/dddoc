import axios from 'axios';

import {axiosConfigToken, getParams, URL} from '../index';

export function apiGetChats(body) {
  return axios.get(
    `${URL}/api/chats${getParams(body.data)}`,
    axiosConfigToken(body.user.accessToken),
  );
}

export function apiPostChatsId(body) {
  return axios.post(
    `${URL}/api/chats/${body.path.chatId}${getParams(body.data)}`,
    null,
    axiosConfigToken(body.user.accessToken),
  );
}
