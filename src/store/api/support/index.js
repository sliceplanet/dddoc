import axios from 'axios';

import {axiosConfigToken, URL} from '../index';

export function apiPostSupport(body) {
  return axios.post(
    `${URL}/api/support`,
    null,
    axiosConfigToken(body.user.accessToken),
  );
}
