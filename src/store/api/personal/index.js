import axios from 'axios';
import {Buffer} from 'buffer';

import {
  axiosConfig,
  axiosConfigToken,
  axiosConfigOctetStream,
  URL,
} from '../index';

export function apiGetClientData(body) {
  return axios.get(
    `${URL}/api/client/data`,
    axiosConfigToken(body.user.accessToken),
  );
}

export function apiGetDoctorData(body) {
  return axios.get(
    `${URL}/api/doctor/data`,
    axiosConfigToken(body.user.accessToken),
  );
}

export function apiPutClientData(body) {
  return axios.put(
    `${URL}/api/client/data`,
    body.data,
    axiosConfigToken(body.user.accessToken),
  );
}

export function apiPutDoctorData(body) {
  return axios.put(
    `${URL}/api/doctor/data`,
    body.data,
    axiosConfigToken(body.user.accessToken),
  );
}

export function apiPostUserPhoto(body) {
  return axios.post(
    `${URL}/api/files/upload`,
    Buffer.from(body.data, 'base64'),
    axiosConfigOctetStream(body.user.accessToken, body.contentType),
  );
}

export function apiGetSpecializations() {
  return axios.get(`${URL}/api/specializations`, axiosConfig);
}
