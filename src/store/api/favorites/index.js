import axios from 'axios';

import {axiosConfigToken, getParams, URL} from '../index';

export function apiGetFavorites(body) {
  return axios.get(
    `${URL}/api/favourites${getParams(body.path)}`,
    axiosConfigToken(body.user.accessToken),
  );
}

export function apiPutFavorites(body) {
  return axios.put(
    `${URL}/api/favourites/${body.path.id}`,
    null,
    axiosConfigToken(body.user.accessToken),
  );
}

export function apiDeleteFavorites(body) {
  return axios.delete(
    `${URL}/api/favourites/${body.path.id}`,
    axiosConfigToken(body.user.accessToken),
  );
}
