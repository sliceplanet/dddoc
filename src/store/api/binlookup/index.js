import axios from 'axios';

export function apiGetBinlist(card) {
  return axios.get(`https://lookup.binlist.net/${card}`, {
    headers: {
      'Accept-Version': '3',
    },
  });
}
