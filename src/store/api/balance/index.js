import axios from 'axios';

import {axiosConfigToken, getParams, URL} from '../index';

export function apiGetPaymentBalance(body) {
  return axios.get(
    `${URL}/api/payment/balance`,
    axiosConfigToken(body.user.accessToken),
  );
}

export function apiGetPaymentTransactions(body) {
  return axios.get(
    `${URL}/api/payment/transactions${getParams(body.path)}`,
    axiosConfigToken(body.user.accessToken),
  );
}

export function apiPostPaymentRefill(body) {
  return axios.post(
    `${URL}/api/payment/refill${getParams(body.data)}`,
    null,
    axiosConfigToken(body.user.accessToken),
  );
}

export function apiPostPaymentPayout(body) {
  return axios.post(
    `${URL}/api/payment/payout${getParams(body.path)}`,
    null,
    axiosConfigToken(body.user.accessToken),
  );
}

export function apiPutPaymentTransfer(body) {
  return axios.put(
    `${URL}/api/payment/transfer`,
    body.data,
    axiosConfigToken(body.user.accessToken),
  );
}

export function apiPostPaymentTransfer(body) {
  return axios.post(
    `${URL}/api/payment/transfer${getParams(body.path)}`,
    body.data,
    axiosConfigToken(body.user.accessToken),
  );
}

export function apiPutVideoIdPayment(body) {
  return axios.put(
    `${URL}/api/payment/${body.path.videoId}/video`,
    null,
    axiosConfigToken(body.user.accessToken),
  );
}
