import axios from 'axios';

import {axiosConfigToken, getParams, URL} from '../index';

export function apiGetReviewsMy(body) {
  return axios.get(
    `${URL}/api/reviews/my${getParams(body.path)}`,
    axiosConfigToken(body.user.accessToken),
  );
}

export function apiGetReviewsFrom(body) {
  return axios.get(
    `${URL}/api/reviews/from/my${getParams(body.path)}`,
    axiosConfigToken(body.user.accessToken),
  );
}

export function apiGetReviewsId(body) {
  return axios.get(
    `${URL}/api/reviews/${body.path.id}${getParams(body.data)}`,
    axiosConfigToken(body.user.accessToken),
  );
}

export function apiPostReviewsId(body) {
  return axios.post(
    `${URL}/api/reviews/${body.path.id}`,
    body.data,
    axiosConfigToken(body.user.accessToken),
  );
}
