import axios from 'axios';

import {axiosConfigToken, getParams, URL} from '../index';

export function apiGetNotifications(body) {
  return axios.get(
    `${URL}/api/notifications${getParams(body.path)}`,
    axiosConfigToken(body.user.accessToken),
  );
}

export function apiPutNotifications(body) {
  return axios.put(
    `${URL}/api/notifications${getParams(body.path)}`,
    null,
    axiosConfigToken(body.user.accessToken),
  );
}

export function apiDeleteNotifications(body) {
  return axios.delete(
    `${URL}/api/notifications/${body.path.id}`,
    axiosConfigToken(body.user.accessToken),
  );
}
