import axios from 'axios';

import {axiosConfigToken, URL} from '../index';

export function apiGetServices(body) {
  return axios.get(
    `${URL}/api/services`,
    axiosConfigToken(body.user.accessToken),
  );
}

export function apiGetServicesId(body) {
  return axios.get(
    `${URL}/api/services/${body.path.id}`,
    axiosConfigToken(body.user.accessToken),
  );
}

export function apiPostServicesId(body) {
  return axios.post(
    `${URL}/api/services/${body.path.id}`,
    body.data,
    axiosConfigToken(body.user.accessToken),
  );
}
export function apiDeleteServicesId(body) {
  return axios.delete(
    `${URL}/api/services/${body.path.id}`,
    axiosConfigToken(body.user.accessToken),
  );
}
export function apiPostServicesNew(body) {
  return axios.post(
    `${URL}/api/services/new`,
    body.data,
    axiosConfigToken(body.user.accessToken),
  );
}
