import axios from 'axios';

import {axiosConfigToken, getParams, URL} from '../index';

export function apiPostDoctorSchedule(body) {
  return axios.post(
    `${URL}/api/doctor/schedule`,
    body.data,
    axiosConfigToken(body.user.accessToken),
  );
}

export function apiPostDoctorFind(body) {
  return axios.post(
    `${URL}/api/doctor/find${getParams(body.path)}`,
    body.data,
    axiosConfigToken(body.user.accessToken),
  );
}

export function apiPostDoctorVerify(body) {
  return axios.post(
    `${URL}/api/doctor/verify`,
    body.data,
    axiosConfigToken(body.user.accessToken),
  );
}
