import axios from 'axios';

import {axiosConfig, axiosConfigToken, URL} from '../index';

export function apiPostUserLogin(body) {
  return axios.post(`${URL}/api/user/login`, body, axiosConfig);
}

export function apiPostUserForgotPassword(body) {
  return axios.post(`${URL}/api/user/forgotpassword`, body, axiosConfig);
}

export function apiPostUserLogout(body) {
  return axios.post(
    `${URL}/api/user/logout`,
    body.data,
    axiosConfigToken(body.user.accessToken),
  );
}
