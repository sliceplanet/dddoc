import axios from 'axios';

import {axiosConfigToken, getParams, URL} from '../index';

export function apiGetVideos(body) {
  return axios.get(
    `${URL}/api/consultations${getParams(body.path)}`,
    axiosConfigToken(body.user.accessToken),
  );
}

export function apiPostVideosNew(body) {
  return axios.post(
    `${URL}/api/consultation/new`,
    body.data,
    axiosConfigToken(body.user.accessToken),
  );
}

export function apiPutVideosIdStart(body) {
  return axios.put(
    `${URL}/api/videos/${body.path.id}/start`,
    null,
    axiosConfigToken(body.user.accessToken),
  );
}

export function apiPutVideosIdCancel(body) {
  return axios.put(
    `${URL}/api/consultation/${body.path.id}/cancel`,
    null,
    axiosConfigToken(body.user.accessToken),
  );
}

export function apiPutVideosIdConfirm(body) {
  return axios.put(
    `${URL}/api/consultation/${body.path.id}/confirm`,
    null,
    axiosConfigToken(body.user.accessToken),
  );
}

export function apiPutVideosIdDone(body) {
  return axios.put(
    `${URL}/api/videos/${body.path.id}/done`,
    null,
    axiosConfigToken(body.user.accessToken),
  );
}

export function apiPutVideosIdDecline(body) {
  return axios.put(
    `${URL}/api/videos/${body.path.id}/decline`,
    null,
    axiosConfigToken(body.user.accessToken),
  );
}

export function apiPutVideosIdAccept(body) {
  return axios.put(
    `${URL}/api/videos/${body.path.id}/accept`,
    null,
    axiosConfigToken(body.user.accessToken),
  );
}

export function apiPutVideosId(body) {
  return axios.put(
    `${URL}/api/consultation/${body.path.id}${getParams(body.data)}`,
    null,
    axiosConfigToken(body.user.accessToken),
  );
}
