import {takeLatest, takeEvery, put} from 'redux-saga/effects';

import * as Register from './register';
import * as Auth from './auth';
import * as Personal from './personal';
import * as Chats from './chats';
import * as Favorites from './favorites';
import * as Videos from './videos';
import * as Services from './services';
import * as Notifications from './notifications';
import * as Doctor from './doctor';
import * as Support from './support';
import * as Reviews from './reviews';
import * as Balance from './balance';

export function* _catch(error, title) {
  yield put({type: 'networkIndicator', data: false});
  console.log(`${title} ->`, error.message);
  console.log(`${title} ->`, error);
  if (error.response) {
    console.log(`${title} ->`, error.response);
    switch (error.response.status) {
      case 401: {
        yield put({type: 'logoutUser'});
        return;
      }
      default: {
        const {msg} = error.response.data;
        yield put({type: 'toast', data: msg});
      }
    }
  } else {
    yield put({type: 'toast', data: error.message});
  }
}

function* dataSaga() {
  yield takeLatest('fetchPostClientRegister', Register.postClientRegister);
  yield takeLatest('fetchPostDoctorRegister', Register.postDoctorRegister);

  yield takeLatest('fetchPostUserLogin', Auth.postUserLogin);
  yield takeLatest('fetchPostUserForgotPassword', Auth.postUserForgotPassword);
  yield takeLatest('fetchPostUserLogout', Auth.postUserLogout);

  yield takeLatest('fetchPutClientData', Personal.putClientData);
  yield takeLatest('fetchPutDoctorData', Personal.putDoctorData);
  yield takeLatest('fetchGetSpecializations', Personal.getSpecializations);

  yield takeLatest('fetchGetChats', Chats.getChats);
  yield takeLatest('fetchPostChatsId', Chats.postChatsId);

  yield takeLatest('fetchGetFavorites', Favorites.getFavorites);
  yield takeLatest('fetchPutFavoritesId', Favorites.putFavoritesId);
  yield takeLatest('fetchDeleteFavoritesId', Favorites.deleteFavoritesId);

  yield takeLatest('fetchGetVideos', Videos.getVideos);
  yield takeLatest('fetchPostVideosNew', Videos.postVideosNew);
  yield takeLatest('fetchPutVideosIdStart', Videos.putVideosIdStart);
  yield takeLatest('fetchPutVideosIdCancel', Videos.putVideosIdCancel);
  yield takeLatest('fetchPutVideosIdConfirm', Videos.putVideosIdConfirm);
  yield takeLatest('fetchPutVideosIdDone', Videos.putVideosIdDone);

  yield takeLatest('fetchGetServices', Services.getServices);
  yield takeLatest('fetchPostServicesId', Services.postServicesId);
  yield takeLatest('fetchDeleteServicesId', Services.deleteServicesId);
  yield takeLatest('fetchPostServicesNew', Services.postServicesNew);

  yield takeLatest('fetchGetNotifications', Notifications.getNotifications);
  yield takeEvery('fetchPutNotifications', Notifications.putNotifications);
  yield takeLatest(
    'fetchDeleteNotifications',
    Notifications.deleteNotifications,
  );
  yield takeLatest('fetchNotify', Notifications.fetchNotify);

  yield takeLatest('fetchPostDoctorFind', Doctor.postDoctorFind);
  yield takeLatest('fetchPostDoctorVerify', Doctor.postDoctorVerify);

  yield takeLatest('fetchPostSupport', Support.postSupport);

  yield takeLatest('fetchGetReviewsMy', Reviews.getReviewsMy);
  yield takeLatest('fetchGetReviewsFrom', Reviews.getReviewsFrom);
  yield takeLatest('fetchPostReviewsId', Reviews.postReviewsId);

  yield takeLatest('fetchGetBalance', Balance.getBalance);
  yield takeLatest('fetchGetTransactions', Balance.getTransactions);
  yield takeLatest('fetchPostRefill', Balance.postRefill);
  yield takeLatest('fetchPutTransfer', Balance.putTransfer);
  yield takeLatest('fetchPutVideoIdPayment', Balance.putVideoIdPayment);
  yield takeLatest('fetchPostTransfer', Balance.postTransfer);
  yield takeLatest('fetchPostPayout', Balance.postPayout);
}

export default dataSaga;
