import React from 'react';
import {Platform} from 'react-native';
import SplashScreen from 'react-native-splash-screen';
import {requestMultiple, PERMISSIONS} from 'react-native-permissions';

import ReduxView from './store';
// import NetworkIndicator from './components/UI/Base/NetworkIndicator';
import Toast from './components/UI/Base/Toast';
import OneSignal from './components/UI/Base/OneSignal';
import Alert from './components/UI/Alert';
import Route from './route';

import moment from 'moment';
import 'moment/locale/ru';
moment.locale('ru');

export default class App extends React.Component {
  componentDidMount() {
    SplashScreen.hide();

    requestMultiple(
      Platform.OS === 'ios'
        ? [PERMISSIONS.IOS.CAMERA, PERMISSIONS.IOS.MICROPHONE]
        : [PERMISSIONS.ANDROID.CAMERA, PERMISSIONS.ANDROID.RECORD_AUDIO],
    );
  }

  render() {
    return (
      <ReduxView>
        {/* <NetworkIndicator /> */}
        <Toast />
        <Route />
        <Alert />
        <OneSignal />
      </ReduxView>
    );
  }
}
